/**
 * @classdesc Classe principale du jeu
 * @author Vincent Audergon
 * @version 1.0
 */

const VERSION = 1.0;

class Game {

    /**
     * Constructeur du jeu
     * @param {Object} global Un objet contenant les différents objets du Framework
     */
    constructor(global) {
        this.global = global;
        this.global.util.callOnGamemodeChange(this.onGamemodeChanged.bind(this));
        this.global.resources.callOnLanguageChange(this.onLanguageChanged.bind(this));
        this.global.resources.callOnFontChange(this.onFontChange.bind(this));
        this.shapeUtils = new ShapeUtils();
        this.scenes = { draw: new PIXI.Container(), drawQuestion: new PIXI.Container(), explore: new PIXI.Container(), qcm: new PIXI.Container(), shapeInfo: new PIXI.Container(), exercice: new PIXI.Container(), creation: new PIXI.Container, play:new PIXI.Container()}
        this.explorationMode = new ExplorationMode(this);
        this.trainMode = new TrainMode(this);
        this.createMode = new CreationMode(this,this.trainMode);
        this.creationTitre = new CreationTitreMode(this);
        this.playMode = new PlayMode(this, this.trainMode);
        this.oldGamemode = undefined;
        this.inputs = {
            drawMode: document.getElementById('drawMode'),
            signal: document.getElementById('signal')
        };
        new this.global.Log('success', `Chargement de "Formes géométriques" version ${VERSION}`).show();
    }

    /**
     * Affiche une scène sur le canvas
     * @param {PIXI.Container} scene La scène à afficher
     */
    showScene(scene) {
        new this.global.Log('success', `Chargement de la scène`).show();
        for (let input in this.inputs) {
            this.inputs[input].style.display = 'none';
        }
        this.reset();
        this.global.pixiApp.stage.addChild(scene);
    }

    /**
     * Retire toutes les scènes du stage PIXI
     */
    reset() {
        for (let scene in this.scenes) {
            this.global.pixiApp.stage.removeChild(this.scenes[scene])
        }
    }

    /**
     * Affiche du texte sur la page (à gauche du canvas)
     * @param {string} text Le texte à afficher
     */
    showText(text) {
        this.global.util.setTutorialText(text);
    }

    showCreationTitre(){
        this.creationTitre.init();
        this.creationTitre.show();
    }

    /**
     * Fonction de callback appelée lors d'un chagement de langue.
     * Indiques à tous les modes de jeu le changement de langue
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        this.explorationMode.onLanguageChanged(lang);
        this.trainMode.onLanguageChanged(lang);
        this.createMode.onLanguageChanged(lang);
        this.playMode.onLanguageChanged(lang);
    }


    onFontChange(isOpendyslexic){
        this.createMode.onFontChange(isOpendyslexic);
        this.playMode.onFontChange(isOpendyslexic);
    }

    /**
     * Listener appelé par le Framework lorsque le mode de jeu change.
     * Démarre le bon mode de jeu en fonction de celui qui est choisi
     * @async
     */
    async onGamemodeChanged() {
        let gamemode = this.global.util.getGamemode();
        let ok = true;
        if (this.trainMode.running) {
            await Swal.fire({
                title: this.global.resources.getOtherText('quit'),
                text: this.global.resources.getOtherText('progresslost'),
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: this.global.resources.getOtherText('yes'),
                cancelButtonText: this.global.resources.getOtherText('no'),
                reverseButtons: true
            }).then(function (result) {
                ok = (result.value);
                if (ok) {
                    this.trainMode.running = false;
                } else {
                    this.global.util.setGamemode(this.oldGamemode);
                }
            }.bind(this));
        }
        if (ok) {

            switch (gamemode) {
                case this.global.Gamemode.Evaluate:
                    this.trainMode.init(true);
                    this.trainMode.show();
                    break;
                case this.global.Gamemode.Train:
                    this.trainMode.init(false);
                    this.trainMode.show();
                    break;
                case this.global.Gamemode.Explore:
                    this.explorationMode.init();
                    this.explorationMode.show(false);
                    break;
                case this.global.Gamemode.Create:
                    this.createMode.init();
                    this.createMode.show();
                    break;
                case this.global.Gamemode.Play:
                    this.playMode.init();
                    this.playMode.show();
                    break;
            }
            this.oldGamemode = gamemode;
            new this.global.Log('success', 'Mode de jeu changé').show();
        }
    }
}