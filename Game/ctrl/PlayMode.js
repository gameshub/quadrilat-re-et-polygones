class PlayMode extends Mode{
    /**
     * Constructeur du mode play
     * @param {Game} refGame La référence vers la classe de jeu
     */
    constructor(refGame, refTrainMode){
        super('play');
        this.refGame = refGame;
        this.trainMode = refTrainMode;
        this.setInterfaces({main: new Play(refGame, refTrainMode)});
    }

    /**
     * Initialise le mode création. Demande le niveau Harmos de l'exercice.
     */
    init(){
        this.refGame.showText("");
    }

    /**
     * Affiche la page de garde de la création
     */
    show(){
        this.interfaces.main.show();
    }

    /**
     * Fonctione de callback appelée lors d'une changement de langue.
     * Affiche la consigne de la question dans la nouvelle langue.
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        super.onLanguageChanged(lang);
        this.interfaces.main.refreshLang(lang);
    }


    onFontChange(isOpendyslexic){
        this.interfaces.main.updateFont(isOpendyslexic);
    }
}