/**
 * @classdesc Mode création des titres
 * @author Théo Teixeira
 * @version 0.1
 */
class CreationTitreMode extends Mode {
    /**
     * Constructeur du mode création
     * @param {Game} refGame La référence vers la classe de jeu
     */
    constructor(refGame){
        super('creationTitre');
        this.refGame = refGame;
        this.shapeUtils = new ShapeUtils;
        /** Si la création d'une question est en cours */
        this.isCreationEnCours = false;
        /** Nombre de questions */
        this.nQuestion = 0;
        /** Exercice créer */
        this.exercice = null;
        /** Question créer */
        this.question = null;
        this.setInterfaces({titlePage: new Titre(refGame)});
    }

    /**
     * Initialise le mode création. Demande le niveau Harmos de l'exercice.
     */
    init(){
        console.log("Initialisation lancée.");
        this.refGame.showText("");
    }

    /**
     * Affiche la page de garde de la création
     */
    show(){
        this.isCreationEnCours = true;
        this.interfaces.titlePage.show();

    }

    /**
     * Fonctione de callback appelée lors d'une changement de langue.
     * Affiche la consigne de la question dans la nouvelle langue.
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        super.onLanguageChanged(lang);
        this.interfaces.titlePage.refreshLang(lang);
    }
}