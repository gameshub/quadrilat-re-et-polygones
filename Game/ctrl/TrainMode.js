/**
 * @classdesc Mode entraînement
 * @author Vincent Audergon
 * @version 1.0
 */
class TrainMode extends Mode {

    /**
     * Constructeur du mode entrainement / evaluation
     * @param {Game} refGame La référence vers la classe de jeu
     */
    constructor(refGame) {
        super('train');

        this.isTryMode = false;
        this.refCreationMode = null;

        this.refGame = refGame;
        this.questionUtils = new QuestionUtils();
        /** @type {boolean} Si un quizz est en cours */
        this.running = false;
        /** @type {boolean} Mode évaluation ou mode exercice */
        this.evaluation = false;
        /** @type {boolean[]} Le résultat des questions */
        this.results = [];
        this.setInterfaces({
            QCM: new QCM(refGame),
            draw: new Draw(refGame, "drawQuestion"),
            titlePage: new Exercice(refGame)
        });

        this.currentExerciceId = 0;
    }

    /**
     * Initialise le mode entrainement / évaluation. Charge un exercice dans la DB en fonction du niveau harmos
     * @param {boolean} evaluation si il s'agit d'une évaluation
     */
    init(evaluation, tryMode = false, exercice = null, refCreationMode = null) {
        if (!tryMode) {
            this.refGame.inputs.signal.onclick = function () {
                Swal.fire({
                    title: this.getText('signalementTitle'),
                        cancelButtonText: this.getText('signalementCancel'),
                        showCancelButton: true,
                    html:
                        '<label>'+this.getText('signalementReason')+'</label><textarea id="swal-reason" class="swal2-textarea"></textarea>' +
                        '<label>'+this.getText('signalementPassword')+'</label><input type="password" id="swal-password" class="swal2-input">',
                    preConfirm: function () {
                        return new Promise(function (resolve) {
                            resolve([
                                $('#swal-reason').val(),
                                $('#swal-password').val()
                            ])
                        })
                    },
                    onOpen: function () {
                        $('#swal-reason').focus()
                    }
                }).then(function (result) {
                    this.refGame.global.resources.signaler(result.value[0],result.value[1],this.currentExerciceId, function (res) {
                        Swal.fire(res.message,'',res.type);
                        if(res.type === 'success'){
                            this.init(this.evaluation);
                            this.show();
                        }
                    }.bind(this));
                }.bind(this));
            }.bind(this);

        }

        if (tryMode && exercice != null && refCreationMode != null) {
            this.isTryMode = tryMode;
            this.refCreationMode = refCreationMode;
        } else
            this.isTryMode = false;

        this.evaluation = evaluation;
        this.data = this.refGame.global.resources.getExercice();
        try {
            exercice = JSON.parse(this.isTryMode ? exercice : this.data.exercice);
            if (!this.isTryMode) {
                this.currentExerciceId = this.data.id;
                let scenario = JSON.parse(this.refGame.global.resources.getScenario());
                if (scenario.shapes) {
                    this.refGame.shapeUtils.loadShapes(scenario.shapes);
                }
            }
            if (this.questionUtils.loadExercice(exercice)) {
                this.interfaces.titlePage.setOnBegin(function () {
                    this.running = true;
                    this.nextQuestion()
                }.bind(this));
            } else {
                this.interfaces.titlePage.setOnBegin(function () {
                    Swal.fire({
                        title: this.getText('sorry'),
                        type: 'error',
                        text: this.getText('noexercice')
                    })
                }.bind(this));
            }
        } catch (error) {
            Swal.fire({
                title: this.getText('sorry'),
                type: 'error',
                text: this.getText('noexercice')
            });
        }
    }

    /**
     * Affiche la page de garde du quizz
     */
    show() {
        this.running = false;
        this.questionUtils.start();
        this.results = [];
        this.interfaces.titlePage.show(`Harmos ${this.refGame.global.resources.getDegre()}`, this.evaluation, this.questionUtils.getExerciceNames());
        this.refGame.showText(this.refGame.global.resources.getTutorialText(this.evaluation ? 'evaluate' : 'train'));
    }

    /**
     * Passe à la prochaine question et l'affiche sur la page
     */
    nextQuestion() {
        if (!this.questionUtils.isFinished()) {
            this.displayQuestion(this.questionUtils.nextQuestion());
            if(!this.isTryMode)
                this.refGame.inputs.signal.style.display = "block";
        } else {
            //FIN DE L'EXERCICE
            let correct = 0;
            let list = "";
            for (let r in this.results) {
                let result = this.results[r];
                if (result) correct++;
                list += `<li>${this.getText('question')} ${Number(r) + 1}: <span style="color:${result ? 'lime' : 'red'}">${result ? this.getText('correct') : this.getText('wrong')}</span></li>`
            }
            let html = `<ul class="sans-puce">${list}</ul><p>${correct}/${this.results.length}</p>`;
            this.refGame.global.util.showAlertPersonnalized({title: this.getText('results'), html: html});
            this.running = false;
            if (this.isTryMode)
                this.refCreationMode.show();
            else {
                this.refGame.global.statistics.addStats(this.evaluation ? 2 : 1);
                this.refGame.global.statistics.updateStats(this.getGrade(correct, this.results.length));
                this.init(this.evaluation);
                this.show();
            }

        }
    }

    /**
     * Calcule la note en fonction du résultat obtenu
     * @param {int} correct le nombre de bonnes réponses
     * @param {int} total le nombre de questions
     * @return {int} la note de 0 à 2
     */
    getGrade(correct, total) {
        let barem = JSON.parse(this.refGame.global.resources.getEvaluation()).notes;
        let pourcentageCorrect = (correct/total)*100;
        for (let b of barem) {
            if (b.min <= pourcentageCorrect && b.max >= pourcentageCorrect) {
                console.log(b);
                return b.note;
            }
        }
        return 0;
    }

    /**
     * Affiche une question sur la page
     * @param {Question} question La question à afficher
     */
    displayQuestion(question) {
        this.question = question;
        if (question.type === 0) { //QCM
            this.interfaces.QCM.show(question);
            this.interfaces.QCM.setOnResponse(function (responses) {
                let result = this.questionUtils.checkResponse(responses);
                this.results.push(result);
                this.refGame.global.util.showAlertPersonnalized({
                    title: result ? this.getText('correct') : this.getText('wrong'),
                    type: result ? 'success' : 'error'
                });
                this.nextQuestion();
            }.bind(this))
        } else if (question.type === 1) { //Draw
            this.interfaces.draw.show();
            this.interfaces.draw.setOnShapeCreated(function (shape) {
                this.refGame.shapeUtils.initShape(shape);
                let identify = this.refGame.shapeUtils.identifyShape(shape);
                shape.id = identify.id;
                shape.names = identify.names;
                let result = this.questionUtils.checkResponse([shape.id], true);
                this.results.push(result);
                this.refGame.global.util.showAlertPersonnalized({
                    title: result ? this.getText('correct') : this.getText('wrong'),
                    type: result ? 'success' : 'error'
                })
                this.nextQuestion();
            }.bind(this));
        }
        this.onLanguageChanged(this.refGame.global.resources.getLanguage());
    }

    /**
     * Fonctione de callback appelée lors d'une changement de langue.
     * Affiche la consigne de la question dans la nouvelle langue.
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        super.onLanguageChanged(lang);
        if (this.question && this.running) this.refGame.showText(this.question.label[lang]);
    }

}