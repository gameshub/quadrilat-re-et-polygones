/**
 * @classdesc Mode création
 * @author Théo Teixeira
 * @version 0.1
 */
class CreationMode extends Mode{
    /**
     * Constructeur du mode création
     * @param {Game} refGame La référence vers la classe de jeu
     */
    constructor(refGame, refTrainMode){
        super('creation');
        this.refGame = refGame;
        this.shapeUtils = new ShapeUtils;
        // Si la création d'une question est en cours
        this.isCreationEnCours = false;
        // Nombre de questions
        this.nQuestion = 0;
        // Exercice créer
        this.exercice = null;
        // Question créer
        this.question = null;
        this.setInterfaces({titlePage: new Creation(refGame,refTrainMode)});
    }

    /**
     * Initialise le mode création. Demande le niveau Harmos de l'exercice.
     */
    init(){
        this.refGame.showText("");
    }

    /**
     * Affiche la page de garde de la création
     */
    show(){
        this.isCreationEnCours = true;
        this.interfaces.titlePage.show();
    }

    /**
     * Fonctione de callback appelée lors d'une changement de langue.
     * Affiche la consigne de la question dans la nouvelle langue.
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        super.onLanguageChanged(lang);
        this.interfaces.titlePage.refreshLang(lang);
    }


    onFontChange(isOpendyslexic){
        this.interfaces.titlePage.updateFont(isOpendyslexic);
    }
}
