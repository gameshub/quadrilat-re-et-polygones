/**
 * @classdesc Mode de jeu exploration
 * @author Vincent Audergon
 * @version 1.0
 */
class ExplorationMode extends Mode {

    /**
     * Constructeur du mode exploration
     * @param {Game} refGame La référence vers la classe de jeu
     */
    constructor(refGame) {
        super('explore');
        this.refGame = refGame;
        this.drawmode = false;
        this.setInterfaces({ draw: new Draw(refGame), explore: new Explore(refGame), shapeInfo: new ShapeInfo(refGame) })
    }

    /**
     * Initialise le mode de jeu et charge les polygones
     */
    init() {
        this.refGame.inputs.drawMode.onclick = function () {
            this.drawMode = !this.drawMode;
            this.show(this.drawMode);
        }.bind(this);
        try{
            let shapes = JSON.parse(this.refGame.global.resources.getScenario());
            shapes = shapes.shapes;
            this.refGame.shapeUtils.loadShapes(shapes);
            this.interfaces.explore.show(this.refGame.shapeUtils.getShapes());
            this.interfaces.draw.setOnShapeCreated(function (shape) {
                this.displayShapeInfo(shape, true, true,0,0,0,0, true, shape, false);
            }.bind(this));
            this.interfaces.explore.setOnClick(function (shape) {
                this.displayShapeInfo(shape, false, true,0,0,0,0, true, undefined, false);
            }.bind(this));
        } catch(error){
            Swal.fire({ title: this.refGame.global.resources.getOtherText('sorry'), type: 'error', text: this.refGame.global.resources.getOtherText('noshapes') });
        }
    }

    /**
     * Affiche le mode explorer dans le mode "dessin" ou "découverte"
     * @param {boolean} draw si c'est le mode dessin qui doit être affiché
     */
    show(draw) {
        if (draw) {
            this.refGame.showText(this.refGame.global.resources.getTutorialText('exploredraw'))
            this.interfaces.draw.show();
        } else {
            this.refGame.showText(this.refGame.global.resources.getTutorialText('explore'))
            this.interfaces.explore.show(this.refGame.shapeUtils.getShapes());
        }
        this.refGame.inputs.drawMode.style.display = 'block';
    }

    /**
     * Affiche les informations liée à une forme sur la page
     * @param {Shape} shape La forme dont on doit afficher les informations
     */
    displayShapeInfo(shape, draw, lines, biss, med, sideBis, axe, point, secShape = undefined, gravity, nDiagonal) {
        let JSONshapes = JSON.parse(this.refGame.global.resources.getScenario())
        let shapes = JSONshapes.shapes;
        if (!shape.initialized) this.refGame.shapeUtils.initShape(shape);
        let identify = this.refGame.shapeUtils.identifyShape(shape);

        let nAxeSym, nPaireCotePara = 0;
        let axeSym, bissec, mediatrice, mediane, gravityPoint, diagonal = [];

        shape.id = identify.id;
        shape.names = [];
        shape.names = identify.names;

        if (draw){
            let sh = this.refGame.shapeUtils.getShapes();
            let ind = undefined;
            for (let i = 0; i < sh.length; i++) {
                if (shape.id === sh[i].id){
                    ind = sh[i];
                    break;
                }
            }
            let s = undefined;
            for (let i = 0; i < shapes.length; i++) {
                if (secShape.id === shapes[i].id){
                    s = shapes[i];
                    nAxeSym = shapes[i]["nbrAxeSym"];
                    nPaireCotePara =  shapes[i]["nbrPaireCotePara"];
                    axeSym = shapes[i]["AxeSym"];
                    bissec = shapes[i]["bissectrice"];
                    mediane = shapes[i]["mediane"];
                    mediatrice = shapes[i]["mediatrice"];
                    gravityPoint = shapes[i]["gravity"];
                    break;
                }
            }
            this.interfaces.shapeInfo.show(ind, secShape, secShape.initGraphics(100, 100, false),
                ind.initGraphics(200, 200, lines, nAxeSym, nPaireCotePara, axeSym, bissec, mediatrice, mediane, gravityPoint, axe, biss, med, sideBis, point, gravity,diagonal,nDiagonal), draw);
        } else {
            let shapeSec = shape;
            let identifySec = identify;

            for (let i = 0; i < shapes.length; i++) {
                if (shape.id === shapes[i].id){
                    nAxeSym = shapes[i]["nbrAxeSym"];
                    nPaireCotePara =  shapes[i]["nbrPaireCotePara"];
                    axeSym = shapes[i]["AxeSym"];
                    bissec = shapes[i]["bissectrice"];
                    mediane = shapes[i]["mediane"];
                    mediatrice = shapes[i]["mediatrice"];
                    gravityPoint = shapes[i]["gravity"];
                    diagonal = shapes[i]['diagonal'];
                    break;
                }
            }
            this.interfaces.shapeInfo.show(shape, shapeSec, shapeSec.initGraphics(100, 100, false),
                shape.initGraphics(200, 200, lines, nAxeSym, nPaireCotePara, axeSym, bissec, mediatrice, mediane, gravityPoint, axe, biss, med, sideBis, point, gravity,diagonal,nDiagonal), draw);
        }

        this.interfaces.shapeInfo.setOnClose(function () {
            this.show(this.drawMode);
        }.bind(this))
    }


}