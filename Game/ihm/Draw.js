
/**
 * @classdesc IHM de dessin
 * @author Vincent Audergon
 * @version 1.0
 */
class Draw extends Interface {

    /**
     * Constructeur de l'ihm de dessin
     * @param {Game} refGame la référence vers la classe de jeu
     * @param {string} [scene="draw"] le nom de la scène utilisée par l'ihm, "draw" par défaut (facultatif)
     */
    constructor(refGame, scene = "draw") {
        super(refGame, scene);
        super.setElements({
            grid: new DrawingGrid(9, 9, 50, this.scene, function (shape) { this.onShapeCreated(shape) }.bind(this)),
            finger: PIXI.Sprite.from(refGame.global.resources.getImage('finger').getImage().src)
        });
        this.elements.grid.setOnLineDrawn(function () { this.elements.finger.visible = false; }.bind(this));
        this.refGame.global.pixiApp.ticker.add(function () {
            this.elements.finger.x += 2;
            this.elements.finger.alpha -= 0.01;
            if (this.elements.finger.x > 460) {
                this.elements.finger.x = 160;
                this.elements.finger.alpha = 1;
            }
        }.bind(this));
        /** @type {function} fonction de callback appelée lorsqu'une forme est dessinée */
        this.onShapeCreated = undefined;
    }

    /**
     * Affiche l'ihm de dessin. Réinitialise la grille de dessin.
     */
    show() {
        this.clear();
        this.elements.grid.reset();
        this.elements.finger.visible = true;
        this.elements.finger.alpha = 1;
        this.elements.finger.x = 160;
        this.elements.finger.y = 190;
        this.init();
    }

    /**
     * Défini la fonction de callback appelée lorsqu'une forme est créée avec la grille de dessin
     * @param {function} onShapeCreated La fonction de callback
     */
    setOnShapeCreated(onShapeCreated) {
        this.onShapeCreated = onShapeCreated;
    }

}
