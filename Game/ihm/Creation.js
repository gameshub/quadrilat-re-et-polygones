/**
 * @classdesc IHM de la création d'exercice
 * @author Théo Teixeira
 * @version 1.0
 */

/**
 * Taille de l'entête
 * @type {number} Taille
 */
const HEADER_SIZE = 100;

class Creation extends Interface {

    /**
     * Constructeur de l'ihm de la création d'exercice
     * @param {Game} refGame la référence vers la classe de jeu
     * @param {TrainMode} refTrainMode la référence au mode exercer pour tester les jeux
     */
    constructor(refGame, refTrainMode) {

        super(refGame, "creation");

        this.initialized = false;

        this.refGame = refGame;

        this.refTrainMode = refTrainMode;
        this.generateGame();
    }

    /**
     * Point d'entrée pour l'affichage
     */
    show() {
        this.clear();
        //Create generated elements
        this.generateMissingElements();
        //Setup elements --> cf. setupElements function's documentation
        this.setupElements();

        this.setElements(this.extractElements());

        //Display the current step (0 by default)
        this.updateStepView();

        this.refGame.showText("");
        this.refreshLang(this.refGame.global.resources.getLanguage());

        this.init();
    }

    /**
     * Méthode permettant de mettre à jour tous les composants graphiques lors du changement de langue.
     * @param {string} lang Langue à afficher
     */
    refreshLang(lang) {
        for (let i = 0; i < this.getShapeForCurrentDegree().length; i++) {
            this.steps.specific[0]["form".concat(i + 1)].text = this.refGame.shapeUtils.getNames(this.getShapeForCurrentDegree()[i].id)[lang];
        }

        for (let alert of Object.keys(this.alert))
            this.alert[alert] = this.getText(alert);

        //Setup for all steps's specifics elements
        for (let i = 0; i < this.steps.specific.length; i++) {
            this.steps.specific[i]["title".concat(i)].text = this.getText('step' + i + 'Title');
            this.steps.specific[i]["subtitle".concat(i)].text = this.getText('step' + i + 'Subtitle');

            let subtitle = this.getText('step' + i + 'Subtitle');
            this.lang.push(this.getText('step' + i + 'Title') +".<br>"+ ((typeof subtitle !== 'undefined') ? subtitle : ''));
            //Update size;
            this.steps.specific[i]["subtitle".concat(i)].y = 85 + this.steps.specific[i]["subtitle".concat(i)].height / 2;
        }

        this.steps.specific[0].btnNext0.setText(this.getText('btnNext'));
        this.steps.specific[1].btnBack1.setText(this.getText('btnBack'));
        this.steps.specific[1].btnSaveAndNext.setText(this.getText('btnSaveAndNext'));
        this.steps.specific[2].btnNext2.setText(this.getText('btnNext'));
        this.steps.specific[3].btnNext3.setText(this.getText('btnNext'));
        this.steps.specific[3].btnBack3.setText(this.getText('btnBack'));
        this.steps.specific[4].btnNext4.setText(this.getText('btnNext'));
        this.steps.specific[4].btnBack4.setText(this.getText('btnBack'));
        this.steps.specific[5].btnNext5.setText(this.getText('btnNext'));
        this.steps.specific[5].btnBack5.setText(this.getText('btnBack'));
        this.steps.specific[6].btnNext6.setText(this.getText('btnNextQuestion'));
        this.steps.specific[6].btnBack6.setText(this.getText('btnBack'));
        this.steps.specific[6].btnQuestionEnd.setText(this.getText('btnQuestionEnd'));

        this.steps.specific[0].btnShowShapes.setText(this.getText('btnDisplayShapes'));
        this.steps.specific[0].btnHideShapes.setText(this.getText('btnHideShapes'));

        this.steps.general.info.text = this.refGame.global.resources.getOtherText('info', {
            mode: this.getText('mode'),
            harmos: this.refGame.global.resources.getDegre(),
            lang: lang
        });

        this.steps.specific[1].french.placeholder = this.getText('french');
        this.steps.specific[1].german.placeholder = this.getText('german');
        this.steps.specific[1].english.placeholder = this.getText('english');
        this.steps.specific[1].italian.placeholder = this.getText('italian');

        this.steps.specific[1].englishInputLabel.text = this.getText("english");
        this.steps.specific[1].frenchInputLabel.text = this.getText("french");
        this.steps.specific[1].germanInputLabel.text = this.getText("german");
        this.steps.specific[1].italianInputLabel.text = this.getText("italian");

        this.steps.specific[3].frenchQuestionName.placeholder = this.getText('french');
        this.steps.specific[3].germanQuestionName.placeholder = this.getText('german');
        this.steps.specific[3].englishQuestionName.placeholder = this.getText('english');
        this.steps.specific[3].italianQuestionName.placeholder = this.getText('italian');

        this.steps.specific[3].englishQuestionNameInputLabel.text = this.getText("english");
        this.steps.specific[3].frenchQuestionNameInputLabel.text = this.getText("french");
        this.steps.specific[3].germanQuestionNameInputLabel.text = this.getText("german");
        this.steps.specific[3].italianQuestionNameInputLabel.text = this.getText("italian");

        this.steps.specific[2].questionType.getButtons()[0].setText(this.getText('questionTypeRadio'));
        this.steps.specific[2].questionType.getButtons()[1].setText(this.getText('questionTypeCheckbox'));
        this.steps.specific[2].questionType.getButtons()[2].setText(this.getText('questionTypeDraw'));


        for (let btn of this.steps.specific[4].multipleChoice.getButtons()) {
            btn.setText(this.refGame.shapeUtils.getNames(btn.data)[lang]);
        }
        for (let btn of this.steps.specific[5].singleChoice.getButtons()) {
            btn.setText(this.refGame.shapeUtils.getNames(btn.data)[lang]);

        }

        this.steps.specific[6].allShapeLabel.text = this.getText('resumeAllShapesLabel');
        this.steps.specific[6].trueShapeLabel.text = this.getText('resumeTrueShapesLabel');

        if (this.currentStep === 6)
            this.prepareResume();

        this.steps.specific[7].isNew.setText(this.getText("isExerciceNew"));
        this.steps.specific[7].understandable.setText(this.getText("isExerciceUnderstandable"));
        this.steps.specific[7].btnNext7.setText(this.getText('btnNext'));
        this.steps.specific[8].btnTest.setText(this.getText('step8Test'));
        this.steps.specific[8].btnSubmit.setText(this.getText('step8Submit'));
        this.steps.specific[8].btnDelete.setText(this.getText('step8Delete'));

        this.steps.specific[9].yes9.setText(this.getText("yes"));
        this.steps.specific[9].no9.setText(this.getText("no"));
        this.steps.specific[10].yes10.setText(this.getText("yes"));
        this.steps.specific[10].no10.setText(this.getText("no"));

   }

    generateGame(){
        this.exercice = new ExerciceForSave();

        this.currentStep = 0;
        this.shapesNameDisplayed = false;

        this.labelStyle = new PIXI.TextStyle();
        this.titleStyle = new PIXI.TextStyle();
        this.subtitleStyle = new PIXI.TextStyle();

        this.titleStyle.align = 'center';
        this.titleStyle.fill = 0x000000;
        this.titleStyle.fontFamily = 'Arial';
        this.titleStyle.fontSize = 32;
        this.subtitleStyle.align = 'center';
        this.subtitleStyle.fill = 0x000000;
        this.subtitleStyle.fontFamily = 'Arial';
        this.subtitleStyle.fontSize = 18;
        this.subtitleStyle.wordWrap = true;
        this.subtitleStyle.wordWrapWidth = 550;
        this.labelStyle.fontFamily = 'Arial';
        this.labelStyle.fontSize = 22;
        this.labelStyle.align = 'left';
        this.labelStyle.fill = 0x000000;


        /**
         * Variable contenant les formes pour le degrès actuel
         * @type {{shapes: Array, lastDegree: number}}
         */
        this.shapes = {
            shapes: [],
            lastDegree: -1
        };

        /**
         * Variable contenant tous les messages d'alerte.
         * @type {{noType: string, confirmDeletionText: string, noNameText: string, checkConditionText: string, checkCondition: string, noName: string, confirmDeletion: string, noTypeText: string}}
         */
        this.alert = {
            noName: '',
            noNameText: '',
            noType: '',
            noTypeText: '',
            checkCondition: '',
            checkConditionText: '',
            confirmDeletion: '',
            confirmDeletionText: ''
        };

        /**
         *
         * @type {{general: {}, specific: *[]}}} Variable contenant tous les éléments graphiques pour les différentes interfaces.
         */
        this.steps = {
            specific: [
                // 0 = Choose Harmos and display shapes for degree
                {
                    btnNext0: new Button(450, 570, '', 0x00AA00, 0xFFFFFF),
                    btnShowShapes: new Button(100, 570, '', 0x00AA00, 0xFFFFFF),
                    btnHideShapes: new Button(100, 570, '', 0x00AA00, 0xFFFFFF),
                    title0: new PIXI.Text('', this.titleStyle),
                    subtitle0: new PIXI.Text('', this.subtitleStyle),
                },
                // 1 = Choose title for exercices
                {

                    btnBack1: new Button(100, 570, '', 0x00AA00, 0xFFFFFF),
                    btnSaveAndNext: new Button(400, 570, '', 0x00AA00, 0xFFFFFF, true),
                    title1: new PIXI.Text('', this.titleStyle),
                    subtitle1: new PIXI.Text('', this.subtitleStyle),
                    french: new PIXI.TextInput({
                        input: {
                            fontSize: '16px',
                            padding: '8px',
                            width: '250px',
                            color: '#000000'
                        },
                        box: {
                            default: {fill: 0xFFFFFF, rounded: 12, stroke: {color: 0xCBCEE0, width: 3}},
                            focused: {fill: 0xFFFFFF, rounded: 12, stroke: {color: 0xABAFC6, width: 3}},
                            disabled: {fill: 0xDBDBDB, rounded: 12}
                        }
                    }),
                    german: new PIXI.TextInput({
                        input: {
                            fontSize: '16px',
                            padding: '8px',
                            width: '250px',
                            color: '#000000'
                        },
                        box: {
                            default: {fill: 0xFFFFFF, rounded: 12, stroke: {color: 0xCBCEE0, width: 3}},
                            focused: {fill: 0xFFFFFF, rounded: 12, stroke: {color: 0xABAFC6, width: 3}},
                            disabled: {fill: 0xDBDBDB, rounded: 12}
                        }
                    }),
                    english: new PIXI.TextInput({
                        input: {
                            fontSize: '16px',
                            padding: '8px',
                            width: '250px',
                            color: '#000000'
                        },
                        box: {
                            default: {fill: 0xFFFFFF, rounded: 12, stroke: {color: 0xCBCEE0, width: 3}},
                            focused: {fill: 0xFFFFFF, rounded: 12, stroke: {color: 0xABAFC6, width: 3}},
                            disabled: {fill: 0xDBDBDB, rounded: 12}
                        }
                    }),
                    italian: new PIXI.TextInput({
                        input: {
                            fontSize: '16px',
                            padding: '8px',
                            width: '250px',
                            color: '#000000'
                        },
                        box: {
                            default: {fill: 0xFFFFFF, rounded: 12, stroke: {color: 0xCBCEE0, width: 3}},
                            focused: {fill: 0xFFFFFF, rounded: 12, stroke: {color: 0xABAFC6, width: 3}},
                            disabled: {fill: 0xDBDBDB, rounded: 12}
                        }
                    }),
                    frenchInputLabel: new PIXI.Text('', this.labelStyle),
                    englishInputLabel: new PIXI.Text('', this.labelStyle),
                    germanInputLabel: new PIXI.Text('', this.labelStyle),
                    italianInputLabel: new PIXI.Text('', this.labelStyle),

                },
                // 2 = Choose type of question
                {
                    btnNext2: new Button(450, 570, '', 0x00AA00, 0xFFFFFF),
                    title2: new PIXI.Text('', this.titleStyle),
                    subtitle2: new PIXI.Text('', this.subtitleStyle),
                    questionType: new ToggleGroup('single', [
                        new ToggleButton(300, 200, '', true),
                        new ToggleButton(300, 250, '', true),
                        new ToggleButton(300, 300, '', true)
                    ])

                },
                // 3 = Choose name for question
                {
                    btnNext3: new Button(450, 570, '', 0x00AA00, 0xFFFFFF),
                    btnBack3: new Button(100, 570, '', 0x00AA00, 0xFFFFFF),
                    title3: new PIXI.Text('', this.titleStyle),
                    subtitle3: new PIXI.Text('', this.subtitleStyle),
                    frenchQuestionName: new PIXI.TextInput({
                        input: {
                            multiline: true,
                            height: '60px',
                            fontSize: '16px',
                            padding: '8px',
                            width: '250px',
                            color: '#000000'
                        },
                        box: {
                            default: {fill: 0xFFFFFF, rounded: 12, stroke: {color: 0xCBCEE0, width: 3}},
                            focused: {fill: 0xFFFFFF, rounded: 12, stroke: {color: 0xABAFC6, width: 3}},
                            disabled: {fill: 0xDBDBDB, rounded: 12}
                        }
                    }),
                    germanQuestionName: new PIXI.TextInput({
                        input: {
                            multiline: true,
                            height: '60px',
                            fontSize: '16px',
                            padding: '8px',
                            width: '250px',
                            color: '#000000'
                        },
                        box: {
                            default: {fill: 0xFFFFFF, rounded: 12, stroke: {color: 0xCBCEE0, width: 3}},
                            focused: {fill: 0xFFFFFF, rounded: 12, stroke: {color: 0xABAFC6, width: 3}},
                            disabled: {fill: 0xDBDBDB, rounded: 12}
                        }
                    }),
                    englishQuestionName: new PIXI.TextInput({
                        input: {
                            multiline: true,
                            height: '60px',
                            fontSize: '16px',
                            padding: '8px',
                            width: '250px',
                            color: '#000000'
                        },
                        box: {
                            default: {fill: 0xFFFFFF, rounded: 12, stroke: {color: 0xCBCEE0, width: 3}},
                            focused: {fill: 0xFFFFFF, rounded: 12, stroke: {color: 0xABAFC6, width: 3}},
                            disabled: {fill: 0xDBDBDB, rounded: 12}
                        }
                    }),
                    italianQuestionName: new PIXI.TextInput({
                        input: {
                            multiline: true,
                            height: '60px',
                            fontSize: '16px',
                            padding: '8px',
                            width: '250px',
                            color: '#000000'
                        },
                        box: {
                            default: {fill: 0xFFFFFF, rounded: 12, stroke: {color: 0xCBCEE0, width: 3}},
                            focused: {fill: 0xFFFFFF, rounded: 12, stroke: {color: 0xABAFC6, width: 3}},
                            disabled: {fill: 0xDBDBDB, rounded: 12}
                        }
                    }),
                    frenchQuestionNameInputLabel: new PIXI.Text('', this.labelStyle),
                    englishQuestionNameInputLabel: new PIXI.Text('', this.labelStyle),
                    germanQuestionNameInputLabel: new PIXI.Text('', this.labelStyle),
                    italianQuestionNameInputLabel: new PIXI.Text('', this.labelStyle)
                },
                // 4 = Choose shapes - Multiple choice
                {
                    btnNext4: new Button(450, 570, '', 0x00AA00, 0xFFFFFF),
                    btnBack4: new Button(100, 570, '', 0x00AA00, 0xFFFFFF),
                    title4: new PIXI.Text('', this.titleStyle),
                    subtitle4: new PIXI.Text('', this.subtitleStyle),
                    multipleChoice: new ToggleGroup('multiple')
                },
                // 5 = Choose shape - Unique choice
                {
                    btnNext5: new Button(450, 570, '', 0x00AA00, 0xFFFFFF),
                    btnBack5: new Button(100, 570, '', 0x00AA00, 0xFFFFFF),
                    title5: new PIXI.Text('', this.titleStyle),
                    subtitle5: new PIXI.Text('', this.subtitleStyle),
                    singleChoice: new ToggleGroup()
                },

                // 6 = Resume
                {
                    btnQuestionEnd: new Button(275, 570, '', 0x00AA00, 0xFFFFFF, true),
                    btnNext6: new Button(450, 570, '', 0x00AA00, 0xFFFFFF, true),
                    btnBack6: new Button(100, 570, '', 0x00AA00, 0xFFFFFF),
                    title6: new PIXI.Text('', this.titleStyle),
                    subtitle6: new PIXI.Text('', this.subtitleStyle),
                    allShape: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 16, fill: 0x000000, align: 'center'}),
                    trueShape: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 16, fill: 0x000000, align: 'center'}),
                    questionLabel: new PIXI.Text('', {
                        fontFamily: 'Arial',
                        fontSize: 19,
                        fill: 0x000000,
                        align: 'center',
                        fontWeight: 'bold',
                        breakWords: true,
                        wordWrap: true,
                        wordWrapWidth: 550
                    }),
                    allShapeLabel: new PIXI.Text('', {
                        fontFamily: 'Arial',
                        fontSize: 18,
                        fill: 0x000000,
                        align: 'center',
                        fontWeight: 'bold',
                        breakWords: true,
                        wordWrap: true,
                        wordWrapWidth: 280
                    }),
                    trueShapeLabel: new PIXI.Text('', {
                        fontFamily: 'Arial',
                        fontSize: 18,
                        fill: 0x000000,
                        align: 'center',
                        fontWeight: 'bold',
                        breakWords: true,
                        wordWrap: true,
                        wordWrapWidth: 280
                    }),

                },
                // 7 = Choose answers for checkbox
                {
                    title7: new PIXI.Text('', this.titleStyle),
                    subtitle7: new PIXI.Text('', this.subtitleStyle),
                    isNew: new CheckBox(25, HEADER_SIZE + 100, '', 0.9),
                    understandable: new CheckBox(25, HEADER_SIZE + 200, '', 0.9),
                    btnNext7: new Button(450, 570, '', 0x00AA00, 0xFFFFFF)

                },
                // 8 = Confirmation soumission
                {
                    title8: new PIXI.Text('', this.titleStyle),
                    subtitle8: new PIXI.Text('', this.subtitleStyle),
                    btnSubmit: new Button(300, 200, '', 0x00AA00, 0xFFFFFF, false, 250),
                    btnDelete: new Button(300, 300, '', 0x00AA00, 0xFFFFFF, false, 250),
                    btnTest: new Button(300, 400, '', 0x00AA00, 0xFFFFFF, false, 250)
                },
                // 9 = Confirmation suppression
                {
                    title9: new PIXI.Text('', this.titleStyle),
                    subtitle9: new PIXI.Text('', this.subtitleStyle),
                    yes9: new Button(200, 300, '', 0x00AA00, 0xFFFFFF),
                    no9: new Button(400, 300, '', 0x00AA00, 0xFFFFFF)
                },
                // 10 = alert pas de question
                {
                    title10: new PIXI.Text('', this.titleStyle),
                    subtitle10: new PIXI.Text('', this.subtitleStyle),
                    yes10: new Button(200, 300, '', 0x00AA00, 0xFFFFFF),
                    no10: new Button(400, 300, '', 0x00AA00, 0xFFFFFF)
                }
            ],
            general: {
                info: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 16, fill: 0x00, align: 'left'})
            }
        };

        this.lang = [];
    }


    /**
     * Méthode retournant tous les éléments graphiques
     * @param {boolean} onlySteps Choix de prendre uniquement les éléments correspondant à une étape spécifique
     * @return {{}} JSON des éléments
     */
    extractElements(onlySteps = false) {
        let elements = {};
        if (!onlySteps)
            for (let element of Object.keys(this.steps.general))
                elements[element] = this.steps.general[element];
        for (let step of this.steps.specific)
            for (let element of Object.keys(step))
                elements[element] = step[element];
        return elements;
    }


    /**
     * Méthode de génération de tous les éléments provenant de la base de données
     */
    generateMissingElements() {
        let x = 120;
        let y = HEADER_SIZE + 50;
        let spaceBetweenLine = 40;
        for (let element of Object.keys(this.steps.specific[0])) {
            if (element.startsWith("form")) {
                delete this.steps.specific[0][element];
            }
        }
        for (let i = 1; i <= this.getShapeForCurrentDegree().length; i++) {
            this.steps.specific[0]["form".concat(i)] = new PIXI.Text('', {
                fontFamily: 'Arial',
                fontSize: 16,
                fill: 0x000000,
                align: 'left'
            });
            this.steps.specific[0]["form".concat(i)].x = x;
            this.steps.specific[0]["form".concat(i)].y = y;
            this.steps.specific[0]["form".concat(i)].anchor.set(0.5);
            y += spaceBetweenLine;
            if (this.getShapeForCurrentDegree().length / 2 - 1 < i && x === 120) {
                x = 420;
                y = HEADER_SIZE + 50;
            }
        }

        x = 150;
        y = HEADER_SIZE + 50;
        spaceBetweenLine = 40;
        this.steps.specific[4].multipleChoice.clearButtons();
        this.steps.specific[5].singleChoice.clearButtons();
        for (let i = 0; i < this.getShapeForCurrentDegree().length; i++) {
            let multiple = new ToggleButton(x, y, '', false, 250);
            multiple.data = this.getShapeForCurrentDegree()[i].id;
            let single = new ToggleButton(x, y, '', false, 250);
            single.data = this.getShapeForCurrentDegree()[i].id;
            this.steps.specific[4].multipleChoice.addButton(multiple);
            this.steps.specific[5].singleChoice.addButton(single);
            y += spaceBetweenLine;
            if (this.getShapeForCurrentDegree().length / 2 - 1 <= i && x === 150) {
                x = 450;
                y = HEADER_SIZE + 50;
            }

        }
    }


    /**
     * Paramétrage de la mise en page et des actions sur les différents boutons
     */
    setupElements() {

        this.exercice.degree = this.refGame.global.resources.getDegre();

        /////////////////////////////////////
        //              General            //
        /////////////////////////////////////

        this.steps.general.info.x = 10;
        this.steps.general.info.y = 10;
        this.steps.general.info.anchor.set(0.0);
        this.steps.general.info.visible = false;

        this.steps.specific[0].btnNext0.setOnClick(function () {
            this.chooseNextStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[1].btnBack1.setOnClick(function () {
            this.chooseBackStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[1].btnSaveAndNext.setOnClick(function () {
            this.chooseNextStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[2].btnNext2.setOnClick(function () {
            this.chooseNextStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[3].btnNext3.setOnClick(function () {
            this.chooseNextStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[3].btnBack3.setOnClick(function () {
            this.chooseBackStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[4].btnNext4.setOnClick(function () {
            this.chooseNextStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[4].btnBack4.setOnClick(function () {
            this.chooseBackStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[5].btnNext5.setOnClick(function () {
            this.chooseNextStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[5].btnBack5.setOnClick(function () {
            this.chooseBackStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[6].btnNext6.setOnClick(function () {
            this.chooseNextStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[6].btnBack6.setOnClick(function () {
            this.chooseBackStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[7].btnNext7.setOnClick(function () {
            this.chooseNextStep();
            this.updateStepView();
        }.bind(this));


        for (let i = 0; i < this.steps.specific.length; i++) {
            this.steps.specific[i]["title".concat(i)].anchor.set(0.5);
            this.steps.specific[i]["subtitle".concat(i)].anchor.set(0.5);
            this.steps.specific[i]["title".concat(i)].x = 300;
            this.steps.specific[i]["title".concat(i)].y = 50;
            this.steps.specific[i]["subtitle".concat(i)].x = 300;
            this.steps.specific[i]["subtitle".concat(i)].y = 85;
        }

        /////////////////////////////////////
        //              Step 0             //
        /////////////////////////////////////

        for (let potentialForm of Object.keys(this.steps.specific[0])) {
            if (potentialForm.startsWith("form"))
                this.steps.specific[0][potentialForm].visible = false;
        }

        this.steps.specific[0].btnShowShapes.setOnClick(function () {
            for (let potentialForm of Object.keys(this.steps.specific[0])) {
                if (potentialForm.startsWith("form"))
                    this.steps.specific[0][potentialForm].visible = true;
                this.shapesNameDisplayed = true;
                this.setButtonVisible(this.steps.specific[0].btnHideShapes, true);
                this.setButtonVisible(this.steps.specific[0].btnShowShapes, false);
            }
        }.bind(this));

        this.steps.specific[0].btnHideShapes.setOnClick(function () {
            for (let potentialForm of Object.keys(this.steps.specific[0])) {
                if (potentialForm.startsWith("form"))
                    this.steps.specific[0][potentialForm].visible = false;
                this.shapesNameDisplayed = false;
                this.setButtonVisible(this.steps.specific[0].btnHideShapes, false);
                this.setButtonVisible(this.steps.specific[0].btnShowShapes, true);
            }
        }.bind(this));

        this.setButtonVisible(this.steps.specific[0].btnHideShapes, this.shapesNameDisplayed);


        /////////////////////////////////////
        //              Step 1             //
        /////////////////////////////////////


        this.steps.specific[1].french.x = 150;
        this.steps.specific[1].german.x = 150;
        this.steps.specific[1].english.x = 150;
        this.steps.specific[1].italian.x = 150;

        this.steps.specific[1].french.y = 120;
        this.steps.specific[1].german.y = 200;
        this.steps.specific[1].english.y = 280;
        this.steps.specific[1].italian.y = 360;

        this.steps.specific[1].french.width = 300;
        this.steps.specific[1].german.width = 300;
        this.steps.specific[1].english.width = 300;
        this.steps.specific[1].italian.width = 300;

        this.steps.specific[1].englishInputLabel.x = 50;
        this.steps.specific[1].frenchInputLabel.x = 50;
        this.steps.specific[1].germanInputLabel.x = 50;
        this.steps.specific[1].italianInputLabel.x = 50;

        this.steps.specific[1].frenchInputLabel.y = 125;
        this.steps.specific[1].germanInputLabel.y = 205;
        this.steps.specific[1].englishInputLabel.y = 285;
        this.steps.specific[1].italianInputLabel.y = 365;

        this.steps.specific[1].englishInputLabel.anchor.set(0);
        this.steps.specific[1].frenchInputLabel.anchor.set(0);
        this.steps.specific[1].germanInputLabel.anchor.set(0);
        this.steps.specific[1].italianInputLabel.anchor.set(0);

        /////////////////////////////////////
        //              Step 2             //
        /////////////////////////////////////

        this.steps.specific[2].questionType.getButtons()[0].data = {
            nextStep: 3,
            type: 'qcm',
            qcmtype: 'radio'
        };
        this.steps.specific[2].questionType.getButtons()[1].data = {
            nextStep: 3,
            type: 'qcm',
            qcmtype: 'checkbox'
        };
        this.steps.specific[2].questionType.getButtons()[2].data = {
            nextStep: 3,
            type: 'draw',
            qcmtype: null
        };

        /////////////////////////////////////
        //              Step 3             //
        /////////////////////////////////////

        this.steps.specific[3].frenchQuestionName.x = 150;
        this.steps.specific[3].germanQuestionName.x = 150;
        this.steps.specific[3].englishQuestionName.x = 150;
        this.steps.specific[3].italianQuestionName.x = 150;

        this.steps.specific[3].frenchQuestionName.y = 120;
        this.steps.specific[3].germanQuestionName.y = 200;
        this.steps.specific[3].englishQuestionName.y = 280;
        this.steps.specific[3].italianQuestionName.y = 360;

        this.steps.specific[3].frenchQuestionName.width = 300;
        this.steps.specific[3].germanQuestionName.width = 300;
        this.steps.specific[3].englishQuestionName.width = 300;
        this.steps.specific[3].italianQuestionName.width = 300;

        this.steps.specific[3].englishQuestionNameInputLabel.x = 50;
        this.steps.specific[3].frenchQuestionNameInputLabel.x = 50;
        this.steps.specific[3].germanQuestionNameInputLabel.x = 50;
        this.steps.specific[3].italianQuestionNameInputLabel.x = 50;

        this.steps.specific[3].frenchQuestionNameInputLabel.y = 125;
        this.steps.specific[3].germanQuestionNameInputLabel.y = 205;
        this.steps.specific[3].englishQuestionNameInputLabel.y = 285;
        this.steps.specific[3].italianQuestionNameInputLabel.y = 365;

        this.steps.specific[3].englishQuestionNameInputLabel.anchor.set(0);
        this.steps.specific[3].frenchQuestionNameInputLabel.anchor.set(0);
        this.steps.specific[3].germanQuestionNameInputLabel.anchor.set(0);
        this.steps.specific[3].italianQuestionNameInputLabel.anchor.set(0);

        /////////////////////////////////////
        //              Step 6             //
        /////////////////////////////////////

        this.steps.specific[6].questionLabel.anchor.set(0.5);
        this.steps.specific[6].allShape.anchor.set(0.5);
        this.steps.specific[6].trueShape.anchor.set(0.5);
        this.steps.specific[6].trueShapeLabel.anchor.set(0.5);
        this.steps.specific[6].allShapeLabel.anchor.set(0.5);

        this.steps.specific[6].btnQuestionEnd.setOnClick(function () {
            this.exercice.endQuestion();
            this.currentStep = 7;
            this.updateStepView();
        }.bind(this));

        /////////////////////////////////////
        //              Step 8             //
        /////////////////////////////////////

        this.steps.specific[8].btnDelete.setOnClick(function () {
            this.currentStep = 10;
            this.updateStepView();
        }.bind(this));
        this.steps.specific[8].btnSubmit.setOnClick(function () {
            this.currentStep = 9;
            this.updateStepView();
        }.bind(this));
        this.steps.specific[8].btnTest.setOnClick(function () {
            this.testExercice();
        }.bind(this));

        /////////////////////////////////////
        //              Step 9             //
        /////////////////////////////////////

        this.steps.specific[9].no9.setOnClick(function () {
            this.currentStep = 8;
            this.updateStepView();
        }.bind(this));
        this.steps.specific[9].yes9.setOnClick(function () {
            this.submitExercice();
        }.bind(this));

        /////////////////////////////////////
        //              Step 10            //
        /////////////////////////////////////

        this.steps.specific[10].no10.setOnClick(function () {
            this.currentStep = 8;
            this.updateStepView();
        }.bind(this));
        this.steps.specific[10].yes10.setOnClick(function () {
            this.deleteExercice();
        }.bind(this));
    }

    /**
     * Mise à jour de l'interface en fonction de l'étape.
     */
    updateStepView() {
        this.hideAll();
        for (let elementToDisplay of Object.keys(this.steps.specific[this.currentStep])) {
            if (this.currentStep === 0 &&
                ((elementToDisplay.startsWith("form") && !this.shapesNameDisplayed)
                    || elementToDisplay.startsWith("btnHideShapes") && !this.shapesNameDisplayed))
                continue;
            if (this.steps.specific[this.currentStep][elementToDisplay] instanceof Button)
                this.setButtonVisible(this.steps.specific[this.currentStep][elementToDisplay], true);
            else if (this.steps.specific[this.currentStep][elementToDisplay] instanceof ToggleButton
                || this.steps.specific[this.currentStep][elementToDisplay] instanceof ToggleGroup)
                this.steps.specific[this.currentStep][elementToDisplay].show();
            else if (this.steps.specific[this.currentStep][elementToDisplay] instanceof CheckBox)
                this.steps.specific[this.currentStep][elementToDisplay].setVisible(true);
            else
                this.steps.specific[this.currentStep][elementToDisplay].visible = true;
        }
        this.refGame.showText(this.lang[this.currentStep]);
    }

    /**
     * Cache tous les éléments présents sur l'interface
     */
    hideAll() {
        let elements = this.extractElements(true);
        for (let elementName of Object.keys(elements))
            if (elements[elementName] instanceof Button)
                this.setButtonVisible(elements[elementName], false);
            else if (elements[elementName] instanceof ToggleButton || elements[elementName] instanceof ToggleGroup)
                elements[elementName].hide();
            else if (elements[elementName] instanceof CheckBox)
                elements[elementName].setVisible(false);
            else
                elements[elementName].visible = false;
    }


    /**
     * Affichage / Désaffichage d'un bouton.
     * @param {Button} button Bouton à modifier
     * @param {boolean} visible Le bouton doit-il être visible
     */
    setButtonVisible(button, visible) {
        for (let pixiElement of button.getPixiChildren()) {
            pixiElement.visible = visible;
        }
    }

    /**
     * Choix de l'étape précédante en fonction de l'étape actuelle;
     */
    chooseBackStep() {
        switch (this.currentStep) {
            case 6:
                if (this.exercice.currentQuestion.type === 'draw') {
                    this.currentStep = 4;
                    this.updateShapeSelection(false);
                } else if (this.exercice.currentQuestion.qcmtype === 'radio') {
                    this.currentStep = 5;
                    this.updateShapeSelection(true);
                } else if (this.exercice.currentQuestion.qcmtype === 'checkbox') {
                    this.currentStep = 4;
                    this.updateShapeSelection(true);
                }
                break;
            case 4:
                if (this.exercice.currentQuestion.type === 'draw')
                    this.currentStep = 3;
                else if (this.exercice.currentQuestion.qcmtype === 'radio')
                    this.currentStep = 3;
                else if (this.exercice.currentQuestion.qcmtype === 'checkbox') {
                    if (this.exercice.currentQuestion.defaultShapeChoosed) {
                        this.currentStep = 4;
                        this.steps.specific[4].subtitle4.text = this.getText('step4Subtitle');
                    } else
                        this.currentStep = 3;
                }
                this.exercice.currentQuestion.defaultShapeChoosed = false;
                this.updateShapeSelection(false);
                break;
            case 5:
                this.updateShapeSelection(false);
                this.exercice.currentQuestion.defaultShapeChoosed = false;
                this.currentStep--;
                break;
            default :
                this.currentStep = this.currentStep > 0 ? this.currentStep - 1 : 0;
                break;
        }
        this.loadForm();
    }

    /**
     * Choix de l'étape suivante en fonction de l'étape actuelle;
     */
    chooseNextStep() {
        switch (this.currentStep) {
            case 1:
                if (this.steps.specific[1].french.text != null &&
                    this.steps.specific[1].french.text !== '' &&
                    this.steps.specific[1].german.text != null &&
                    this.steps.specific[1].german.text !== '') {
                    this.currentStep++;
                    this.exercice.name.fr = this.steps.specific[1].french.text;
                    this.exercice.name.de = this.steps.specific[1].german.text;
                    this.exercice.name.en = this.steps.specific[1].english.text;
                    this.exercice.name.it = this.steps.specific[1].italian.text;
                    this.exercice.createQuestion();
                } else
                    Util.getInstance().showAlert('warning', this.alert.noNameText, this.alert.noName);
                break;
            case 2:
                if (this.steps.specific[2].questionType.getActives().length > 0) {
                    this.currentStep = this.steps.specific[2].questionType.getActives()[0].data.nextStep;
                    if (this.exercice.currentQuestion.type !== this.steps.specific[2].questionType.getActives()[0].data.type
                        || this.exercice.currentQuestion.qcmtype !== this.steps.specific[2].questionType.getActives()[0].data.qcmtype)
                        this.exercice.currentQuestion.trueResponses = [];
                    this.exercice.currentQuestion.type = this.steps.specific[2].questionType.getActives()[0].data.type;
                    this.exercice.currentQuestion.qcmtype = this.steps.specific[2].questionType.getActives()[0].data.qcmtype;
                } else
                    Util.getInstance().showAlert('warning', this.alert.noTypeText, this.alert.noType);
                break;
            case 3:
                if (this.steps.specific[3].frenchQuestionName.text != null &&
                    this.steps.specific[3].frenchQuestionName.text !== '' &&
                    this.steps.specific[3].germanQuestionName.text != null &&
                    this.steps.specific[3].germanQuestionName.text !== '') {
                    this.currentStep++;
                } else
                    Util.getInstance().showAlert('warning', this.alert.noNameText, this.alert.noName);

                this.exercice.currentQuestion.label.fr = this.steps.specific[3].frenchQuestionName.text;
                this.exercice.currentQuestion.label.de = this.steps.specific[3].germanQuestionName.text;
                this.exercice.currentQuestion.label.en = this.steps.specific[3].englishQuestionName.text;
                this.exercice.currentQuestion.label.it = this.steps.specific[3].italianQuestionName.text;
                if (this.exercice.currentQuestion.type === 'draw')
                    this.steps.specific[4].subtitle4.text = this.getText('step4Subtitle1');
                this.steps.specific[4].multipleChoice.reset();
                break;
            case 4:
                if (this.exercice.currentQuestion.type === 'draw') {
                    this.exercice.currentQuestion.defaultShapeChoosed = true;
                    this.currentStep = 6;
                } else if (this.exercice.currentQuestion.qcmtype === 'radio')
                    this.currentStep = 5;
                else if (this.exercice.currentQuestion.qcmtype === 'checkbox') {
                    if (this.exercice.currentQuestion.defaultShapeChoosed) {
                        this.currentStep = 6;
                    } else {
                        this.currentStep = 4;
                        this.steps.specific[4].subtitle4.text = this.getText('step4Subtitle2');
                    }
                }
                this.saveShapes(this.steps.specific[4].multipleChoice, this.exercice.currentQuestion.defaultShapeChoosed);
                this.exercice.currentQuestion.defaultShapeChoosed = true;
                this.steps.specific[4].multipleChoice.reset();
                this.updateShapeSelection(true);
                break;
            case 5:
                this.currentStep++;
                this.saveShapes(this.steps.specific[5].singleChoice, this.exercice.currentQuestion.defaultShapeChoosed);
                break;
            case 6:
                this.currentStep = 2;
                this.exercice.endQuestion();
                this.updateShapeSelection(false);
                this.exercice.createQuestion();
                break;
            case 7:
                if (this.steps.specific[7].isNew.isChecked() && this.steps.specific[7].understandable.isChecked())
                    this.currentStep++;
                else
                    Util.getInstance().showAlert('warning', this.alert.checkConditionText, this.alert.checkCondition);
                break;
            default :
                this.currentStep = this.currentStep < 10 ? this.currentStep + 1 : 10;
                break;
        }
        this.loadForm();
        this.prepareResume();
    }

    /**
     * Sauvegarde les formes choisies dans le ToggleGroup dans l'exercice en cours
     * @param {ToggleGroup} toggleGroup Le sélecteur de formes.
     * @param {boolean} isTrueShapes Est-ce que les formes sont les formes correctes.
     */
    saveShapes(toggleGroup, isTrueShapes) {
        let activesShapes = toggleGroup.getActives();
        if (isTrueShapes)
            this.exercice.currentQuestion.trueResponses = [];
        else
            this.exercice.currentQuestion.allResponses = [];
        for (let shape of activesShapes) {
            this.exercice.addResponses(shape.data, isTrueShapes);
        }
    }

    /**
     * Chargement des données de l'exercice en cours dans l'IHM.
     */
    loadForm() {
        if (this.exercice.currentQuestion != null) {
            this.steps.specific[2].questionType.reset();
            if (this.exercice.currentQuestion.type === 'draw')
                this.steps.specific[2].questionType.buttons[2].toggle();
            else if (this.exercice.currentQuestion.type === 'qcm' && this.exercice.currentQuestion.qcmtype === 'radio')
                this.steps.specific[2].questionType.buttons[0].toggle();
            else if (this.exercice.currentQuestion.type === 'qcm' && this.exercice.currentQuestion.qcmtype === 'checkbox')
                this.steps.specific[2].questionType.buttons[1].toggle();


            this.steps.specific[3].frenchQuestionName.text = this.exercice.currentQuestion.label.fr;
            this.steps.specific[3].englishQuestionName.text = this.exercice.currentQuestion.label.en;
            this.steps.specific[3].germanQuestionName.text = this.exercice.currentQuestion.label.de;
            this.steps.specific[3].italianQuestionName.text = this.exercice.currentQuestion.label.it;

            if (this.exercice.currentQuestion.defaultShapeChoosed)
                for (let e of this.exercice.currentQuestion.trueResponses) {
                    for (let e2 of this.steps.specific[4].multipleChoice.buttons)
                        if (e2.data === e.value && e2.isEnabled() && !e2.isActive())
                            e2.toggle();
                    for (let e2 of this.steps.specific[5].singleChoice.buttons)
                        if (e2.data === e.value && e2.isEnabled() && !e2.isActive())
                            e2.toggle();
                }
            else
                for (let e of this.exercice.currentQuestion.allResponses) {
                    for (let e2 of this.steps.specific[4].multipleChoice.buttons)
                        if (e2.data === e.value && e2.isEnabled() && !e2.isActive())
                            e2.toggle();
                    for (let e2 of this.steps.specific[5].singleChoice.buttons)
                        if (e2.data === e.value && e2.isEnabled() && !e2.isActive())
                            e2.toggle();
                }
        }
    }

    /**
     * Mise à jour de la sélection des formes et des formes actives.
     * @param {boolean} disableButton Faut-il désactiver les formes en cours
     */
    updateShapeSelection(disableButton) {
        for (let btn of this.steps.specific[4].multipleChoice.buttons)
            btn.enable();
        for (let btn of this.steps.specific[5].singleChoice.buttons)
            btn.enable();
        if (disableButton) {
            let active = [];
            for (let shape of this.exercice.currentQuestion.allResponses)
                active.push(shape.value);
            for (let btn of this.steps.specific[4].multipleChoice.buttons)
                if (active.indexOf(btn.data) < 0)
                    btn.disable();
            for (let btn of this.steps.specific[5].singleChoice.buttons)
                if (active.indexOf(btn.data) < 0)
                    btn.disable();
        }

    }

    /**
     * Prépare l'interface du résumé de la question dynamiquement.
     */
    prepareResume() {
        if (this.exercice.currentQuestion == null)
            return;
        let currentLanguage = this.refGame.global.resources.getLanguage();

        this.steps.specific[6].questionLabel.text = this.exercice.currentQuestion.label[currentLanguage];
        this.steps.specific[6].questionLabel.x = 300;
        this.steps.specific[6].questionLabel.y = 120 + Math.floor(this.steps.specific[6].questionLabel.height / 2);

        let headerSize = HEADER_SIZE + 20 + this.steps.specific[6].questionLabel.height + 20;

        this.steps.specific[6].allShapeLabel.x = 155;
        this.steps.specific[6].allShapeLabel.y = headerSize;

        this.steps.specific[6].trueShapeLabel.x = 445;
        this.steps.specific[6].trueShapeLabel.y = headerSize;

        headerSize += this.steps.specific[6].trueShapeLabel.height + 15;

        let shapes = '';
        for (let resp of this.exercice.currentQuestion.allResponses) {
            shapes += this.refGame.shapeUtils.getNames(resp.value)[currentLanguage] + '\n';
        }

        this.steps.specific[6].allShape.text = shapes;


        shapes = '';
        for (let resp of this.exercice.currentQuestion.trueResponses) {
            shapes += this.refGame.shapeUtils.getNames(resp.value)[currentLanguage] + '\n';
        }
        this.steps.specific[6].trueShape.text = shapes;


        this.steps.specific[6].trueShape.x = 445;
        this.steps.specific[6].trueShape.y = headerSize + Math.floor(this.steps.specific[6].trueShape.height / 2);

        this.steps.specific[6].allShape.x = 155;
        this.steps.specific[6].allShape.y = headerSize + Math.floor(this.steps.specific[6].allShape.height / 2);

    }

    /**
     * Supprime l'exercice en cours
     */
    deleteExercice() {
        this.currentStep = 0;
        this.generateGame();
        this.show();
        Util.getInstance().showAlert('success', this.alert.confirmDeletionText, this.alert.confirmDeletion);
    }

    /**
     * Enregistrement de l'exercice dans la base de données (appel aux services REST)
     */
    submitExercice() {
        this.refGame.global.resources.saveExercice(this.exercice.toString(), function (result) {
            this.currentStep = 0;
            this.generateGame();
            this.show();
            Util.getInstance().showAlert(result.type, null, result.message);
        }.bind(this));
    }

    /**
     * Début du mode de test de l'exercice en cours
     */
    testExercice() {
        this.hideAll();
        this.refTrainMode.init(false, true, this.exercice.toString(), this);
        this.refTrainMode.show();
    }

    /**
     * Méthode de mise à jour de la police d'écriture
     * @param isOpendyslexic Police spécial pour les dyslexiques
     */
    updateFont(isOpendyslexic) {
        let font = isOpendyslexic ? 'Opendyslexic' : 'Arial';
        let elements = this.extractElements();
        for (let element of Object.keys(elements)) {
            if (elements[element] instanceof Button
                || elements[element] instanceof ToggleButton
                || elements[element] instanceof ToggleGroup
                || elements[element] instanceof CheckBox)
                elements[element].updateFont(font);
            else if (elements[element] instanceof PIXI.Text)
                elements[element].style.fontFamily = font;

        }
    }

     /**
     * Retourne la liste des formes pour le dégrès courrant
     * @return {Array} Les formes disponibles pour le degrès.
     */
    getShapeForCurrentDegree() {
        let degree = this.refGame.global.resources.getDegre();
        if (degree == this.shapes.lastDegree)
            return this.shapes.shapes;
        let shapes = [];
        for (let shape of this.refGame.shapeUtils.getShapes()) {
            if (shape.harmos <= degree)
                shapes.push(shape);
        }
        this.shapes.shapes = shapes;
        this.shapes.lastDegree = degree;
        return shapes;
    }

}
