/**
 * @classdesc IHM de la création des titres d'exercice
 * @author Théo Teixeira
 * @version 0.1
 */
class Titre extends Interface{
    constructor(refGame){
        super(refGame, "titre");
        this.refGame = refGame;
        super.setElements({
            title: new PIXI.Text('Hello',{ fontFamily: 'Arial', fontSize: 32, fill: 0x000000, align: 'center' }),

            lblFR: new PIXI.Text('',{ fontFamily: 'Arial', fontSize: 14, fill: 0x000000, align: 'center' }),
            lblDE: new PIXI.Text('',{ fontFamily: 'Arial', fontSize: 14, fill: 0x000000, align: 'center' }),
            lblEN: new PIXI.Text('',{ fontFamily: 'Arial', fontSize: 14, fill: 0x000000, align: 'center' }),
            lblIT: new PIXI.Text('',{ fontFamily: 'Arial', fontSize: 14, fill: 0x000000, align: 'center' }),

            btnSauver: new Button(300,500,'',0x00AA00, 0xFFFFFF),
            btnRetour: new Button(350,500,'',0x00AA00, 0xFFFFFF),
            btnQuit: new Button(325,550,'',0x00AA00, 0xFFFFFF),
        });

    }

    show() {
        console.log("clear");
        // this.clear();
        this.elements.title.x = 300;
        this.elements.title.y = 20;


        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
    }

    refreshLang(lang){

    }

}