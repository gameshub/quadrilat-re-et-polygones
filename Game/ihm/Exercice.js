/**
 * @classdesc IHM Page de garde d'un exercice
 * @author Vincent Audergon
 * @version 1.0
 */
class Exercice extends Interface {

    /**
     * Constructeur de l'ihm page de garde
     * @param {Game} refGame la référence vers la classe de jeu
     */
    constructor(refGame) {
        super(refGame, "exercice");
        super.setElements({
            title: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 32, fill: 0x000000, align: 'center'}),
            subTitle: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 27, fill: 0x000000, align: 'center'}),
            begin: new Button(300, 300, '', 0x00AA00, 0xFFFFFF),
            info: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 16, fill: 0x00, align: 'left'})
        });
        /** @type {function} fonction de callback appelée au moment du click sur le bouton */
        this.onBegin = function () {
            console.log('Replace this action with exercice.setOnBegin')
        };
        /** @type {boolean} indique si c'est une évaluation ou un exercice */
        this.evaluation = false;
        this.names = null;
    }

    /**
     * Affiche la page de garde
     * @param {string} harmos le niveau harmos
     * @param {boolean} evaluation si il s'agit d'un exercice ou d'une evaluation
     */
    show(harmos, evaluation, names = null) {
        this.clear();

        this.elements.info.x = 0;
        this.elements.info.y = 15;
        this.elements.info.visible = false;
        this.names = names;
        this.evaluation = evaluation;
        this.elements.begin.setOnClick(function () {
            this.onBegin()
        }.bind(this));
        this.elements.title.text = harmos;
        this.elements.title.anchor.set(0.5);
        this.elements.title.x = 300;
        this.elements.title.y = 100;
        this.elements.subTitle.anchor.set(0.5);
        this.elements.subTitle.x = 300;
        this.elements.subTitle.y = 150;
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
    }

    /**
     * Fonction de callback appelée lorsque la langue a été changée.
     * Mets à jour les textes des composants de l'ihm
     * @param {string} lang la nouvelle langue
     */
    refreshLang(lang) {
        this.elements.begin.setText(this.getText('begin'));
        this.elements.subTitle.text = (this.names != null && this.names[lang] != null && this.names[lang] !== '') ? this.names[lang] : (this.evaluation ? this.getText('evaluation') : this.getText('exercice'));

        this.elements.info.text = this.refGame.global.resources.getOtherText('info', {
            mode: (this.evaluation ? this.getText('ModeEvaluer') : this.getText('ModeEntrainer')),
            harmos: this.refGame.global.resources.getDegre(),
            lang: this.refGame.global.resources.getLanguage()
        });
    }

    /**
     * Défini la fonction de callback appelée lorsque le joueur clique sur "commencer".
     * @param {function} onBegin la fonction de callback
     */
    setOnBegin(onBegin) {
        this.onBegin = onBegin;
    }

}