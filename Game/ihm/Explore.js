/**
 * @classdesc IHM Exploration
 * @author Vincent Audergon
 * @version 1.0
 */
class Explore extends Interface {

    /**
     * Constructeur de l'IHM
     * @param {Game} refGame la référence vers la classe de jeu
     */
    constructor(refGame) {
        super(refGame, "explore");
        this.refGame = refGame;
        this.setElements({
            info: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 16, fill: 0x00, align: 'left'})
        });
        /** @type {function} fonction de callback appelée lorsqu'un click est effectué sur une forme */
        this.onClick = function () { console.log('Replace this action with explore.setOnClick') }
        this.poly = 0;
    }

    /**
     * Affiche l'ihm et les différentes formes qui doivent y figurer
     * @param {Shape[]} shapes la liste des formes à afficher dans l'ihm
     */
    show(shapes) {
        this.clear();
        this.elements.info.x = 0;
        this.elements.info.y = 15;
        this.elements.info.visible = false;
        let tailleGrille = shapes.length;
        let trueShapes = JSON.parse(this.refGame.global.resources.getScenario());
        let dbShapes = trueShapes.shapes;
        let width = Math.sqrt(tailleGrille);
        if (!Number.isInteger(width)) width = Math.floor(width) + 1;

        let harmos = this.refGame.global.resources.getDegre();

        for (let s in shapes) {
            let shape = shapes[s];
            if (!shape.id.includes("_") && dbShapes[s].Harmos <= harmos) {
                let graphics = shape.initGraphics(500 / width, 500 / width);
                graphics.x = (s * 500 / width) % 500 + 500 / width / 2;
                graphics.y = (Math.floor(s / width)) * (500 / width) + 500 / width / 2 + 40;
                this.elements[`shape${s}`] = graphics;
                graphics.on('pointertap', function () {
                    this.onClick(shape);
                }.bind(this));
                this.poly++;
            } else {
                let graphics = shape.initGraphics(1 / width, 1 / width);
                graphics.x = (s * 1 / width) % 1 + 1 / width / 2;
                graphics.y = (Math.floor(s / width)) * (1 / width) + 1 / width / 2;
                this.elements[`shape${s}`] = graphics;
                graphics.on('pointertap', function () {
                    this.onClick(shape);
                }.bind(this));
            }
        }
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
    }

    refreshLang(lang) {
        this.elements.info.text = this.refGame.global.resources.getOtherText('info', {
            mode: this.getText('ModeExplorer'),
            harmos: this.refGame.global.resources.getDegre(),
            lang: lang
        });
    }

    /**
     * Défini la fonction de callback appelée lorsqu'un clic est effectué sur un des polygones
     * @param {function} onClick la fonction de callback
     */
    setOnClick(onClick) {
        this.onClick = onClick;
    }

}
