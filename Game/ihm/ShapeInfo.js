/**
 * @classdesc IHM d'informations sur un polygone
 * @author Vincent Audergon
 * @version 1.0
 */
class ShapeInfo extends Interface {

    /**
     * Constructeur de l'ihm d'informations
     * @param {Game} refGame la référence vers la classe de jeu
     */
    constructor(refGame) {
        super(refGame, "shapeInfo");
        super.setElements({
            polygon: new PIXI.Graphics(),
            background: new PIXI.Graphics(),
            lblAngles: new PIXI.Text('Angles', {fontFamily: 'Arial', fontSize: 20, fill: 0x00, align: 'left'}),
            lblParallels: new PIXI.Text('Paralleles', {fontFamily: 'Arial', fontSize: 20, fill: 0x00, align: 'left'}),
            lblSym: new PIXI.Text('Axes de symmétrie', {fontFamily: 'Arial', fontSize: 20, fill: 0x00, align: 'left'}),
            lblCom: new PIXI.Text('||Insert comment here||', {
                fontFamily: 'Arial',
                fontSize: 14,
                fill: 0x00,
                align: 'center'
            }),

            lblBiss: new PIXI.Text('Test', {fontFamily: 'Arial', fontSize: 20, fill: 0x00, align: 'left'}),
            lblSideBiss: new PIXI.Text('Test', {fontFamily: 'Arial', fontSize: 20, fill: 0x00, align: 'left'}),
            lblMed: new PIXI.Text('Test', {fontFamily: 'Arial', fontSize: 20, fill: 0x00, align: 'left'}),

            btnLignes: new Button(100, 240, '', 0xd62f2f, 0xFFFFFF),
            btnBiss: new Button(100, 280, '', 0x33cc33, 0xFFFFFF),
            btnMediane: new Button(100, 320, '', 0x0099ff, 0xFFFFFF),
            btnMediatrice: new Button(100, 360, '', 0xcc00ff, 0xFFFFFF),
            btnAxeSym: new Button(100, 420, '', 0xbf4080, 0xFFFFFF),
            btnGravity: new Button(100, 470, 'Gravi', 0xfaae0a, 0xFFFFFF),

            lblCheck: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 16, fill: 0x000000, align: 'left'}),
            lblRadio: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 16, fill: 0x000000, align: 'left'}),
            lblBtnDraw: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 16, fill: 0x000000, align: 'left'}),

            btnClose: new Button(300, 550, '', 0xd62f2f, 0xFFFFFF),
            title: new PIXI.Text('Test', {fontFamily: 'Arial', fontSize: 30, fill: 0x101010, align: 'center'}),
        });
        this.refGame.global.pixiApp.ticker.add(function () {
            // this.elements.polygon.rotation += 1;
        }.bind(this));
        /** @type {function} fonction de callback appelée lorsqu'un click est effectué sur le bonton "fermer" */
        this.close = undefined;

        /** @type {function} fonction de callback appelée lorsqu'un click est effectué sur le bonton "btnLignes" */
        this.btnLignes = function () {
            console.log('Replace this action with ShapeInfo.setBtnlignes')
        };
        /** @type {function} fonction de callback appelée lorsqu'un click est effectué sur le bonton "btnBiss" */
        this.btnBiss = function () {
            console.log('Replace this action with ShapeInfo.setBtnBissectrice')
        };
        /** @type {function} fonction de callback appelée lorsqu'un click est effectué sur le bonton "btnMediane" */
        this.btnMediane = function () {
            console.log('Replace this action with ShapeInfo.setBtnMediane')
        };
        /** @type {function} fonction de callback appelée lorsqu'un click est effectué sur le bonton "btnMediatrice" */
        this.btnMediatrice = function () {
            console.log('Replace this action with ShapeInfo.setBtnMediatrice')
        };
        /** @type {function} fonction de callback appelée lorsqu'un click est effectué sur le bonton "btnAxeSym" */
        this.btnAxeSym = function () {
            console.log('Replace this action with ShapeInfo.setBtnAxeSym')
        };

        /** @type {Shape} la forme affichée */
        this.shape = undefined;
        this.shapeSecondaire = undefined;

        this.isAll = false;
        this.maxAxeSym = 0;
        this.maxSideBiss = 0;
        this.maxBiss = 0;
        this.maxMed = 0;
        this.gravity = false;

        this.isDraw = false;
        this.secShape = undefined;

        this.nAxeSym = 0;
        this.nSideBiss = 0;
        this.nBiss = 0;
        this.nMediane = 0;
        this.nDiagonal = 0;
        this.gravity = false;

        this.allText = "axesTous";
        this.axeSymText = "axeSym";
        this.medianeText = "medianes";
        this.bissText = "bissectrices";
        this.commentText = "commentDefault";
        this.mediatriceText = "mediatrices";
        this.centreGraviteText = "centreGravite";
    }

    /**
     * Affiche l'ihm d'informations sur le polygone donné.
     * @param {Shape} shape Le polygone dont on affiche les informations
     * @param {PIXI.Sprite} graphics Le sprite du polygone
     * @param {PIXI.Sprite} graphicsSec forme secondaire affichée à l'utilisateur pour exemple.
     */
    show(shape, shapeSec, graphicsSec, graphics, draw) {

        this.maxAxeSym = shape.axeSym.length;
        this.maxSideBiss = shape.sideBisections.length;
        this.maxBiss = shape.bisections.length;
        this.maxMed = shape.mediane.length;
        this.maxDiagonal = shape.diagonal.length;

        if (this.maxAxeSym !== 0) {
            this.axeSymText = "axeSym";
        } else {
            this.axeSymText = "noAxe";
        }

        if (this.maxBiss === 0) this.bissText = "noAxe";

        if (this.maxMed === 0 && this.maxDiagonal === 0)
            this.medianeText = "noAxe";
        else if (this.maxMed !== 0 && this.maxDiagonal === 0)
            this.medianeText = "medianes";
        else if (this.maxMed === 0 && this.maxDiagonal !== 0)
            this.medianeText = "diagonal";


        if (this.maxSideBiss === 0) this.mediatriceText = "noAxe";

        if (this.maxAxeSym === 0 && this.maxBiss === 0 &&
            this.maxMed === 0 && this.maxSideBiss === 0) this.allText = "axesRien";

        if (typeof shape.gravity[0] === 'undefined') this.centreGraviteText = "noAxe";

        this.isDraw = draw;
        this.secShape = shapeSec;
        this.clear();
        this.shape = shape;
        this.shapeSecondaire = graphicsSec;
        this.shapeSecondaire.visible = draw;
        this.elements.background.beginFill(0xe6eeff);
        this.elements.background.lineStyle(4, 0x4d88ff, 1);
        this.elements.background.drawRect(0, 0, 600, 200);
        this.elements.background.endFill();
        this.elements.polygon = graphics;
        this.elements.polygon.x = 375;
        this.elements.polygon.y = 375;
        this.elements.polygon.interactive = false;
        this.elements.polygon2 = graphicsSec;
        this.elements.polygon2.x = 550;
        this.elements.polygon2.y = 400;
        this.elements.polygon2.interactive = false;
        this.elements.title.anchor.set(0.5);
        this.elements.title.x = 300;
        this.elements.title.y = 20;
        this.elements.btnLignes.setOnClick(function () {
            if (this.isAll) {
                this.gravity = false;
                this.nSideBiss = 0;
                this.nMediane = 0;
                this.nAxeSym = 0;
                this.nBiss = 0;
                this.nDiagonal = 0;
                this.allText = "axesTous";
                this.commentText = "commentDefault";
                this.isAll = false;
                this.refreshView();
            } else {
                this.gravity = true;
                this.nSideBiss = -1;
                this.nAxeSym = -1;
                this.nBiss = -1;
                this.nMediane = -1;
                this.nDiagonal = -1
                this.allText = "axesRien";
                this.commentText = "commentDefault";
                this.isAll = true;
                this.refreshView();
            }
        }.bind(this));
        this.elements.btnClose.setOnClick(function () {
            this.isAll = false;
            this.gravity = false;
            this.maxMed = 0;
            this.maxBiss = 0;
            this.maxAxeSym = 0;
            this.maxSideBiss = 0;
            this.nSideBiss = 0;
            this.nMediane = 0;
            this.nAxeSym = 0;
            this.nBiss = 0;
            this.secShape = undefined;

            this.allText = "axesRien";
            this.axeSymText = "axeSym";
            this.medianeText = "medianes";
            this.bissText = "bissectrices";
            this.commentText = "commentDefault";
            this.mediatriceText = "mediatrices";
            this.centreGraviteText = "centreGravite";

            this.elements.btnBiss.setText("");
            this.elements.btnLignes.setText("");
            this.elements.btnAxeSym.setText("");
            this.elements.btnMediane.setText("");
            this.elements.btnMediatrice.setText("");
            this.close();
        }.bind(this));
        this.elements.btnBiss.setOnClick(function () {
            if (this.maxBiss !== 0) {
                if (this.nBiss === this.maxBiss) {
                    this.nBiss = -1;
                    this.commentText = "commentBissectrice";
                    this.refreshView();
                } else {
                    this.commentText = "commentBissectrice";
                    this.nBiss++;
                    this.refreshView();
                }
            } else {
                this.bissText = "noAxe";
            }

        }.bind(this));
        this.elements.btnMediane.setOnClick(function () {
            if (this.maxMed !== 0) {
                if (this.nMediane === this.maxMed) {
                    this.nMediane = -1;
                    if (shape.sides.count === 3) {
                        this.commentText = "commentMedianeTriangle";
                    } else {
                        this.commentText = "commentMedianeQuad";
                    }
                    this.refreshView();
                } else {
                    this.nMediane++;
                    if (shape.sides.count === 3) {
                        this.commentText = "commentMedianeTriangle";
                    } else {
                        this.commentText = "commentMedianeQuad";
                    }
                    this.refreshView();
                }
            } else if (this.maxDiagonal !== 0) {
                if (this.nDiagonal === this.maxDiagonal) {
                    this.nDiagonal = -1;
                    this.commentText = "commentDiagonalQuad";
                    this.refreshView();
                } else {
                    this.nDiagonal++;
                    this.commentText = "commentDiagonalQuad";
                    this.refreshView();
                }
            } else {
                this.medianeText = "noAxe";
            }
        }.bind(this));
        this.elements.btnMediatrice.setOnClick(function () {
            if (this.maxSideBiss !== 0) {
                if (this.nSideBiss === this.maxSideBiss) {
                    this.nSideBiss = -1;
                    this.commentText = "commentMediatrice";
                    this.refreshView();
                } else {
                    this.nSideBiss++;
                    this.commentText = "commentMediatrice";
                    this.refreshView();
                }
            } else {
                this.mediatriceText = "noAxe";
            }

        }.bind(this));
        this.elements.btnAxeSym.setOnClick(function () {
            if (this.maxAxeSym !== 0) {
                if (this.nAxeSym === this.maxAxeSym) {
                    this.nAxeSym = -1;
                    this.commentText = "commentAxeDeSymetrie";
                    this.refreshView();
                } else {
                    this.nAxeSym++;
                    this.commentText = "commentAxeDeSymetrie";
                    this.refreshView();
                }
            } else {
                this.axeSymText = "noAxe";
            }

        }.bind(this));
        this.elements.btnGravity.setOnClick(function () {
            if (typeof shape.gravity[0] !== 'undefined') {
                if (this.gravity) {
                    this.gravity = false;
                    this.commentText = "gravi";
                    this.refreshView();
                } else {
                    this.gravity = true;
                    this.commentText = "gravi";
                    this.refreshView();
                }
            } else {
                this.centreGraviteText = "noAxe";
                this.refreshView();
            }
        }.bind(this));
        this.elements.lblAngles.x = 10;
        this.elements.lblAngles.y = 36;
        this.elements.lblParallels.x = 10;
        this.elements.lblParallels.y = 65;
        this.elements.lblSym.x = 10;
        this.elements.lblSym.y = 90;
        this.elements.lblBiss.x = 10;
        this.elements.lblBiss.y = 115;
        this.elements.lblSideBiss.x = 10;
        this.elements.lblSideBiss.y = 140;
        this.elements.lblMed.x = 10;
        this.elements.lblMed.y = 165;
        this.elements.lblCom.anchor.set(0.5);
        this.elements.lblCom.x = 300;
        this.elements.lblCom.y = 510;
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
    }

    /**
     * Fonction de callback appelée lorsque la langue a été changée.
     * Mets à jour les textes des composants de l'ihm
     * @param {string} lang la nouvelle langue
     */
    refreshLang(lang) {
        if (this.shape) {
            if (this.shape.description) {
                //Override description
                this.elements.lblParallels.text = this.shape.description;
                //Setup mulitline mode
                this.elements.lblParallels.style.wordWrap = true;
                this.elements.lblParallels.style.wordWrapWidth = 550;

                this.elements.lblSym.text = "";
                this.elements.lblBiss.text = "";
                this.elements.lblSideBiss.text = "";
                this.elements.lblMed.text = "";
            } else {
                //clear mulitline mode
                this.elements.lblParallels.style.wordWrap = false;

                this.elements.lblSym.text = this.shape.axeSym.length === 0 ? this.getText('nosymAxis') : `${this.shape.axeSym.length} ${this.getText('symAxis')}`;
                this.elements.lblParallels.text = this.shape.nPaireCotePara === 0 ? this.getText('noparallel') : `${this.shape.nPaireCotePara} ${this.getText('parallels')}`;
                this.elements.lblBiss.text = this.shape.bisections.length === 0 ? this.getText('nobissectrice') : `${this.shape.bisections.length} ${this.getText('bissectrice')}`;
                this.elements.lblSideBiss.text = this.shape.sideBisections.length === 0 ? this.getText('nomediatrice') : `${this.shape.sideBisections.length} ${this.getText('mediatrice')}`;
                this.elements.lblMed.text = this.shape.mediane.length === 0 ? this.getText('nomediane') : `${this.shape.mediane.length} ${this.getText('mediane')}`;
            }
            if (this.shape) this.elements.title.text = this.shape.getName(lang);
            let droit = 0;
            let text = `${this.getText('angles')} :`;
            text += this.angleText();
            this.elements.lblAngles.text = this.shape.displayAngle ? text : '';

            this.elements.lblCom.text = this.getText(this.commentText);
            this.elements.btnLignes.setText(`${this.getText(this.allText)}`);

            if (this.bissText !== "noAxe") {
                if (this.nBiss !== -1) {
                    this.elements.btnBiss.setText(`${this.getText(this.bissText) + " " + this.nBiss + "/" + this.maxBiss}`);
                } else {
                    this.elements.btnBiss.setText(`${this.getText(this.bissText) + " " + this.getText("tout")}`);
                }
            } else {
                this.elements.btnBiss.setText(`${this.getText(this.bissText)}`);
            }

            if (this.axeSymText !== "noAxe") {
                if (this.nAxeSym !== -1) {
                    this.elements.btnAxeSym.setText(`${this.getText(this.axeSymText) + " " + this.nAxeSym + "/" + this.maxAxeSym}`);
                } else {
                    this.elements.btnAxeSym.setText(`${this.getText(this.axeSymText) + " " + this.getText("tout")}`);
                }
            } else {
                this.elements.btnAxeSym.setText(`${this.getText(this.axeSymText)}`);
            }

            if (this.medianeText !== "noAxe") {
                if (this.maxMed !== 0) {
                    if (this.nMediane !== -1) {
                        this.elements.btnMediane.setText(`${this.getText(this.medianeText) + " " + this.nMediane + "/" + this.maxMed}`);
                    } else {
                        this.elements.btnMediane.setText(`${this.getText(this.medianeText) + " " + this.getText("tout")}`);
                    }
                } else {
                    if (this.nDiagonal !== -1) {
                        this.elements.btnMediane.setText(`${this.getText(this.medianeText) + " " + this.nDiagonal + "/" + this.maxDiagonal}`);
                    } else {
                        this.elements.btnMediane.setText(`${this.getText(this.medianeText) + " " + this.getText("tout")}`);
                    }
                }
            } else {
                this.elements.btnMediane.setText(`${this.getText(this.medianeText)}`);
            }

            if (this.mediatriceText !== "noAxe") {
                if (this.nSideBiss !== -1) {
                    this.elements.btnMediatrice.setText(`${this.getText(this.mediatriceText) + " " + this.nSideBiss + "/" + this.maxSideBiss}`);
                } else {
                    this.elements.btnMediatrice.setText(`${this.getText(this.mediatriceText) + " " + this.getText("tout")}`);
                }
            } else {
                this.elements.btnMediatrice.setText(`${this.getText(this.mediatriceText)}`);
            }

            if (this.centreGraviteText !== "noAxe") {
                if (this.gravity) {
                    this.elements.btnGravity.setText(`${this.getText(this.centreGraviteText) + " ON"}`);
                } else {
                    this.elements.btnGravity.setText(`${this.getText(this.centreGraviteText) + " OFF"}`);
                }
            } else {
                this.elements.btnGravity.setText(`${this.getText(this.centreGraviteText)}`);
            }
        }
        this.elements.btnClose.setText(this.getText('close'));
    }

    refreshView() {
        if (this.isDraw) {
            this.refGame.explorationMode.displayShapeInfo(this.shape, this.isDraw, true, this.nBiss, this.nMediane, this.nSideBiss, this.nAxeSym, true, this.secShape, this.gravity);
        } else {
            this.refGame.explorationMode.displayShapeInfo(this.shape, this.isDraw, true, this.nBiss, this.nMediane, this.nSideBiss, this.nAxeSym, true, undefined, this.gravity, this.nDiagonal);
        }
    }

    /**
     * Défini la fonction de callback appelée lorsque l'utilisateur clique sur "fermer"
     * @param {function} onClose la fonction de callback
     */
    setOnClose(onClose) {
        this.close = onClose;
    }

    setBtnlignes(ligne) {
        this.btnLignes = ligne;
    }

    setBtnBissectrice(biss) {
        this.btnBiss = biss;
    }


    setBtnMediane(med) {
        this.btnMediane = med;
    }

    setBtnMediatrice(med) {
        this.btnMediatrice = med;
    }

    setBtnAxeSym(sym) {
        this.btnAxeSym = sym;
    }

    angleText() {
        let degreeSymbol = String.fromCharCode(176);
        let resultText = "";
        let angleMap = new Map();
        let rightAngle = 0;
        for (let a in this.shape.angles) {
            let angle = this.shape.angles[a];
            if (angleMap.get(angle.alpha))
                angleMap.get(angle.alpha).push(a);
            else
                angleMap.set(angle.alpha, [a]);
            if (angle.alpha === 90)
                rightAngle++;
        }
        for (let value of angleMap.keys()) {
            resultText += " ";
            for (let name of angleMap.get(value)) {
                resultText += name + " = ";
            }
            resultText += value + degreeSymbol;
        }
        //  resultText += rightAngle > 0 ? ` (${rightAngle} ${this.getText('rightAngles')})` : '';
        return resultText;
    }
}
