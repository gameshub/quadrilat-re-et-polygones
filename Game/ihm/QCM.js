
/** Constante définissant le mode radio du QCM */
const QCM_TYPE_RADIO = "radio";
/** Constante définissant le mode choix multiples du QCM */
const QCM_TYPE_CHECK = "checkbox";

/**
 * @classdesc IHM Questions choix multiples
 * @author Vincent Audergon
 * @version 1.0
 */
class QCM extends Interface {

    /**
     * Constructeur de l'ihm QCM
     * @param {Game} refGame la référence vers la classe de jeu
     */
    constructor(refGame) {
        super(refGame, "qcm");
        super.setElements({
            validateButton: new Button(300, 550, '', 0x00AA00, 0xFFFFFF,true)
        });
        /** @type {function} fonction de callback appelée lorsque la réponse est validée */
        this.onResponse = undefined;
        /** @type {string[]} la liste des réponses */
        this.responses = [];
        /** @type {string} le type de QCM */
        this.qcmtype;
    }

    /**
     * Affiche l'ihm ainsi que les réponses possibles de la question
     * @param {Question} question la question à afficher
     */
    show(question) {
        for (let el in this.elements){
            if(el.startsWith('shape'))
                this.elements[el].visible = false;
        }
        this.clear();
        this.responses = [];
        this.qcmtype = question.qcmtype;
        let shapes = [];
        for (let id of question.responses) {
            shapes.push(this.refGame.shapeUtils.getShapeById(id.value));
        }
        for (let id of question.traps) {
            shapes.push(this.refGame.shapeUtils.getShapeById(id.value));
        }
        shapes = this.shuffle(shapes);
        let gridHeight = 500 / shapes.length;
        for (let s in shapes) {
            let shape = shapes[s];
            let element = this.elements[`shape${s}`] = shape.initGraphics(600, gridHeight);
            element.x = 600 / 2;
            element.y = gridHeight * s + gridHeight / 2;
            element.value = shape.id;
            element.interactive = true;
            element.alpha = 0.5;
            element.on('pointerdown', function () {
                this.onClick(element);
            }.bind(this));
            this.responses.push(element);
        }
        this.elements.validateButton.setOnClick(function(){
            this.onResponse(this.getSelectedResponses());
        }.bind(this));
        this.refreshLang();
        this.init();
    }

    /**
     * Mélange un tableau aléatoirement
     * @param {Array} a le tableau à mélanger
     */
    shuffle(a) {
        let j, x, i;
        for (i = a.length - 1; i > 0; i--) {
            j = Math.floor(Math.random() * (i + 1));
            x = a[i];
            a[i] = a[j];
            a[j] = x;
        }
        return a;
    }

    /**
     * Retournes les résponses que l'utilisateur a séléctionné
     * @return {string[]} les réponses
     */
    getSelectedResponses(){
        let values = [];
        for (let response of this.responses){
            if (response.selected) values.push(response.value);
        }
        return values;
    }

    /**
     * Fonction de callback appelée lorsqu'un clic est effectué sur une réponse
     * @param {PIXI.Sprite} element l'élément sur lequel on a cliqué
     */
    onClick(element) {
        if (this.qcmtype === QCM_TYPE_CHECK){
            element.alpha = element.alpha === 1 ? 0.5 : 1;
            element.selected = element.alpha === 1;
        }else if (this.qcmtype === QCM_TYPE_RADIO) {
            for (let el of this.responses){
                el.alpha = 0.5,
                    el.selected = false;
            }
            element.alpha = 1;
            element.selected = true;
        }
    }

    /**
     * Fonction de callback appelée lorsque la langue a été changée.
     * Mets à jour le texte du bouton "valider"
     * @param {string} lang la nouvelle langue
     */
    refreshLang(lang){
        this.elements.validateButton.setText(this.getText('validate'));
    }

    /**
     * Défini la fonction de callback appele lorsque l'utilisateur valide sa réponse
     * @param {function} onResponse la fonction de callback
     */
    setOnResponse(onResponse) {
        this.onResponse = onResponse;
    }

}
