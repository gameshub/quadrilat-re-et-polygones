/**
 * @classdesc Gestionnaire de question
 * @author Vincent Audergon
 * @version 1.0
 */
class QuestionUtils {

    /**
     * Constructeur du gestionnaire de questions
     */
    constructor() {
        /** @type {Question[]} la liste des questions */
        this.questions = [];

        this.exerciceNames = null;
        /** @type {int} l'index de la question actuelle */
        this.cqIndex = -1;
    }

    /**
     * Vérifie la réponse donnée à la question courrante
     * @param {string[]} shapes La ou les réponses données (tableau d'id de formes)
     * @return {boolean} si la réponse est correcte
     */
    checkResponse(shapes, ignoreNumber = false) {
        let result = false;
        let question = this.questions[this.cqIndex];
        if (shapes.length === question.responses.length) {
            let correct = 0;
            for (let res of shapes) {
                for (let cor of question.responses) {
                    if (res === cor.value) {
                        correct++;
                        break;
                    }
                }
            }
            if (correct === question.responses.length) {
                result = true;
            }
            if(ignoreNumber && (correct >=1))
                result=true;
        }
        return result;
    }

    getExerciceNames() {
        return this.exerciceNames;
    }

    /**
     * Transforme un exercice en liste de questions
     * @param {JSON} exercice L'exercice obtenu de la DB
     * @return {boolean} Si l'opération s'est bien passé et qu'il y a au minimum 1 question
     */
    loadExercice(exercice) {
        let ok = false;
        this.questions = [];
        if (exercice.questions) {
            for (let question of exercice.questions) {
                let responses = [];
                let traps = [];
                for (let response of question.responses) {
                    if (response.response) {
                        responses.push(response);
                    } else {
                        traps.push(response);
                    }
                }
                this.questions.push(new Question(question.label, responses, traps, question.type === "qcm" ? 0 : 1, question.qcmtype));
            }
            if (this.questions.length > 0) ok = true;
        }
        if (exercice.name) {
            this.exerciceNames = exercice.name;
        }
        return ok;
    }

    /**
     * Retourne la prochaine question ou null si il n'y en a plus
     * @return {Question} la prochaine {@link Question}
     */
    nextQuestion() {
        this.cqIndex++;
        let question = null;
        if (this.questions.length > this.cqIndex) {
            question = this.questions[this.cqIndex];
        }
        return question;
    }

    /**
     * Indique si la liste de questions est terminée
     * @return {boolean} si la liste est terminée
     */
    isFinished() {
        return (this.cqIndex + 1) >= this.questions.length;
    }

    /**
     * Commence la liste de question
     */
    start() {
        this.cqIndex = -1;
        this.shuffle();
    }

    /**
     * Mélange aléatoirement la liste de questions
     */
    shuffle() {
        let j, x, i;
        for (i = this.questions.length - 1; i > 0; i--) {
            j = Math.floor(Math.random() * (i + 1));
            x = this.questions[i];
            this.questions[i] = this.questions[j];
            this.questions[j] = x;
        }
    }

}
