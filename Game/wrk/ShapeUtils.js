/**
 * @classdesc Classe de gestion des polygones
 * @author Vincent Audergon
 * @version 1.0
 */
class ShapeUtils {

    /**
     * Constructeur du gestionnaire de polygones
     */
    constructor() {
        /** @type {Shape[]} les formes chargées dans le worker */
        this.shapes = [];
        /** @type {string[]} le nom des formes dans les différentes langues */
        this.names = [];
    }

    /**
     * Identifie une forme par rapport aux formes chargées avec la fonction loadShapes
     * @param {Shape} shape La forme a identifier
     * @return {JSON} Les noms de la forme et l'id de la forme ({id,names})
     * @example
     *          let shapeUtils = new ShapeUtils();
     *          let {id,names} = shapeUtils.identifyShape(shape);
     */
    identifyShape(shape) {
        let id = null;
        let names = null;
        let res = null;
        let vectors = shape.vectors;

        switch (shape.sides.count) {
            case 3:
                let vector_1 = vectors["a"];
                let vector_2 = vectors["b"];
                let vector_3 = vectors["c"];

                res = this.calcTriangle(vector_1, vector_2, vector_3, shape);
                id = this.getId(res);
                names = this.getNames(id);

                break;
            case 4:
                let vector_14 = vectors["a"];
                let vector_24 = vectors["b"];
                let vector_34 = vectors["c"];
                let vector_44 = vectors["d"];

                res = this.calcQuad(vector_14, vector_24, vector_34, vector_44, shape);
                id = this.getId(res);
                names = this.getNames(id);

                break;
            case 5:
                res = this.isPentagone(vectors);
                id = this.getId(res);
                names = this.getNames(id);
                break;
            case 6:
                id = this.getId("hexagone");
                names = this.getNames(id);
                break;
            default:
                id = this.getId("autre_poly");
                names = this.getNames(id);
                break;
        }
        return {id: id, names: names};
    }

    getId(id) {
        let stringIDs = new StringNames;
        let ids = stringIDs.getIDs();
        let res = "";
        for (let i = 0; i < ids.length; i++) {
            if (id === ids[i].name) {
                res = ids[i].id;
                break;
            }
        }
        return res;
    }

    isPentagone(vectors){
        //TODO:Test des angles
        return "pentagone";

    }

    calcTriangle(vector_1, vector_2, vector_3, shape) {
        let nAngleDroit = this.countRightAngles(shape);
        let rectangle = false;
        let isocele = false;
        let equilat = false;
        let res = "";

        if (nAngleDroit === 1) rectangle = true;

        if (vector_1.norme === vector_2.norme && vector_1.norme === vector_3.norme) equilat = true;

        if (vector_1.norme === vector_2.norme || vector_1.norme === vector_3.norme
            || vector_3.norme === vector_2.norme) isocele = true;

        if (!rectangle && isocele && !equilat) {
            res = "triangle_isocele";
        } else if (rectangle && !isocele && !equilat) {
            res = "triangle_rectangle";
        } else if (rectangle && isocele && !equilat) {
            res = "triangle_isocele_rectangle";
        } else if (!rectangle && isocele && equilat) {
            res = "triangle_equilateral";
        } else {
            res = "triangle";
        }

        return res;

    }

    calcQuad(vector_14, vector_24, vector_34, vector_44, shape) {
        let nAngleDroit = this.countRightAngles(shape);
        let resC = "quadrilatere_irregulier";
        if (nAngleDroit === 4) {
            if (vector_14.norme === vector_24.norme
                && vector_34.norme === vector_44.norme
                && vector_34.norme === vector_14.norme) {
                if (!shape.id.includes("_")) {
                    resC = "carre";
                } else {
                    resC = "carre1";
                }
            } else {
                if (!shape.id.includes("_")) {
                    resC = "rectangle";
                } else {
                    resC = "rectangle1";
                }
            }
        } else if (nAngleDroit === 2) {
            resC = "trapeze_rectangle"
        } else if (vector_14.direction === vector_34.direction
            && vector_24.norme === vector_44.norme
            && vector_14.norme === vector_34.norme
            && vector_24.direction === vector_44.direction) {
            if(shape.symAxis >= 1){
                resC = "losange";
            } else {
                resC = "parallelogramme";
            }
        } else if ((vector_14.direction === vector_34.direction
            && vector_24.norme === vector_44.norme)
            || (vector_24.direction === vector_44.direction
                && vector_14.norme === vector_34.norme)) {
            if (shape.symAxis >= 1) {
                resC = "trapeze_isocele";
            } else {
                resC = "trapeze";
            }
        } else if (vector_14.norme === vector_24.norme && vector_34.norme === vector_44.norme
            || vector_14.norme === vector_44.norme && vector_24.norme === vector_34.norme) {
            let a1 = VectorUtils.calcAngle(vector_14, vector_24, false);
            let a2 = VectorUtils.calcAngle(vector_24, vector_34, false);
            let a3 = VectorUtils.calcAngle(vector_34, vector_44, false);
            let a4 = VectorUtils.calcAngle(vector_44, vector_14, false);
            if (a1 <= 30 && a3 <= 30 || a2 <= 30 && a4 <= 30) {
                resC = "pointe_de_fleche";
            } else {
                resC = "cerf-volant";
            }
        }
        return resC;
    }

    getNames(id) {
        let names = null;
        for (let i = 0; i < this.shapes.length; i++) {
            if (this.shapes[i].id === id) {
                names = this.shapes[i].names;
                break;
            }
        }
        return names;
    }

    /**
     * Compte le nombre d'angles droits d'un polygone
     * @param {Shape} shape le polygone
     * @return {int} le nombre d'angles droits
     */
    countRightAngles(shape) {
        let rights = 0;
        for (let a in shape.angles) {
            let angle = shape.angles[a];
            if (angle.alpha === 90) rights++;
        }
        return rights;
    }

    /**
     * Créer et calcule les propriété des formes contenues dans un JSON.
     * Stocke ensuite les formes et les considère comme étant les formes de références.
     * @param {Shape[]} shapes La liste des formes à charger
     * @return {boolean} Si les formes on bien été chargées
     */
    loadShapes(shapes) {
        let ok = false;
        if (shapes) {
            this.shapes = [];
            for (let shape of shapes) {
                let origin = new Point(shape.points[0].x, shape.points[0].y);
                let s = new Shape(500, 500, shape.points, 50, shape.id, shape.names, origin, shape.Harmos, shape.displayAngle, shape.description);
                this.initShape(s);
                this.shapes.push(s);
            }
            ok = true;
        }
        return ok;
    }

    /**
     * Initialise un polygone et calcule ses propriétés
     * @param {Shape} shape Le polygone à initialiser
     */
    initShape(shape) {
        shape.init();
    }

    /**
     * Retourne un polygone en fonction de l'id donné
     * @param {string} id l'id
     * @return {Shape} le polygone ou undefined si aucun n'est trouvés
     */
    getShapeById(id) {
        for (let shape of this.shapes) {
            if (shape.id === id) return shape;
        }
        return undefined;
    }

    /**
     * Retourne la liste des polygones chargés
     * @return {Shape[]} la liste des polygones
     */
    getShapes() {
        return this.shapes;
    }


}