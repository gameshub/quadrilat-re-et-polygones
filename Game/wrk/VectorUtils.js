/**
 * @classdesc classe statique de calculs vectoriels
 * @author Vincent Audergon
 * @version 1.0
 */
class VectorUtils {

    /**
     * Calcule la norme d'un vecteur
     * @param {Vector} v1 le {@link Vector}
     * @return {double} la norme du vecteur
     */
    static calcNorme(v1) {
        return Math.sqrt(Math.pow(v1.getTrueX(), 2) + Math.pow(v1.getTrueY(), 2));
    }

    /**
     * Calcule le produit scalaire de deux vecteur
     * @param {Vector} v1 Le premier {@link Vector}
     * @param {Vector} v2 Le deuxième {@link Vector}
     * @return {double} Le produit scalaire
     */
    static calcScalarProduct(v1, v2) {
        return v1.getTrueX() * v2.getTrueX() + v1.getTrueY() * v2.getTrueY();
    }

    /**
     * Calcule l'angle entre deux vecteurs
     * @param {Vector} v1 Le premier {@link Vector}
     * @param {Vector} v2 Le deuxième {@link Vector}
     * @param {boolean} rad Si l'angle doit être rendu en radiants (degrés par défaut)
     * @return {double} l'angle en degrés
     */
    static calcAngle(v1, v2, rad) {
        let cos = VectorUtils.calcScalarProduct(v1, v2) / (v1.norme * v2.norme);
        cos = -cos;
        let angle = Math.acos(cos);
        let angleDeg = angle * (180 / Math.PI);

        return rad ? angle : Math.round((angleDeg) * 10) / 10;
    }

    /**
     * Indique si deux vecteur sont parallèles (colinéaire)
     * @return {boolean} si les vecteurs sont colinéaires
     */
    static areParallel(v1, v2) {
        return v1.direction === v2.direction;
    }

    /**
     * Retourne la bissectrice de l'angle entre deux vecteurs
     * @param {Vector} angle Le premier {@link Vector}
     * @param {Vector} v Le deuxième {@link Vector}
     * @return {Vector} Un vecteur qui indique la direction de la bissectrice
     */
    static calcBisection(angle, v1, v2) {
        // let a = (angle.alpha / 2 + (v1.direction === 0 ? 180 : v2.direction)); //uniquement si y < 0 ?
        let a = null;
        let res = null;

        if (v2.direction < 180){
            let a = angle.alpha/2;
        } else if (v2.direction > 180) {
            let a = angle.beta/2;
        }

        a *= (Math.PI / 180);
        let bisection = new Line(a, v2.offset);
        return bisection;
    }

    /**
     * Retourne la médiatrice d'un côté
     * @param {Vector} v1 Le {@link Vector}
     * @return {Line} Une droite qui correspond à la médiatrice
     */
    static calcSideBisection(v1) {
        let direction = v1.fullDirection;
        direction *= (Math.PI / 180); //Deg => rad
        let sbis = new Line(direction + Math.PI / 2, new Point(v1.offset.x + (v1.norme * Math.cos(direction)) / 2, v1.offset.y + (v1.norme * Math.sin(direction) / 2)));
        return sbis;
    }

    /**
     * Vérifie si un vecteur se trouve dans un quadrant (de I à IV) en considérant son origine en 0,0
     * @param {Vector} v1 Le {@link Vector}
     * @param {int} number Le numéro du quadrant (1-4)
     * @return {boolean} Si le {@link Vector} se trouve dans le quadrant
     */
    static isInQ(v1, number) {
        switch (number) {
            case 1:
                return v1.way.x > 0 && v1.way.y > 0; //x+, y+1
            case 2:
                return v1.way.x < 0 && v1.way.y > 0; //x-, y+
            case 3:
                return v1.way.x < 0 && v1.way.y < 0; //x-, y-
            case 4:
                return v1.way.x > 0 && v1.way.y < 0; //x+, y-
            default:
                return false;
        }
    }


    /**
     * Retourne la direction d'un vecteur (entre 180° et 0°)
     * @param v1 Le {@link Vector}
     * @return {Vector} La direction sous forme de {@link Vector}
     */
    static calcDirection(v1) {
        let direction = (180 / Math.PI) * Math.atan(v1.getTrueY() / v1.getTrueX());
        if (VectorUtils.isInQ(v1, 4) || VectorUtils.isInQ(v1, 2)) direction += 180;
        return Math.round((direction % 180) * 10) / 10;
    }


    /**
     * Retourne la direction d'un vecteur
     * @param {Vector} v1 Le {@link Vector}
     * @return {Vector} La direction sous forme de {@link Vector}
     */
    static calcFullDirection(v1) {
        let direction = VectorUtils.calcDirection(v1);
        if (direction === 0 && v1.way.x < 0) direction += 180;
        if (v1.way.y < 0) direction += 180
        return direction;
    }

    /**
     * Calcule le centre de gravité d'un polygone
     * @param {Point[]} points les {@link Point} qui composent le polygone
     * @return {Point} Le {@link Point} de gravité
     */
    static calcGravityPoint(points) {
        let sum = { x: 0, y: 0 };
        for (let point of points) {
            sum.x += point.x;
            sum.y += point.y;
        }
        return new Point(sum.x / points.length, sum.y / points.length);
    }

    /**
     * Vérifie si une droite est l'axe de symétrie d'un polygone.
     * @param {Line} l La droite à vérifier
     * @param {Point} g Le centre de gravité d'un polygone
     * @return {boolean} si l'axe est un axe de symétrie
     */
    static isSymAxis(l, g) {
        return l.containsPoint(g);
    }
}
