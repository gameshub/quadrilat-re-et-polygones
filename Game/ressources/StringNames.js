class StringNames {
    constructor() {
        this.CARRE = "CARR";
        this.RECTANGLE = "RECT";
        this.TRAPEZE = "TRAN";
        this.TRAPEZE_RECTANGLE = "TRAR";
        this.TRAPEZE_ISOCELE = "TRAI";
        this.PARALLELOGRAMME = "PARA";
        this.LOSANGE = "LOSA";
        this.TRIANGLE = "TRIN";
        this.TRIANGLE_ISOCELE = "TRII";
        this.TRIANGLE_RECTANGLE = "TRIR";
        this.TRIANGLE_ISOCELE_RECTANGLE = "TRRI";
        this.TRIANGLE_EQUILATERAL = "TRIE";
        this.CERF_VOLANT = "CERF";
        this.POINTE_DE_FLECHE = "FLEC";
        this.QUADRILATERE_IRR = "QUAD";
        this.PENTAGONE = "PENT";
        this.HEXAGONE = "HEXA";
        this.AUTRE_POLY = "POLY";

        this.ids = [{name: "carre", id: this.CARRE}, {name: "rectangle", id: this.RECTANGLE},
            {name: "trapeze", id: this.TRAPEZE},
            {name: "trapeze_rectangle", id: this.TRAPEZE_RECTANGLE},
            {name: "trapeze_isocele", id: this.TRAPEZE_ISOCELE},
            {name: "parallelogramme", id: this.PARALLELOGRAMME},
            {name: "triangle_isocele", id: this.TRIANGLE_ISOCELE}, {
                name: "triangle_rectangle",
                id: this.TRIANGLE_RECTANGLE
            },
            {name: "triangle", id: this.TRIANGLE},
            {name: "losange", id: this.LOSANGE},
            {name: "triangle_isocele_rectangle", id: this.TRIANGLE_ISOCELE_RECTANGLE},
            {
                name: "triangle_equilateral",
                id: this.TRIANGLE_EQUILATERAL
            },
            {name: "cerf-volant", id: this.CERF_VOLANT},
            {name: "pointe_de_fleche", id: this.POINTE_DE_FLECHE},
            {name: "quadrilatere_irregulier", id: this.QUADRILATERE_IRR},
            {name: "pentagone", id: this.PENTAGONE},
            {name: "hexagone", id: this.HEXAGONE},
            {name: "autre_poly", id: this.AUTRE_POLY}];
    }

    getIDs() {
        return this.ids;
    }

}