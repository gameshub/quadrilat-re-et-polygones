class ExerciceForSave {
    constructor() {
        this.name = {
            fr:'',
            de:'',
            en:'',
            it:''
        };
        this.degree = null;
        this.questions = [];
        this.currentQuestion = null;

        ExerciceForSave.prototype.toString = function () {
            let json = {
                name:this.name,
                degree:this.degree,
                questions:this.questions
            };
            return JSON.stringify(json);
        }
    }

    createQuestion(){
        this.currentQuestion = {
            label:{
                fr:'',
                de:'',
                en:'',
                it:''
            },
            allResponses:[],
            trueResponses:[],
            type:null,
            qcmtype:null,
            defaultShapeChoosed:false
        }
    }

    addResponses(shape, good){
        if(this.currentQuestion == null)
            throw "There no current question !";
        if(good)
            this.currentQuestion.trueResponses.push({
                value:shape,
                response:good
            });
        else
            this.currentQuestion.allResponses.push({
                value:shape,
                response:good
            });
    }

    endQuestion(){
        this.questions.push({
            label:this.currentQuestion.label,
            responses:this.getResponses(),
            type:this.currentQuestion.type,
            qcmtype:this.currentQuestion.qcmtype
        });
    }

    getResponses(){
        let responses = [];
        let ids = [];
        for (let response of this.currentQuestion.allResponses){
            ids.push(response.value);
            responses.push(response);
        }
        for (let response of this.currentQuestion.trueResponses){
            if(ids.indexOf(response.value) > -1){
                responses[ids.indexOf(response.value)].response = true;
            } else {
                responses.push(response)
            }
        }
        return responses;
    }


}