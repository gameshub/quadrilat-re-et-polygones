/**
 * @classdesc Bean shape, représente un polygone
 * @author Vincent Audergon
 * @version 2.0
 */
class Shape {

    /**
     * Constructeur de l'objet Shape
     * @param {double} x La coordonnée x du polygone sur le canvas
     * @param {double} y La coordonnée y du polygone sur le canvas
     * @param {Point[]} pts La liste des {@link Point} qui composents le polygone
     * @param {double} scale La taille de la forme sur le canvas
     * @param {string} id L'id du polygone
     * @param {string[]} names Le nom de la formes dans toutes les langues
     * @param {Point} [origin={x:0,y:0}] Le {@link Point} d'origine du polygone (facultatif, 0;0 par défaut)
     * @param {number} harmos Degré harmos
     */
    constructor(x, y, pts, scale, id, names, origin, harmos, displayAngle,description) {
        /** @type {PIXI.Graphics} les graphismes du polygone */
        this.graphics = new PIXI.Graphics();
        this.graphics.x = x;
        this.graphics.y = y;
        /** @type {Point[]} les points qui servent à calculer les propriétés du polygone */
        this.pts = pts;
        /** @type {double} l'ordre de grandeur du polygone (au niveau graphique) */
        this.scale = scale;
        /** @type {string[]} l'id de la forme */
        this.id = id;
        /** @type {string[]} les noms de la forme dans les différentes langues */
        this.names = names;
        /** @type {Point} l'origine du polygone */
        this.origin = origin || new Point(0, 0);
        /** @type {boolean} si les propriétés du polygone ont été calculées */
        this.initialized = false;

        this.displayAngle = displayAngle;

        this.description = description;

        //Propriétés du polygone
        /** @type {Point[]} les points du polygone */
        this.points = [];
        /** @type {Vector[]} les vecteurs qui composent le polygone */
        this.vectors = [];
        /** @type {Angle[]} les angles du polygone */
        this.angles = [];
        /** @type {double[]} les côtés du polygone */
        this.sides = { count: 0 };
        /** @type {int} le nombre de paires de côtés parallèles */
        this.parallels = 0;
        /** @type {Point} le centre de gravité du polygone */
        this.gravityPoint = new Point(0,0);
        /** @type {int} le nombre d'axes de symétrie */
        this.symAxis = 0;
        /** @type {boolean} si le polygone est convexe */
        this.convex = true;
        /** @type {int} le niveau harmos */
        this.harmos = harmos;
        /** @type {Line[]} les médiatrices du polygone */
        this.sideBisections = [];
        /** @type {Line[]} le(s) axe de symétrie du polygone */
        this.axeSym = [];
        /** @type {Line[]} le(s) médianes du polygone */
        this.mediane = [];
        /** @type {Line[]} les bisectrices du polygone */
        this.bisections = [];

        this.diagonal = [];

        this.nAxeSym = 0;
        this.nPaireCotePara = 0;

        this.gravity = [];
    }

    /**
     * Calcules les différentes propriétés du polygone
     */
    init() {
        //console.log("Start1");

        let lastVect = new Vector;
        let pointSupp = false;
        this.gravityPoint = VectorUtils.calcGravityPoint(this.pts);

        //Calcul du vecteur, de la médiatrice et des points

        //console.log("Pts avant modif:");


        for (let i = 0; i < this.pts.length; i++) {
            let currentPoint = this.pts[i];
            let secondPoint = this.pts[(i + 1 < this.pts.length) ? i + 1 : 0];
            let thirdPoint = null;
            if (i + 2 > this.pts.length){
                thirdPoint = this.pts[1];
            } else if (i + 2 > this.pts.length - 1){
                thirdPoint = this.pts[0];
            } else {
                thirdPoint = this.pts[i + 2];
            }

            let firsVect = new Vector(secondPoint.x - currentPoint.x, secondPoint.y - currentPoint.y,
                new Point(currentPoint.x, currentPoint.y), currentPoint.name, secondPoint.name);
            let secondVect = new Vector(thirdPoint.x - secondPoint.x, thirdPoint.y - secondPoint.y,
                new Point(secondPoint.x, secondPoint.y), secondPoint.name, thirdPoint.name);

            if (firsVect.fullDirection === secondVect.fullDirection) {
                delete this.pts[i + 1];

                for (let j = i + 1; j < this.pts.length - 1; j++) {
                    this.pts[j] = this.pts[j + 1]
                }
                this.pts.pop();
                i--;
            }
        }

        let alphabet = {0:"A",1:"B",2:"C",3:"D",4:"E",5:"F",6:"G",7:"H",8:"I",9:"J",10:"K"};
        for (let i = 0; i < this.pts.length; i++) {
            this.pts[i].name = alphabet[i];
        }

        for (let i = 0; i < this.pts.length; i++) {
            let currentPoint = this.pts[i];
            let secondPoint = this.pts[(i + 1 < this.pts.length) ? i + 1 : 0];

            let vect = new Vector(secondPoint.x - currentPoint.x, secondPoint.y - currentPoint.y,
                new Point(currentPoint.x, currentPoint.y), currentPoint.name, secondPoint.name);

            this.sides.count++;
            this.sides[currentPoint.name + secondPoint.name] = vect.norme;
            let sideBisection = VectorUtils.calcSideBisection(vect);
            if (sideBisection.containsPoint(this.gravityPoint)) this.symAxis++;
            this.sideBisections[`m${currentPoint.name.toLowerCase()}`] = sideBisection;
            this.vectors[currentPoint.name.toLowerCase()] = vect;
            this.points[currentPoint.name] = new Point(currentPoint.x, currentPoint.y, currentPoint.nonconvex);
            if (currentPoint.nonconvex) this.convex = false;
        }


        for (let i = 0; i < this.pts.length; i++) {
            let lastPoint = new Point;
            lastPoint = this.pts[(i - 1 >= 0) ? i - 1 : this.pts.length - 1];

            let lastVector = this.vectors[lastPoint.name.toLowerCase()];
            let currentPoint = this.pts[i];
            let currentVector = this.vectors[currentPoint.name.toLowerCase()];

            let secondPoint = new Point;
            secondPoint = this.pts[(i + 1 < this.pts.length) ? i + 1 : 0];
            //Calcul de l'angle;
            let alpha = VectorUtils.calcAngle(this.vectors[currentPoint.name.toLowerCase()], this.vectors[lastPoint.name.toLowerCase()],false);
            if (currentPoint.nonconvex) alpha = 360 - alpha;
            let angle = new Angle(alpha, 360 - alpha, [lastPoint, currentPoint, secondPoint]);
            this.angles[lastPoint.name + currentPoint.name + secondPoint.name] = angle;
            //Calcul des bissectrices
            let bisection = VectorUtils.calcBisection(angle, lastVector, currentVector);
            if (bisection.containsPoint(this.gravityPoint)) this.symAxis++;
            this.bisections[`b${currentPoint.name.toLowerCase()}`] = bisection;
            //Calcul des parallèles
            if (this.vectors[lastPoint.name.toLowerCase()].direction === this.vectors[secondPoint.name.toLowerCase()].direction) {
                this.parallels++;
            }
        }
        this.parallels /= 2;
        this.initialized = true;
        this.graphics = this.initGraphics();
    }

    /**
     * Retourne un Sprite du polygone. Si une largeur et une hauteur maximale sont données,
     * le sprite sera dimensionné pour rentrer dans ces limites (le plus proche possible)
     * @param {double} maxwidth La largeur maximale du sprite généré (facultatif)
     * @param {double} maxheight La hauteur maximale du sprite généré (facultatif)
     * @param {boolean} lines Défini si les différents axes du polygones doivent apparaître (facultatif)
     * @return {PIXI.Sprite} le sprite du polygone
     */
    initGraphics(maxwidth, maxheight, lines, nAxeSym = 0, nPaireCotePara = 0, axeSym = [], bissec = [], mediatrice = [], mediane = [], gravity = [], affAxeSym = 0, affBissec = 0, affMediane = 0,affMediatrice = 0,  affPointNom = false, affgravity = false,diagonal=[], affDiagonal=0) {
        let graphics = this.generateGraphics();
        this.axeSym = axeSym;
        this.nAxeSym = nAxeSym;
        this.nPaireCotePara = nPaireCotePara;
        this.bisections = bissec;
        this.sideBisections = mediatrice;
        this.mediane = mediane;
        this.gravity = gravity;
        this.diagonal = diagonal;

        if (maxwidth && maxheight) {

            let oldscale = this.scale;
            let wscale = this.scale * (maxwidth / graphics.width);
            let hscale = this.scale * (maxheight / graphics.height);
            this.scale = wscale > hscale ? hscale : wscale;
            graphics = this.generateGraphics(lines, affAxeSym, affBissec, affMediatrice, affMediane, affPointNom, affgravity, affDiagonal);
            this.scale = oldscale;
        }
        graphics.anchor.set(0.5);
        graphics.interactive = true;
        graphics.buttonMode = true;
        return graphics;
    }

    /**
     * Génère un sprite à partir des points du polygone.
     * Il est possible de donner une liste de droites à afficher sur le polygone.
     * @param {Line[]} lines La liste des {@link Line} à afficher (facultatif)
     * @return {PIXI.Sprite} le sprite généré
     */
    generateGraphics(lines = false, affAxeSym = 0, affBissec = 0, affMediatrice = 0, affMediane = 0, affPointNom = false, affgravity = false, affDiagonal) {
        let graphics = new PIXI.Graphics();
        console.log(affMediane);
        graphics.beginFill(this.getRandomColor());
        graphics.lineStyle(1, 0x00, 1);
        graphics.moveTo(this.origin.x, this.origin.y);
        let nomPoint = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
        let index = 0;

        for (let point in this.points) {
            graphics.lineTo(this.points[point].x * this.scale, this.points[point].y * this.scale);

            if (affPointNom){
                let pointNom = new PIXI.Text(nomPoint[index], {fontFamily: 'Arial', fontSize: 15, fill: 0x00, align: 'left'});
                index++;
                pointNom.position.x = this.points[point].x * this.scale;
                pointNom.position.y = this.points[point].y * this.scale - 10;
                graphics.addChild(pointNom);
            }
        }
        graphics.lineTo(this.origin.x, this.origin.y);
        graphics.endFill();
        let polygon = new PIXI.Sprite(graphics.generateCanvasTexture());
        if (lines) {

            if (affMediatrice !== 0){
                // Affiche la ou les médiatrices
                if (affMediatrice === -1){
                    // Affiche toutes les médiatrices
                    for (let i = 0; i < this.sideBisections.length; i++) {
                        let sBis = this.sideBisections[i];
                        let x1 = sBis["x1"];
                        let y1 = sBis["y1"];
                        let x2 = sBis["x2"];
                        let y2 = sBis["y2"];

                        graphics.lineStyle(3,0x0099ff,1);
                        graphics.beginFill(0x0099ff);
                        graphics.moveTo(x1 * this.scale, y1 * this.scale);
                        graphics.lineTo(x2 * this.scale, y2 * this.scale);

                        graphics.endFill();
                    }
                } else {
                    // Affiche une médiatrice
                    let sBis = this.sideBisections[affMediatrice - 1];
                    let sbisNom = new PIXI.Text(sBis["name"], {fontFamily: 'Arial', fontSize: 14, fill: 0x00, align: 'left'});

                    let x1 = sBis["x1"];
                    let y1 = sBis["y1"];
                    let x2 = sBis["x2"];
                    let y2 = sBis["y2"];

                    graphics.lineStyle(3,0x0099ff,1);
                    graphics.beginFill(0x0099ff);
                    graphics.moveTo(x1 * this.scale, y1 * this.scale);
                    graphics.lineTo(x2 * this.scale, y2 * this.scale);
                    sbisNom.x = (x1 + 0.02) * this.scale;
                    sbisNom.y = (y1 + 0.02) * this.scale;
                    graphics.addChild(sbisNom);

                    graphics.endFill();
                }
            }

            if (affBissec !== 0){
                // Affiche toutes les bissectrices
                if (affBissec === -1){
                    for (let i = 0; i < this.bisections.length; i++) {
                        let bis = this.bisections[i];

                        let x1 = bis["x1"];
                        let y1 = bis["y1"];
                        let x2 = bis["x2"];
                        let y2 = bis["y2"];

                        graphics.lineStyle(3,0x33cc33,1);
                        graphics.beginFill(0x33cc33);
                        graphics.moveTo(x1 * this.scale, y1 * this.scale);
                        graphics.lineTo(x2 * this.scale, y2 * this.scale);

                        graphics.endFill();
                    }
                } else {
                    // Affiche une bissectrice
                    let bis = this.bisections[affBissec - 1];
                    let bisNom = new PIXI.Text(bis["name"], {fontFamily: 'Arial', fontSize: 14, fill: 0x00, align: 'left'});

                    let x1 = bis["x1"];
                    let y1 = bis["y1"];
                    let x2 = bis["x2"];
                    let y2 = bis["y2"];

                    graphics.lineStyle(3,0x33cc33,1);
                    graphics.beginFill(0x33cc33);
                    graphics.moveTo(x1 * this.scale, y1 * this.scale);
                    graphics.lineTo(x2 * this.scale, y2 * this.scale);
                    bisNom.x = (x1 + 0.02) * this.scale;
                    bisNom.y = (y1 + 0.02) * this.scale;
                    graphics.addChild(bisNom);

                    graphics.endFill();
                }
            }

            if (affMediane !== 0){
                if (affMediane === -1){
                    // Affiche toutes les médianes
                    for (let i = 0; i < this.mediane.length; i++) {
                        let med = this.mediane[i];

                        let x1 = med["x1"];
                        let y1 = med["y1"];
                        let x2 = med["x2"];
                        let y2 = med["y2"];

                        graphics.lineStyle(3,0xcc00ff,1);
                        graphics.beginFill(0xcc00ff);
                        graphics.moveTo(x1 * this.scale, y1 * this.scale);
                        graphics.lineTo(x2 * this.scale, y2 * this.scale);

                        graphics.endFill();
                    }
                } else {
                    // Affiche une médiane
                    let med = this.mediane[affMediane - 1];
                    let medNom = new PIXI.Text(med["name"], {fontFamily: 'Arial', fontSize: 14, fill: 0x00, align: 'left'});

                    let x1 = med["x1"];
                    let y1 = med["y1"];
                    let x2 = med["x2"];
                    let y2 = med["y2"];

                    graphics.lineStyle(3,0xcc00ff,1);
                    graphics.beginFill(0xcc00ff);
                    graphics.moveTo(x1 * this.scale, y1 * this.scale);
                    graphics.lineTo(x2 * this.scale, y2 * this.scale);
                    medNom.x = (x1 + 0.02) * this.scale;
                    medNom.y = (y1 + 0.02) * this.scale;
                    graphics.addChild(medNom);

                    graphics.endFill();
                }
            }

            if (affAxeSym !== 0){
                if (affAxeSym === -1){
                    // Affiche les axes de symétrie
                    for (let i = 0; i < this.axeSym.length; i++) {
                        let sym = this.axeSym[i];

                        let x1 = sym["x1"];
                        let y1 = sym["y1"];
                        let x2 = sym["x2"];
                        let y2 = sym["y2"];

                        graphics.lineStyle(2,0xbf4080,1);
                        graphics.beginFill(0xbf4080);
                        graphics.moveTo(x1 * this.scale, y1 * this.scale);
                        graphics.lineTo(x2 * this.scale, y2 * this.scale);
                    }
                } else {
                    // Affiche un axe de symétrie
                    let sym = this.axeSym[affAxeSym - 1];
                    let symNom = new PIXI.Text(sym["name"], {fontFamily: 'Arial', fontSize: 14, fill: 0x00, align: 'left'});

                    let x1 = sym["x1"];
                    let y1 = sym["y1"];
                    let x2 = sym["x2"];
                    let y2 = sym["y2"];

                    graphics.lineStyle(2,0xbf4080,1);
                    graphics.beginFill(0xbf4080);
                    graphics.moveTo(x1 * this.scale, y1 * this.scale);
                    graphics.lineTo(x2 * this.scale, y2 * this.scale);
                    symNom.x = (x1 + 0.02) * this.scale;
                    symNom.y = (y1 + 0.02) * this.scale;
                    graphics.addChild(symNom);
                }
            }

            if (affgravity){
                let grav = this.gravity[0];
                if (typeof grav !== 'undefined'){
                    let centre = new PIXI.Graphics();
                    console.log(grav);
                    centre.beginFill(0xfaae0a);
                    centre.lineStyle(5, 0xfaae0a);
                    centre.drawRect(grav.x * this.scale,grav.y * this.scale,5,5);
                    graphics.addChild(centre);
                }
            }
            if (affDiagonal !== 0){
                if (affDiagonal === -1){
                    // Affiche toutes les médianes
                    for (let i = 0; i < this.diagonal.length; i++) {
                        let diagonal = this.diagonal[i];

                        let x1 = diagonal["x1"];
                        let y1 = diagonal["y1"];
                        let x2 = diagonal["x2"];
                        let y2 = diagonal["y2"];

                        graphics.lineStyle(3,0xcc00ff,1);
                        graphics.beginFill(0xcc00ff);
                        graphics.moveTo(x1 * this.scale, y1 * this.scale);
                        graphics.lineTo(x2 * this.scale, y2 * this.scale);

                        graphics.endFill();
                    }
                } else {
                    // Affiche une médiane
                    let diagonal = this.diagonal[affDiagonal - 1];
                    let diagonalName = new PIXI.Text(diagonal["name"], {fontFamily: 'Arial', fontSize: 14, fill: 0x00, align: 'left'});

                    let x1 = diagonal["x1"];
                    let y1 = diagonal["y1"];
                    let x2 = diagonal["x2"];
                    let y2 = diagonal["y2"];

                    graphics.lineStyle(3,0xcc00ff,1);
                    graphics.beginFill(0xcc00ff);
                    graphics.moveTo(x1 * this.scale, y1 * this.scale);
                    graphics.lineTo(x2 * this.scale, y2 * this.scale);
                    diagonalName.x = (x1 + 0.02) * this.scale;
                    diagonalName.y = (y1 + 0.02) * this.scale;
                    graphics.addChild(diagonalName);

                    graphics.endFill();
                }
            }

            // console.log('LINES', lines, this.sideBisections);
        }
        return new PIXI.Sprite(graphics.generateCanvasTexture());
    }

    /**
     * Retourne le nom du polygone dans la langue demandée
     * @param {string} lang La langue désirée
     * @return {string} le nom du polygone
     */
    getName(lang) {
        // console.log(this.names);
        // console.log(lang);
        return this.names[lang];
    }

    /**
     * Retourne une couleur aléatoire parmis un set de couleurs prédéfinies
     * @return {int} la couleur en hexadécimal
     */
    getRandomColor() {
        let colors = [0xff9999, 0xff80ff, 0x99bbff, 0xadebad, 0xffdd99, 0xecb3ff, 0xb3ccff, 0xb3ffff, 0xb3b3ff, 0xc2efc2];
        let random = Math.floor(Math.random() * colors.length) + 1;
        return colors[random - 1];
    }

}