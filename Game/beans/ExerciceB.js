/**
 * @classdesc Bean ExerciceB
 * @author Théo Teixeira
 * @version 0.1
 */
class ExerciceB {
    /**
     * Constructeur de Exercice
     * @param {string[]} name Le nom de l'exercice
     * @param {int} harmos Le niveau Harmos de l'exerice
     * @param {Question[]} questions Les questions de l'exerice
     * @example
     *
     *      let e1 = new Exercice({fr: "En français", de:"En allemand"},
     *                            5,{question1, question2},)
     */
    constructor(name, harmos, questions){
        this.name = name;
        this.harmos = harmos;
        this.questions = questions;
    }
}