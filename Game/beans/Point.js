/**
 * @classdesc Bean point
 * @author Vincent Audergon
 * @version 1.0
 */
class Point {

    /**
     * Constructeur de l'objet point
     * @param {double} x la composante x du point
     * @param {double} y la composante y du point
     * @param {boolean} [nonconvex=false] indique si l'angle lié au point est convexe ou non (facultatif, faux par défaut)
     */
    constructor(x, y, nonconvex = false) {
        this.x = x;
        this.y = y;
        this.nonconvex = nonconvex;
    }

    /**
     * Additionne les composantes x et y du point avec celles d'un autres pour en recréer un nouveau
     * @param {Point} point le deuxième {@link Point}
     * @return {Point} le nouveau {@link Point}
     */
    add(point) {
        return new Point(this.x + point.x, this.y + point.y);
    }

    /**
     * Soustrait les composantes x et y d'un autre point au point pour en recréer un nouveau
     * @param {Point} point le deuxième {@link Point}
     * @return {Point} le nouveau {@link Point}
     */
    sub(point) {
        return new Point(this.x - point.x, this.y - point.y);
    }

    /**
     * Multiplie les composantes x et y du point avec celles d'un autres pour en recréer un nouveau
     * @param {Point} point le deuxième {@link Point}
     * @return {Point} le nouveau {@link Point}
     */
    mult(point) {
        return new Point(this.x * point.x, this.y * point.y);
    }

    /**
     * Divise les composantes x et y du point par celles d'un autres pour en recréer un nouveau
     * @param {Point} point le deuxième {@link Point}
     * @return {Point} le nouveau {@link Point}
     */
    div(point) {
        return new Point(this.x / point.x, this.y / point.y);
    }

    /**
     * Rend un nouveau point avec les valeurs absolues du point (par ex. 1;-1 => 1;1)
     * @return {Point} le nouveau {@link Point}
     */
    abs(){
        return new Point(Math.abs(this.x), Math.abs(this.y));
    }

    /**
     * Indique si deux points sont égaux ou non (composantes égales entre elles)
     * @param {Point} point le deuxième {@link Point}
     * @return {boolean} si les points sont égaux
     */
    equals(point) {
        return this.x === point.x && this.y === point.y;
    }
}