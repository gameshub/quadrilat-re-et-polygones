/**
 * @interface
 * @classdesc Interface Asset, défini un asset, soit un élément du canvas comme un bouton ou une grille
 * @author Vincent Audergon
 * @version 1.0
 */
class Asset {

    /**
     * Retourne la liste des éléments PIXI d'un asset
     * @return {Object[]} les éléments PIXI
     */
    getPixiChildren(){}

    /**
     * Retourne la position y du composant
     * @return {number} posY
     */
    getY(){}
    /**
     * Retourne la position x du composant
     * @return {number} posX
     */
    getX(){}
    setY(y){}
    setX(x){}
    getWidth(){}
    getHeight(){}
    setVisible(visible){}

}
