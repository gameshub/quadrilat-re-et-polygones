
/**
 * @classdesc Objet angle
 * @author Vincent Audergon
 * @version 1.0
 */
class Angle {

    /**
     * Constructeur de l'objet Angle
     * @param {double} alpha L'angle alpha (intérieur au polygone)
     * @param {double} beta L'angle beta (exterieur au polygone)
     * @param {string[]} points Les points concercées par l'angle (ABC par ex.)
     * @example
     *
     *      new Angle(90,270,['A','B','C'])
     */
    constructor(alpha, beta, points){
        this.alpha = alpha;
        this.beta = beta;
        this.points = points;
    }

}
/**
 * @classdesc Bean ExerciceB
 * @author Théo Teixeira
 * @version 0.1
 */
class ExerciceB {
    /**
     * Constructeur de Exercice
     * @param {string[]} name Le nom de l'exercice
     * @param {int} harmos Le niveau Harmos de l'exerice
     * @param {Question[]} questions Les questions de l'exerice
     * @example
     *
     *      let e1 = new Exercice({fr: "En français", de:"En allemand"},
     *                            5,{question1, question2},)
     */
    constructor(name, harmos, questions){
        this.name = name;
        this.harmos = harmos;
        this.questions = questions;
    }
}
class ExerciceForSave {
    constructor() {
        this.name = {
            fr:'',
            de:'',
            en:'',
            it:''
        };
        this.degree = null;
        this.questions = [];
        this.currentQuestion = null;

        ExerciceForSave.prototype.toString = function () {
            let json = {
                name:this.name,
                degree:this.degree,
                questions:this.questions
            };
            return JSON.stringify(json);
        }
    }

    createQuestion(){
        this.currentQuestion = {
            label:{
                fr:'',
                de:'',
                en:'',
                it:''
            },
            allResponses:[],
            trueResponses:[],
            type:null,
            qcmtype:null,
            defaultShapeChoosed:false
        }
    }

    addResponses(shape, good){
        if(this.currentQuestion == null)
            throw "There no current question !";
        if(good)
            this.currentQuestion.trueResponses.push({
                value:shape,
                response:good
            });
        else
            this.currentQuestion.allResponses.push({
                value:shape,
                response:good
            });
    }

    endQuestion(){
        this.questions.push({
            label:this.currentQuestion.label,
            responses:this.getResponses(),
            type:this.currentQuestion.type,
            qcmtype:this.currentQuestion.qcmtype
        });
    }

    getResponses(){
        let responses = [];
        let ids = [];
        for (let response of this.currentQuestion.allResponses){
            ids.push(response.value);
            responses.push(response);
        }
        for (let response of this.currentQuestion.trueResponses){
            if(ids.indexOf(response.value) > -1){
                responses[ids.indexOf(response.value)].response = true;
            } else {
                responses.push(response)
            }
        }
        return responses;
    }


}
/**
 * @classdesc Objet Line, représente une droite en mathématiques
 * @author Vincent Audergon
 * @version 1.0
 */
class Line {

    /**
     * Constructeur de l'objet Line. Il est possible de créer la droite à partir
     * d'un angle et d'un point ou des attributs a et b d'une fonction affine.
     * @param {double|double} angle L'angle de la fonction en **radians** | a si fromAngle est faux
     * @param {Point|double} p1 Un {@link Point} de la fonction | b si fromAngle est faux
     * @param {boolean} [fromAngle=true] Argument facultatif, si la droite est calculée
     *                            à partir d'un angle ou pas, vrai par défaut
     *
     * @example
     *
     *      let l1 = new Line(1,1, false); //A partir de a et b
     *      let l2 = new Line(45,new Point(1,1)) //A partir d'un angle
     */
    constructor(angle, p1, fromAngle = true) {
        if (fromAngle) {
            if (angle % Math.PI !== Math.PI / 2) {
                this.a = Math.round(Math.tan(angle) * 100) / 100;
                this.b = Math.round((p1.y - this.a * p1.x) * 100) / 100;
            } else {
                this.a = Infinity;
                this.b = p1.x;
            }
        } else {
            this.a = angle;
            this.b = p1;
        }
    }

    /**
     * Calcule la coordonnée y en fonction de la coordonnée x
     * Selon l'équation *y = ax+b*
     * @param {double} x La coordonnée x
     * @return {double} La coordonnée y
     */
    calcY(x) {
        if (this.a != Infinity) {
            return this.a * x + this.b;
        } else {
            return x === this.b ? Infinity : NaN;
        }
    }

    /**
     * Vérifie si un point est sur la droite ou non
     * @param {Point} point Le {@link Point} à vérifier
     * @return {boolean} Si le point est sur la droite
     */
    containsPoint(point) {
        return this.calcY(point.x) === point.y || this.calcY(point.x) === Infinity;
    }
}
/**
 * @classdesc Bean node. Noeud qui compose une grille de dessin
 * @author Vincent Audergon
 * @version 1.0
 */
class Node {

    /**
     * Constructeur de Node
     * @param {double} x La position x du noeud
     * @param {double} y La position y du noeud
     * @param {double} width La largeur et hauteur du noeud
     * @param {DrawingGrid} refGrid La référence vers la {@link DrawingGrid} (grille de dessin)
     */
    constructor(x, y, width, refGrid) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.refGrid = refGrid;
        this.init();
    }

    /**
     * Initialise le noeud
     */
    init() {

        this.graphics = new PIXI.Graphics();
        this.graphics.interactive = true;
        this.graphics.buttonMode = true;
        this.graphics.beginFill(0xFFFFFF);
        this.graphics.drawRect(this.x-10,this.y-10, this.width+20,this.width+20);
        this.graphics.endFill(0xFFFFFF);
        this.graphics.on('pointerdown', this.onNodeClicked.bind(this));

        this.point = new PIXI.Graphics();
        this.point.beginFill(0x000000);
        this.point.drawRect(this.x, this.y, this.width, this.width);
        this.point.endFill(0x000000);
        this.point.interactive = true;
        this.point.buttonMode = true;
        this.point.on('pointerdown', this.onNodeClicked.bind(this));
    }

    /**
     * Retourne les coordonées du centre du noeud
     * @return {Point} le centre du noeud
     */
    center() {
        return new Point(this.x + this.width / 2, this.y + this.width / 2);
    }

    /**
     * Méthode de callback appelée lorsqu'un click est détecté sur le noeud
     * @param {Event} e L'événement JavaScript
     */
    onNodeClicked(e) {
        this.refGrid.onNodeClicked(e, this);
    }

    /**
     * Méthode de callback appelée lorsque le click est enfoncé
     * et que le curseur passe au dessus du noeud
     * @deprecated
     * @param {Event} e L'événement JavaScript
     */
    onNodeEncountered(e) {
        this.refGrid.onNodeEncountered(e, this)
    }


}
/**
 * @classdesc Bean point
 * @author Vincent Audergon
 * @version 1.0
 */
class Point {

    /**
     * Constructeur de l'objet point
     * @param {double} x la composante x du point
     * @param {double} y la composante y du point
     * @param {boolean} [nonconvex=false] indique si l'angle lié au point est convexe ou non (facultatif, faux par défaut)
     */
    constructor(x, y, nonconvex = false) {
        this.x = x;
        this.y = y;
        this.nonconvex = nonconvex;
    }

    /**
     * Additionne les composantes x et y du point avec celles d'un autres pour en recréer un nouveau
     * @param {Point} point le deuxième {@link Point}
     * @return {Point} le nouveau {@link Point}
     */
    add(point) {
        return new Point(this.x + point.x, this.y + point.y);
    }

    /**
     * Soustrait les composantes x et y d'un autre point au point pour en recréer un nouveau
     * @param {Point} point le deuxième {@link Point}
     * @return {Point} le nouveau {@link Point}
     */
    sub(point) {
        return new Point(this.x - point.x, this.y - point.y);
    }

    /**
     * Multiplie les composantes x et y du point avec celles d'un autres pour en recréer un nouveau
     * @param {Point} point le deuxième {@link Point}
     * @return {Point} le nouveau {@link Point}
     */
    mult(point) {
        return new Point(this.x * point.x, this.y * point.y);
    }

    /**
     * Divise les composantes x et y du point par celles d'un autres pour en recréer un nouveau
     * @param {Point} point le deuxième {@link Point}
     * @return {Point} le nouveau {@link Point}
     */
    div(point) {
        return new Point(this.x / point.x, this.y / point.y);
    }

    /**
     * Rend un nouveau point avec les valeurs absolues du point (par ex. 1;-1 => 1;1)
     * @return {Point} le nouveau {@link Point}
     */
    abs(){
        return new Point(Math.abs(this.x), Math.abs(this.y));
    }

    /**
     * Indique si deux points sont égaux ou non (composantes égales entre elles)
     * @param {Point} point le deuxième {@link Point}
     * @return {boolean} si les points sont égaux
     */
    equals(point) {
        return this.x === point.x && this.y === point.y;
    }
}
/**
 * @classdesc Bean Question
 * @author Vincent Audergon
 * @version 1.0
 */
class Question {

    /**
     * Constructeur de Question
     * @param {string[]} label La consigne de la question dans les différentes langues
     * @param {JSON} responses Les réponses possibles
     * @param {JSON} traps Les réponses piège
     * @param {int} type Le type de la question (0 => QCM, 1 => dessin)
     * @param {string} qcmtype Le type de QCM (radio | checkbox)
     * @example
     *
     *      let q1 = new Question({fr: "En français", de:"En allemand"},
     *                            {value:"rect", response: true},
     *                            {value:"square", response: false},
     *                            0, "radio")
     */
    constructor(label, responses, traps, type, qcmtype) {
        this.label = label;
        this.responses = responses;
        this.traps = traps;
        this.type = type;
        this.qcmtype = qcmtype;
    }

}
/**
 * @classdesc Bean shape, représente un polygone
 * @author Vincent Audergon
 * @version 2.0
 */
class Shape {

    /**
     * Constructeur de l'objet Shape
     * @param {double} x La coordonnée x du polygone sur le canvas
     * @param {double} y La coordonnée y du polygone sur le canvas
     * @param {Point[]} pts La liste des {@link Point} qui composents le polygone
     * @param {double} scale La taille de la forme sur le canvas
     * @param {string} id L'id du polygone
     * @param {string[]} names Le nom de la formes dans toutes les langues
     * @param {Point} [origin={x:0,y:0}] Le {@link Point} d'origine du polygone (facultatif, 0;0 par défaut)
     * @param {number} harmos Degré harmos
     */
    constructor(x, y, pts, scale, id, names, origin, harmos, displayAngle,description) {
        /** @type {PIXI.Graphics} les graphismes du polygone */
        this.graphics = new PIXI.Graphics();
        this.graphics.x = x;
        this.graphics.y = y;
        /** @type {Point[]} les points qui servent à calculer les propriétés du polygone */
        this.pts = pts;
        /** @type {double} l'ordre de grandeur du polygone (au niveau graphique) */
        this.scale = scale;
        /** @type {string[]} l'id de la forme */
        this.id = id;
        /** @type {string[]} les noms de la forme dans les différentes langues */
        this.names = names;
        /** @type {Point} l'origine du polygone */
        this.origin = origin || new Point(0, 0);
        /** @type {boolean} si les propriétés du polygone ont été calculées */
        this.initialized = false;

        this.displayAngle = displayAngle;

        this.description = description;

        //Propriétés du polygone
        /** @type {Point[]} les points du polygone */
        this.points = [];
        /** @type {Vector[]} les vecteurs qui composent le polygone */
        this.vectors = [];
        /** @type {Angle[]} les angles du polygone */
        this.angles = [];
        /** @type {double[]} les côtés du polygone */
        this.sides = { count: 0 };
        /** @type {int} le nombre de paires de côtés parallèles */
        this.parallels = 0;
        /** @type {Point} le centre de gravité du polygone */
        this.gravityPoint = new Point(0,0);
        /** @type {int} le nombre d'axes de symétrie */
        this.symAxis = 0;
        /** @type {boolean} si le polygone est convexe */
        this.convex = true;
        /** @type {int} le niveau harmos */
        this.harmos = harmos;
        /** @type {Line[]} les médiatrices du polygone */
        this.sideBisections = [];
        /** @type {Line[]} le(s) axe de symétrie du polygone */
        this.axeSym = [];
        /** @type {Line[]} le(s) médianes du polygone */
        this.mediane = [];
        /** @type {Line[]} les bisectrices du polygone */
        this.bisections = [];

        this.diagonal = [];

        this.nAxeSym = 0;
        this.nPaireCotePara = 0;

        this.gravity = [];
    }

    /**
     * Calcules les différentes propriétés du polygone
     */
    init() {
        //console.log("Start1");

        let lastVect = new Vector;
        let pointSupp = false;
        this.gravityPoint = VectorUtils.calcGravityPoint(this.pts);

        //Calcul du vecteur, de la médiatrice et des points

        //console.log("Pts avant modif:");


        for (let i = 0; i < this.pts.length; i++) {
            let currentPoint = this.pts[i];
            let secondPoint = this.pts[(i + 1 < this.pts.length) ? i + 1 : 0];
            let thirdPoint = null;
            if (i + 2 > this.pts.length){
                thirdPoint = this.pts[1];
            } else if (i + 2 > this.pts.length - 1){
                thirdPoint = this.pts[0];
            } else {
                thirdPoint = this.pts[i + 2];
            }

            let firsVect = new Vector(secondPoint.x - currentPoint.x, secondPoint.y - currentPoint.y,
                new Point(currentPoint.x, currentPoint.y), currentPoint.name, secondPoint.name);
            let secondVect = new Vector(thirdPoint.x - secondPoint.x, thirdPoint.y - secondPoint.y,
                new Point(secondPoint.x, secondPoint.y), secondPoint.name, thirdPoint.name);

            if (firsVect.fullDirection === secondVect.fullDirection) {
                delete this.pts[i + 1];

                for (let j = i + 1; j < this.pts.length - 1; j++) {
                    this.pts[j] = this.pts[j + 1]
                }
                this.pts.pop();
                i--;
            }
        }

        let alphabet = {0:"A",1:"B",2:"C",3:"D",4:"E",5:"F",6:"G",7:"H",8:"I",9:"J",10:"K"};
        for (let i = 0; i < this.pts.length; i++) {
            this.pts[i].name = alphabet[i];
        }

        for (let i = 0; i < this.pts.length; i++) {
            let currentPoint = this.pts[i];
            let secondPoint = this.pts[(i + 1 < this.pts.length) ? i + 1 : 0];

            let vect = new Vector(secondPoint.x - currentPoint.x, secondPoint.y - currentPoint.y,
                new Point(currentPoint.x, currentPoint.y), currentPoint.name, secondPoint.name);

            this.sides.count++;
            this.sides[currentPoint.name + secondPoint.name] = vect.norme;
            let sideBisection = VectorUtils.calcSideBisection(vect);
            if (sideBisection.containsPoint(this.gravityPoint)) this.symAxis++;
            this.sideBisections[`m${currentPoint.name.toLowerCase()}`] = sideBisection;
            this.vectors[currentPoint.name.toLowerCase()] = vect;
            this.points[currentPoint.name] = new Point(currentPoint.x, currentPoint.y, currentPoint.nonconvex);
            if (currentPoint.nonconvex) this.convex = false;
        }


        for (let i = 0; i < this.pts.length; i++) {
            let lastPoint = new Point;
            lastPoint = this.pts[(i - 1 >= 0) ? i - 1 : this.pts.length - 1];

            let lastVector = this.vectors[lastPoint.name.toLowerCase()];
            let currentPoint = this.pts[i];
            let currentVector = this.vectors[currentPoint.name.toLowerCase()];

            let secondPoint = new Point;
            secondPoint = this.pts[(i + 1 < this.pts.length) ? i + 1 : 0];
            //Calcul de l'angle;
            let alpha = VectorUtils.calcAngle(this.vectors[currentPoint.name.toLowerCase()], this.vectors[lastPoint.name.toLowerCase()],false);
            if (currentPoint.nonconvex) alpha = 360 - alpha;
            let angle = new Angle(alpha, 360 - alpha, [lastPoint, currentPoint, secondPoint]);
            this.angles[lastPoint.name + currentPoint.name + secondPoint.name] = angle;
            //Calcul des bissectrices
            let bisection = VectorUtils.calcBisection(angle, lastVector, currentVector);
            if (bisection.containsPoint(this.gravityPoint)) this.symAxis++;
            this.bisections[`b${currentPoint.name.toLowerCase()}`] = bisection;
            //Calcul des parallèles
            if (this.vectors[lastPoint.name.toLowerCase()].direction === this.vectors[secondPoint.name.toLowerCase()].direction) {
                this.parallels++;
            }
        }
        this.parallels /= 2;
        this.initialized = true;
        this.graphics = this.initGraphics();
    }

    /**
     * Retourne un Sprite du polygone. Si une largeur et une hauteur maximale sont données,
     * le sprite sera dimensionné pour rentrer dans ces limites (le plus proche possible)
     * @param {double} maxwidth La largeur maximale du sprite généré (facultatif)
     * @param {double} maxheight La hauteur maximale du sprite généré (facultatif)
     * @param {boolean} lines Défini si les différents axes du polygones doivent apparaître (facultatif)
     * @return {PIXI.Sprite} le sprite du polygone
     */
    initGraphics(maxwidth, maxheight, lines, nAxeSym = 0, nPaireCotePara = 0, axeSym = [], bissec = [], mediatrice = [], mediane = [], gravity = [], affAxeSym = 0, affBissec = 0, affMediane = 0,affMediatrice = 0,  affPointNom = false, affgravity = false,diagonal=[], affDiagonal=0) {
        let graphics = this.generateGraphics();
        this.axeSym = axeSym;
        this.nAxeSym = nAxeSym;
        this.nPaireCotePara = nPaireCotePara;
        this.bisections = bissec;
        this.sideBisections = mediatrice;
        this.mediane = mediane;
        this.gravity = gravity;
        this.diagonal = diagonal;

        if (maxwidth && maxheight) {

            let oldscale = this.scale;
            let wscale = this.scale * (maxwidth / graphics.width);
            let hscale = this.scale * (maxheight / graphics.height);
            this.scale = wscale > hscale ? hscale : wscale;
            graphics = this.generateGraphics(lines, affAxeSym, affBissec, affMediatrice, affMediane, affPointNom, affgravity, affDiagonal);
            this.scale = oldscale;
        }
        graphics.anchor.set(0.5);
        graphics.interactive = true;
        graphics.buttonMode = true;
        return graphics;
    }

    /**
     * Génère un sprite à partir des points du polygone.
     * Il est possible de donner une liste de droites à afficher sur le polygone.
     * @param {Line[]} lines La liste des {@link Line} à afficher (facultatif)
     * @return {PIXI.Sprite} le sprite généré
     */
    generateGraphics(lines = false, affAxeSym = 0, affBissec = 0, affMediatrice = 0, affMediane = 0, affPointNom = false, affgravity = false, affDiagonal) {
        let graphics = new PIXI.Graphics();
        console.log(affMediane);
        graphics.beginFill(this.getRandomColor());
        graphics.lineStyle(1, 0x00, 1);
        graphics.moveTo(this.origin.x, this.origin.y);
        let nomPoint = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
        let index = 0;

        for (let point in this.points) {
            graphics.lineTo(this.points[point].x * this.scale, this.points[point].y * this.scale);

            if (affPointNom){
                let pointNom = new PIXI.Text(nomPoint[index], {fontFamily: 'Arial', fontSize: 15, fill: 0x00, align: 'left'});
                index++;
                pointNom.position.x = this.points[point].x * this.scale;
                pointNom.position.y = this.points[point].y * this.scale - 10;
                graphics.addChild(pointNom);
            }
        }
        graphics.lineTo(this.origin.x, this.origin.y);
        graphics.endFill();
        let polygon = new PIXI.Sprite(graphics.generateCanvasTexture());
        if (lines) {

            if (affMediatrice !== 0){
                // Affiche la ou les médiatrices
                if (affMediatrice === -1){
                    // Affiche toutes les médiatrices
                    for (let i = 0; i < this.sideBisections.length; i++) {
                        let sBis = this.sideBisections[i];
                        let x1 = sBis["x1"];
                        let y1 = sBis["y1"];
                        let x2 = sBis["x2"];
                        let y2 = sBis["y2"];

                        graphics.lineStyle(3,0x0099ff,1);
                        graphics.beginFill(0x0099ff);
                        graphics.moveTo(x1 * this.scale, y1 * this.scale);
                        graphics.lineTo(x2 * this.scale, y2 * this.scale);

                        graphics.endFill();
                    }
                } else {
                    // Affiche une médiatrice
                    let sBis = this.sideBisections[affMediatrice - 1];
                    let sbisNom = new PIXI.Text(sBis["name"], {fontFamily: 'Arial', fontSize: 14, fill: 0x00, align: 'left'});

                    let x1 = sBis["x1"];
                    let y1 = sBis["y1"];
                    let x2 = sBis["x2"];
                    let y2 = sBis["y2"];

                    graphics.lineStyle(3,0x0099ff,1);
                    graphics.beginFill(0x0099ff);
                    graphics.moveTo(x1 * this.scale, y1 * this.scale);
                    graphics.lineTo(x2 * this.scale, y2 * this.scale);
                    sbisNom.x = (x1 + 0.02) * this.scale;
                    sbisNom.y = (y1 + 0.02) * this.scale;
                    graphics.addChild(sbisNom);

                    graphics.endFill();
                }
            }

            if (affBissec !== 0){
                // Affiche toutes les bissectrices
                if (affBissec === -1){
                    for (let i = 0; i < this.bisections.length; i++) {
                        let bis = this.bisections[i];

                        let x1 = bis["x1"];
                        let y1 = bis["y1"];
                        let x2 = bis["x2"];
                        let y2 = bis["y2"];

                        graphics.lineStyle(3,0x33cc33,1);
                        graphics.beginFill(0x33cc33);
                        graphics.moveTo(x1 * this.scale, y1 * this.scale);
                        graphics.lineTo(x2 * this.scale, y2 * this.scale);

                        graphics.endFill();
                    }
                } else {
                    // Affiche une bissectrice
                    let bis = this.bisections[affBissec - 1];
                    let bisNom = new PIXI.Text(bis["name"], {fontFamily: 'Arial', fontSize: 14, fill: 0x00, align: 'left'});

                    let x1 = bis["x1"];
                    let y1 = bis["y1"];
                    let x2 = bis["x2"];
                    let y2 = bis["y2"];

                    graphics.lineStyle(3,0x33cc33,1);
                    graphics.beginFill(0x33cc33);
                    graphics.moveTo(x1 * this.scale, y1 * this.scale);
                    graphics.lineTo(x2 * this.scale, y2 * this.scale);
                    bisNom.x = (x1 + 0.02) * this.scale;
                    bisNom.y = (y1 + 0.02) * this.scale;
                    graphics.addChild(bisNom);

                    graphics.endFill();
                }
            }

            if (affMediane !== 0){
                if (affMediane === -1){
                    // Affiche toutes les médianes
                    for (let i = 0; i < this.mediane.length; i++) {
                        let med = this.mediane[i];

                        let x1 = med["x1"];
                        let y1 = med["y1"];
                        let x2 = med["x2"];
                        let y2 = med["y2"];

                        graphics.lineStyle(3,0xcc00ff,1);
                        graphics.beginFill(0xcc00ff);
                        graphics.moveTo(x1 * this.scale, y1 * this.scale);
                        graphics.lineTo(x2 * this.scale, y2 * this.scale);

                        graphics.endFill();
                    }
                } else {
                    // Affiche une médiane
                    let med = this.mediane[affMediane - 1];
                    let medNom = new PIXI.Text(med["name"], {fontFamily: 'Arial', fontSize: 14, fill: 0x00, align: 'left'});

                    let x1 = med["x1"];
                    let y1 = med["y1"];
                    let x2 = med["x2"];
                    let y2 = med["y2"];

                    graphics.lineStyle(3,0xcc00ff,1);
                    graphics.beginFill(0xcc00ff);
                    graphics.moveTo(x1 * this.scale, y1 * this.scale);
                    graphics.lineTo(x2 * this.scale, y2 * this.scale);
                    medNom.x = (x1 + 0.02) * this.scale;
                    medNom.y = (y1 + 0.02) * this.scale;
                    graphics.addChild(medNom);

                    graphics.endFill();
                }
            }

            if (affAxeSym !== 0){
                if (affAxeSym === -1){
                    // Affiche les axes de symétrie
                    for (let i = 0; i < this.axeSym.length; i++) {
                        let sym = this.axeSym[i];

                        let x1 = sym["x1"];
                        let y1 = sym["y1"];
                        let x2 = sym["x2"];
                        let y2 = sym["y2"];

                        graphics.lineStyle(2,0xbf4080,1);
                        graphics.beginFill(0xbf4080);
                        graphics.moveTo(x1 * this.scale, y1 * this.scale);
                        graphics.lineTo(x2 * this.scale, y2 * this.scale);
                    }
                } else {
                    // Affiche un axe de symétrie
                    let sym = this.axeSym[affAxeSym - 1];
                    let symNom = new PIXI.Text(sym["name"], {fontFamily: 'Arial', fontSize: 14, fill: 0x00, align: 'left'});

                    let x1 = sym["x1"];
                    let y1 = sym["y1"];
                    let x2 = sym["x2"];
                    let y2 = sym["y2"];

                    graphics.lineStyle(2,0xbf4080,1);
                    graphics.beginFill(0xbf4080);
                    graphics.moveTo(x1 * this.scale, y1 * this.scale);
                    graphics.lineTo(x2 * this.scale, y2 * this.scale);
                    symNom.x = (x1 + 0.02) * this.scale;
                    symNom.y = (y1 + 0.02) * this.scale;
                    graphics.addChild(symNom);
                }
            }

            if (affgravity){
                let grav = this.gravity[0];
                if (typeof grav !== 'undefined'){
                    let centre = new PIXI.Graphics();
                    console.log(grav);
                    centre.beginFill(0xfaae0a);
                    centre.lineStyle(5, 0xfaae0a);
                    centre.drawRect(grav.x * this.scale,grav.y * this.scale,5,5);
                    graphics.addChild(centre);
                }
            }
            if (affDiagonal !== 0){
                if (affDiagonal === -1){
                    // Affiche toutes les médianes
                    for (let i = 0; i < this.diagonal.length; i++) {
                        let diagonal = this.diagonal[i];

                        let x1 = diagonal["x1"];
                        let y1 = diagonal["y1"];
                        let x2 = diagonal["x2"];
                        let y2 = diagonal["y2"];

                        graphics.lineStyle(3,0xcc00ff,1);
                        graphics.beginFill(0xcc00ff);
                        graphics.moveTo(x1 * this.scale, y1 * this.scale);
                        graphics.lineTo(x2 * this.scale, y2 * this.scale);

                        graphics.endFill();
                    }
                } else {
                    // Affiche une médiane
                    let diagonal = this.diagonal[affDiagonal - 1];
                    let diagonalName = new PIXI.Text(diagonal["name"], {fontFamily: 'Arial', fontSize: 14, fill: 0x00, align: 'left'});

                    let x1 = diagonal["x1"];
                    let y1 = diagonal["y1"];
                    let x2 = diagonal["x2"];
                    let y2 = diagonal["y2"];

                    graphics.lineStyle(3,0xcc00ff,1);
                    graphics.beginFill(0xcc00ff);
                    graphics.moveTo(x1 * this.scale, y1 * this.scale);
                    graphics.lineTo(x2 * this.scale, y2 * this.scale);
                    diagonalName.x = (x1 + 0.02) * this.scale;
                    diagonalName.y = (y1 + 0.02) * this.scale;
                    graphics.addChild(diagonalName);

                    graphics.endFill();
                }
            }

            // console.log('LINES', lines, this.sideBisections);
        }
        return new PIXI.Sprite(graphics.generateCanvasTexture());
    }

    /**
     * Retourne le nom du polygone dans la langue demandée
     * @param {string} lang La langue désirée
     * @return {string} le nom du polygone
     */
    getName(lang) {
        // console.log(this.names);
        // console.log(lang);
        return this.names[lang];
    }

    /**
     * Retourne une couleur aléatoire parmis un set de couleurs prédéfinies
     * @return {int} la couleur en hexadécimal
     */
    getRandomColor() {
        let colors = [0xff9999, 0xff80ff, 0x99bbff, 0xadebad, 0xffdd99, 0xecb3ff, 0xb3ccff, 0xb3ffff, 0xb3b3ff, 0xc2efc2];
        let random = Math.floor(Math.random() * colors.length) + 1;
        return colors[random - 1];
    }

}
/**
 * @classdesc Object vecteur
 * @author Vincent Audergon
 * @version 1.0
 */
class Vector {

    /**
     * Constructeur de l'objet vector
     * @param {double} x La composante x du vecteur
     * @param {double} y La composante y du vecteur
     * @param {Point} offset L'origine du vecteur
     * @param {string} from Le nom du pont de départ
     * @param {string} to Le nom du point d'arrivé
     * @param {boolean} [calcDirection=true] Indique si la direction doit être calculée, vrai par défaut (facultatif)
     * @example
     *
     *      let v1 = new Vector(1,1,new Point(0,0),'A','B', true); //Avec calcul de la direction
     *      let v2 = new Vector(1,1,new Point(0,0),'A','B', false); //Sans calcul de la direction
     */
    constructor(x, y, offset, from, to, calcDirection = true) {
        /** @type {Point} l'origine du vecteur */
        this.offset = offset;
        /** @type {string} le nom du point de départ */
        this.from = from;
        /** @type {string} le nom du point d'arrivée */
        this.to = to;
        /** @type {Point} le sens du vecteur */
        this.way = new Point(x < 0 ? -1 : 1, y < 0 ? -1 : 1);
        /** @type {double} la composante x du vecteur */
        this.x = Math.abs(x);
        /** @type {double} la composante y du vecteur */
        this.y = Math.abs(y);
        /** @type {double} la norme du vecteur */
        this.norme = VectorUtils.calcNorme(this);
        /** @type {Vector} la direction du vecteur (0 à 180°) */
        this.direction = calcDirection ? VectorUtils.calcDirection(this) : NaN;
        /** @type {Vector} la direction complète du vecteur (0 à 360°) */
        this.fullDirection = calcDirection ? VectorUtils.calcFullDirection(this) : NaN;
    }

    /**
     * Retourne la composante x **signée**
     * @return {double} La composante x
     */
    getTrueX() {
        return this.x * this.way.x;
    }

    /**
     * Retourne la composante y **signée**
     * @return {double} La composante y
     */
    getTrueY() {
        return this.y * this.way.y;
    }

    /**
     * Effectue une addition sur les composantes du vecteur
     * @param {Vector} v2 Le deuxième {@link Vector}
     */
    add(v2) {
        this.x += v2.x;
        this.y += v2.y;
    }

    /**
     * Effectue une soustraction sur les composantes du vecteur
     * @param {Vector} v2 Le deuxième {@link Vector}
     */
    sub(v2) {
        this.x -= v2.x;
        this.y -= v2.y;
    }

    /**
     * Effectue une multiplication sur les composantes du vecteur
     * @param {Vector} v2 Le deuxième {@link Vector}
     */
    mult(v2) {
        this.x *= v2.x;
        this.y *= v2.y;
    }

    /**
     * Effectue une division sur les composantes du vecteur
     * @param {Vector} v2 Le deuxième {@link Vector}
     */
    div(v2) {
        this.x /= v2.x;
        this.y /= v2.y;
    }

    /**
     * Effectue un modulo sur les composantes du vecteur
     * @param {Vector} v2 Le deuxième {@link Vector}
     */
    mod(v2) {
        this.x %= v2.x;
        this.y %= v2.y;
    }

    /**
     * Transforme le vecteur en chaîne de caractères
     * @return {string} la chaîne de caractères
     */
    toString() {
        return `[${this.x}, ${this.y}]`;
    }

    /**
     * Retourne un clone du vecteur
     * @return {Vector} le clone
     */
    clone() {
        return new Vector(this.x, this.y, this.offset, this.name);
    }

}
class StringNames {
    constructor() {
        this.CARRE = "CARR";
        this.RECTANGLE = "RECT";
        this.TRAPEZE = "TRAN";
        this.TRAPEZE_RECTANGLE = "TRAR";
        this.TRAPEZE_ISOCELE = "TRAI";
        this.PARALLELOGRAMME = "PARA";
        this.LOSANGE = "LOSA";
        this.TRIANGLE = "TRIN";
        this.TRIANGLE_ISOCELE = "TRII";
        this.TRIANGLE_RECTANGLE = "TRIR";
        this.TRIANGLE_ISOCELE_RECTANGLE = "TRRI";
        this.TRIANGLE_EQUILATERAL = "TRIE";
        this.CERF_VOLANT = "CERF";
        this.POINTE_DE_FLECHE = "FLEC";
        this.QUADRILATERE_IRR = "QUAD";
        this.PENTAGONE = "PENT";
        this.HEXAGONE = "HEXA";
        this.AUTRE_POLY = "POLY";

        this.ids = [{name: "carre", id: this.CARRE}, {name: "rectangle", id: this.RECTANGLE},
            {name: "trapeze", id: this.TRAPEZE},
            {name: "trapeze_rectangle", id: this.TRAPEZE_RECTANGLE},
            {name: "trapeze_isocele", id: this.TRAPEZE_ISOCELE},
            {name: "parallelogramme", id: this.PARALLELOGRAMME},
            {name: "triangle_isocele", id: this.TRIANGLE_ISOCELE}, {
                name: "triangle_rectangle",
                id: this.TRIANGLE_RECTANGLE
            },
            {name: "triangle", id: this.TRIANGLE},
            {name: "losange", id: this.LOSANGE},
            {name: "triangle_isocele_rectangle", id: this.TRIANGLE_ISOCELE_RECTANGLE},
            {
                name: "triangle_equilateral",
                id: this.TRIANGLE_EQUILATERAL
            },
            {name: "cerf-volant", id: this.CERF_VOLANT},
            {name: "pointe_de_fleche", id: this.POINTE_DE_FLECHE},
            {name: "quadrilatere_irregulier", id: this.QUADRILATERE_IRR},
            {name: "pentagone", id: this.PENTAGONE},
            {name: "hexagone", id: this.HEXAGONE},
            {name: "autre_poly", id: this.AUTRE_POLY}];
    }

    getIDs() {
        return this.ids;
    }

}
/**
 * @interface
 * @classdesc Interface Asset, défini un asset, soit un élément du canvas comme un bouton ou une grille
 * @author Vincent Audergon
 * @version 1.0
 */
class Asset {

    /**
     * Retourne la liste des éléments PIXI d'un asset
     * @return {Object[]} les éléments PIXI
     */
    getPixiChildren(){}

    /**
     * Retourne la position y du composant
     * @return {number} posY
     */
    getY(){}
    /**
     * Retourne la position x du composant
     * @return {number} posX
     */
    getX(){}
    setY(y){}
    setX(x){}
    getWidth(){}
    getHeight(){}
    setVisible(visible){}

}
/**
 * @classdesc Asset bouton
 * @author Vincent Audergon
 * @version 1.0
 */
class Button extends Asset {

    /**
     * Constructeur de l'asset bouton
     * @param {double} x Coordonnée X
     * @param {double} y Coordonnée Y
     * @param {string} label Le texte du bouton
     * @param {int} bgColor La couleur du bouton
     * @param {int} fgColor La couleur du texte
     */
    constructor(x, y, label, bgColor, fgColor, autofit = false, width = 150) {
        super();
        /** @type {double} la coordonée x */
        this.x = x;
        /** @type {double} la coordonée y */
        this.y = y;
        /** @type {string} le texte du bouton */
        this.text = label;
        /** @type {int} la couleur de fond du bouton */
        this.bgColor = bgColor;
        /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
        this.graphics = new PIXI.Graphics();
        /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
        this.lbl = new PIXI.Text(this.text, {fontFamily: 'Arial', fontSize: 18, fill: fgColor, align: 'center'})

        this.height = 0;
        this.autofit = autofit;
        this.width = width;
        /** @type {function} fonction de callback appelée lorsqu'un click est effectué sur le bouton */
        this.onClick = function () {
            console.log('Replace this action with button.setOnClick')
        };
        this.init();
    }

    /**
     * Initialise les éléments qui composent le bouton
     */
    init() {
        this.setText(this.text);
        this.graphics.interactive = true;
        this.graphics.buttonMode = true;
        this.graphics.on('pointerdown', function () {
            this.onClick()
        }.bind(this));
    }


    /**
     * Défini le texte à afficher sur le bouton
     * @param {string} text Le texte à afficher
     */
    setText(text) {
        this.lbl.text = text;
        this.update();
    }

    update(){
        let buttonWidth = this.getWidth();
        let buttonHeight = this.lbl.height * 1.5;
        this.height = buttonHeight;
        this.graphics.clear();
        this.graphics.beginFill(this.bgColor);
        this.graphics.drawRect(this.x - buttonWidth / 2, this.y - buttonHeight / 2, buttonWidth, buttonHeight);
        this.graphics.endFill();
        this.lbl.anchor.set(0.5);
        this.lbl.x = this.x;
        this.lbl.y = this.y;
    }

    /**
     * Défini la fonction de callback à appeler après un click sur le bouton
     * @param {function} onClick La fonction de callback
     */
    setOnClick(onClick) {
        this.onClick = onClick;
    }

    /**
     * Retourne les éléments PIXI du bouton
     * @return {Object[]} les éléments PIXI qui composent le bouton
     */
    getPixiChildren() {
        return [this.graphics, this.lbl];
    }


    updateFont(font) {
        this.lbl.style.fontFamily = font;
    }

    getHeight() {
        return this.height;
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {
        for (let element of this.getPixiChildren()) {
            element.visible = visible
        }
    }

    setY(y) {
        this.y = y;
        this.update();
    }

    setX(x) {
        this.x = x;
        this.update();
    }


    getWidth() {
        return this.autofit ? (this.width < this.lbl.width + 20 ? this.lbl.width + 20 : this.width) : this.width;
    }
}
class CheckBox extends Asset {

    /**
     * Créé le composant graphic
     */
    constructor(x = 0, y = 0, text = '', sizeFactor=1) {
        super();
        this.y = y;
        this.x = x;
        this.text = text;
        this.sizeFactor = sizeFactor;

        this.selected = false;

        this.elements = {
            square: new PIXI.Graphics(),
            line1: new PIXI.Graphics(),
            line2: new PIXI.Graphics(),
            text: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 18, fill: 0x000000, align: 'left', breakWords:true, wordWrap:true})
        };

        this.elements.square.interactive = true;
        this.elements.square.buttonMode = true;
        this.elements.square.on('pointerdown', function () {
            this.select();
        }.bind(this));

        this.draw();
    }


    /**
     * Modifie la position de la checkbox.
     * @param x {number} Position X
     * @param y {number} Position Y
     */
    setPosition(x, y) {
        this.x = x;
        this.y = y;
        this.draw();
    }

    setText(value) {
        this.text = value;
        this.draw();
    }

    /**
     * Affiche ou cache le composant.
     * @param visible {boolean} Est visible ?
     */
    setVisible(visible) {
        if (!visible)
            for (let element of this.getPixiChildren())
                element.visible = false;
        else {
            this.elements.text.visible = true;
            this.elements.square.visible = true;
            this.select(false);
        }
    }

    draw() {
        let line1StartX = this.x + 3*this.sizeFactor;
        let line1StartY = this.y + 20*this.sizeFactor;
        let line1EndX = this.x + 13*this.sizeFactor;
        let line1EndY = this.y + 30*this.sizeFactor;

        let line2StartX = line1EndX-5*this.sizeFactor/4;
        let line2StartY = line1EndY+5*this.sizeFactor/4;
        let line2EndX = this.x + 35*this.sizeFactor;
        let line2EndY = this.y + 8*this.sizeFactor;


        this.elements.square.clear();
        this.elements.square.lineStyle(1, 0x000000, 1);
        this.elements.square.beginFill(0xe0e0e0, 0.25);
        this.elements.square.drawRoundedRect(this.x, this.y, 38*this.sizeFactor, 38*this.sizeFactor, 5);
        this.elements.square.endFill();

        this.elements.line1.clear();
        this.elements.line1.lineStyle(5*this.sizeFactor, 0x000000).moveTo(line1StartX, line1StartY).lineTo(line1EndX, line1EndY);

        this.elements.line2.clear();
        this.elements.line2.lineStyle(5*this.sizeFactor, 0x000000).moveTo(line2StartX, line2StartY).lineTo(line2EndX, line2EndY);


        this.elements.text.text = this.text;
        this.elements.text.style.wordWrapWidth =600-Math.floor( this.x + 5 + 38*this.sizeFactor +25);
        this.elements.text.anchor.set(0.0);
        this.elements.text.x = this.x + 38*this.sizeFactor+5;
        this.elements.text.y = this.y + (38*this.sizeFactor-this.elements.text.height)/2;

    }

    getPixiChildren() {
        let objects = [];
        for (let element of Object.keys(this.elements)) {
            objects.push(this.elements[element]);
        }
        return objects;
    }

    select(toggle = true) {
        if (toggle)
            this.selected = !this.selected;
        this.elements.line1.visible = this.selected;
        this.elements.line2.visible = this.selected;
    }

    isChecked() {
        return this.selected;
    }


    updateFont(font){
        this.elements.text.style.fontFamily = font;
    }


    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }


    setY(y) {
        this.y = y;
        this.setPosition(this.x, this.y);
    }

    setX(x) {
        this.x = x;
        this.setPosition(this.x, this.y);
    }
}
/**
 * @classdesc Asset grille de dessin, retourne une séries de points lorsque l'utilisateur a dessiné un polygone
 * @author Vincent Audergon
 * @version 2.0
 */
class DrawingGrid extends Asset {

    /**
     * Constructeur de la grille de dessin
     * @param {int} col Le nombre de colonnes qui composent la grille
     * @param {int} lines Le nombre de lignes qui composent la grille
     * @param {double} width La largeur / hauteur entre chaque noeuds de la grille
     * @param {Pixi.Stage} stage Le stage Pixi
     * @param {function} onShapeCreated La fonction de callback appelée lorsqu'une forme a été dessinée
     */
    constructor(col, lines, width, stage, onShapeCreated) {
        super();
        /** @type {int} le nombre de colonnes */
        this.col = col;
        /** @type {int} le nombre de lignes */
        this.lines = lines;
        /** @type {double} la largeur / hauteur d'une cellule */
        this.width = width;
        /** @type {PIXI.Stage} le stage PIXI sur lequel dessiner les éléments de la grille */
        this.stage = stage;
        /** @type {Node[]} la liste des noeuds qui composent la grille */
        this.nodes = [];
        /** @type {Point[]} la liste des points de la forme en cours de dessin */
        this.points = [];
        /** @type {PIXI.Graphics} la liste des lignes dessinées */
        this.strLines = [];
        /** @type {Point} l'origine de la forme en cours de dessin */
        this.origin = new Point(0, 0);
        /** @type {Node} le dernier noeud rencontré lors du dessin */
        this.lastnode = undefined;
        /** @type {PIXI.Graphics} le trait qui suit le curseur lors d'un dessin */
        this.line = undefined;
        /** @type {function} fonction de callback appelée lorsqu'une forme est dessinée */
        this.onShapeCreated = onShapeCreated;
        /** @type {boolean} si un dessin est en cours */
        this.drawing = false;
        this.init();
    }

    /**
     * Initialise la grille de dessin
     */
    init() {
        this.stage.interactive = true;
        this.stage.on('pointermove', this.onPointerMove.bind(this));
        this.stage.on('pointerup', this.onReleased.bind(this));
        for (let c = 0; c < this.col; c++) {
            for (let l = 0; l < this.lines; l++) {
                this.nodes.push(new Node(c * this.width, l * this.width, this.width / 4.5, this));
            }
        }
    }

    /**
     * Réinitialise la grille de dessin
     */
    reset() {
        for (let line of this.strLines) {
            this.stage.removeChild(line);
        }
        this.strLines = [];
        this.points = [];
        this.origin = new Point(0, 0);
        this.drawing = false;
    }

    /**
     * Créer le prochain point du polygone en cours de dessin
     * @param {Node} node le {@link Node} qui défini le nouveau point
     * @return {Point} le nouveau {@link Point}
     */
    createNextPoint(node) {
        let p = new Point(node.x / this.width - this.origin.x, node.y / this.width - this.origin.y);
        p.name = String.fromCharCode(65 + this.points.length);
        return p;
    }

    /**
     * Vérifie qu'un point ne soit pas deja existant dans la grille
     * @param {Point} p le point à controller
     * @return {boolean} si le point existe déjà
     */
    containsPoint(p) {
        for (let point of this.points) {
            if (point.x === p.x && point.y === p.y) return true;
        }
        return false;
    }

    /**
     * Créer le trait affiché sur la grille lorsque l'utilisateur déssine
     * @param {Node} node le noeud de départ
     */
    initLine(node) {
        this.line = new PIXI.Graphics();
        this.stage.addChild(this.line);
        this.line.lineStyle(6, 0xFF0000, 1);
        this.line.moveTo(node.center().x, node.center().y);
    }

    /**
     * Dessine une ligne le dernier noeud rencontré et le noeud donné en argument
     * @param {Node} node le noeud jusqu'au quel faire le trait
     */
    drawStrLine(node) {
        this.stage.removeChild(this.line);
        this.initLine(node);
        let straightLine = new PIXI.Graphics();
        this.strLines.push(straightLine);
        this.stage.addChild(straightLine);
        straightLine.lineStyle(6, 0xFF, 1);
        straightLine.moveTo(this.lastnode.center().x, this.lastnode.center().y);
        straightLine.lineTo(node.center().x, node.center().y);
        this.lastnode = node;
        if (this.onLineDrawn) this.onLineDrawn();
    }

    /**
     * Retourne la liste des éléments PIXI de la grille de dessin
     * @return {Object[]} les éléments PIXI qui composent la grille
     */
    getPixiChildren() {
        let children = [];
        for (let n of this.nodes) {
            if (n.graphics) children.push(n.graphics);
            if(n.point) children.push(n.point);
        }
        return children;
    }

    /**
     * Méthode de callback appelée lorsqu'un click est effectué sur un noeud de la grille.
     * Défini le noeud comme étant le noeud de départ et crée le trait de dessin
     * @param {Event} e l'événement JavaScript
     * @param {Node} node le noeud sur lequel on a clické
     */
    onNodeClicked(e, node) {
        this.lastnode = node;
        this.initLine(node);
        this.drawing = true;
        this.points.push();
        this.origin = this.createNextPoint(node);
        let p = new Point(0, 0);
        p.name = this.origin.name;
        this.points.push(p);
    }

    /**
     * Méthode de callback appelée lorsque le click est relâché
     * @param {Event} e L'événement JavaScript
     */
    onReleased(e) {
        this.drawing = false;
        this.stage.removeChild(this.line);
        this.reset();
    }

    /**
     * Méthode de callback appelée lorsque le curseur bouge.
     * Déssine le trait à la position du curseur
     * @param {Event} e L'événement JavaScript
     */
    onPointerMove(e) {
        if (this.drawing) {
            let position = e.data.getLocalPosition(this.stage);
            this.line.lineTo(position.x, position.y);
            for (let n of this.nodes) {
                if (n.graphics.containsPoint(e.data.getLocalPosition(n.graphics.parent))) this.onNodeEncountered(e, n);
            }
        }
    }

    /**
     * Méthode de callback appelée lorsque que le curseur touche un noeud de la grille.
     * Vérifie si la forme est terminée ou si il faut dessiner un trait droit entre ce noeud et le noeud de départ.
     * @param {Event} e L'événement JavaScript
     * @param {Node} node Le noeud de la grille touché
     */
    onNodeEncountered(e, node) {
        if (this.drawing && this.lastnode !== node) { // Si le noeud rencontré n'est pas le dernier noeud
            let point = this.createNextPoint(node);
            this.drawStrLine(node);
            let len = this.points.length;
            // console.log("Diagonale : ");
            // if (len >= 2) {
            //     let val1 = this.points[len - 2].sub(this.points[len - 1]);
            //     let val2 =  this.points[len - 1].sub(point);
            //     console.log("############################################################");
            //     console.log("Point len - 2 : ");
            //     console.log(this.points[len - 2]);
            //     console.log("Point len - 1 : ");
            //     console.log(this.points[len - 1]);
            //     console.log("Point :");
            //     console.log(point);
            //     console.log("len-2 - len-1 :");
            //     console.log(val1);
            //     console.log("len-1 - len :");
            //     console.log(val2);
            //
            //     if ((this.points[len - 1].x === this.points[len - 2].x && this.points[len - 1].x === point.x) || //Al. horizontal
            //         (this.points[len - 1].y === this.points[len - 2].y && this.points[len - 1].y === point.y) || //Al. vertical
            //         (val1.equals(val2))) { //Al. diagonale
            //         //Si trois points sont allignés on ne crée pas un nouveau mais on déplace le dernier
            //         if (point.equals(this.points[0]) && len >= 3) {
            //             //Si c'est le dernier point, on efface le dernier
            //             this.points.pop();
            //             this.pointsCalcul.push(point);
            //         } else if (!this.containsPoint(point)) {
            //             //Si c'est pas le dernier et qu'il n'exite pas encore on déplace le dernier
            //             point.name = this.points[len - 1].name;
            //             this.points[len - 1] = point;
            //             this.pointsCalcul.push(point);
            //         }
            //     } else if (!this.containsPoint(point)) {
            //         this.points.push(point);
            //         this.pointsCalcul.push(point);
            //     }
            // }
            if (!this.containsPoint(point)) {
                this.points.push(point);
            }
            if (this.points.length >= 3 && point.equals(this.points[0])) { //Sinon, si il correspond à l'origine
                //Fin de la forme

                this.drawStrLine(node);
                this.onShapeCreated(new Shape(0, 0, this.points, this.width, 'unknown', {}, this.points[0]));
                this.reset();
            }
        }
    }

    /**
     * Défini la fonction de callback appelée lorsqu'une ligne est dessinée
     * @param {function} onLineDrawn La fonction de callback
     */
    setOnLineDrawn(onLineDrawn) {
        this.onLineDrawn = onLineDrawn;
    }

    /**
     * Défini la fonction de callback lorsqu'un polygone est créé sur le canvas
     * @param {function} onShapeCreated La fonction de callback
     */
    setOnShapeCreated(onShapeCreated) {
        this.onShapeCreated = onShapeCreated;
    }

}
class ScrollPane extends Asset {


    constructor(x, y, width, height) {
        super();
        this.x = x | 0;
        this.y = y | 0;
        this.width = width | 0;
        this.height = height | 0;
        this.index = 1;

        this.maxIndex = 0;

        this.touchScrollLimiter = 0;
        this.lastTouch = 0;
        this.data = {};
        this.dragging = false;

        this.elements = [];

        this.graphics = {
            elementContainer: new PIXI.Container(),
            scrollBar: new PIXI.Graphics(),
            scrollButton: new PIXI.Graphics()
        };

        this.graphics.scrollButton
            .on('mousedown', this.onDragStart.bind(this))
            .on('touchstart', this.onDragStart.bind(this))
            .on('mouseup', this.onDragEnd.bind(this))
            .on('mouseupoutside', this.onDragEnd.bind(this))
            .on('touchend', this.onDragEnd.bind(this))
            .on('touchendoutside', this.onDragEnd.bind(this))
            .on('mousemove', this.onDragMove.bind(this))
            .on('touchmove', this.onDragMove.bind(this));


        let canvas = document.getElementById('canvas');
        canvas.addEventListener('wheel', function (event) {
            this.index += (event.deltaY > 0 ? (this.height / this.elements.length) : -(this.height / this.elements.length));
            if (this.index > this.height - 31)
                this.index = (this.height - 31);
            else if (this.index <= 1)
                this.index = 1;
            this.scroll();
        }.bind(this));
        canvas.addEventListener('touchmove', function (event) {
            if (this.lastTouch === 0)
                this.lastTouch = event.touches[0].clientY;
            this.touchScrollLimiter += (this.lastTouch - event.touches[0].clientY);
            this.lastTouch = event.touches[0].clientY;
            if ((this.touchScrollLimiter > (this.maxIndex / this.elements.length))
                || (this.touchScrollLimiter < -(this.maxIndex / this.elements.length))) {
                console.log(this.touchScrollLimiter);
                this.index += this.touchScrollLimiter > 0 ? (this.height / this.elements.length) : -(this.height / this.elements.length);
                this.touchScrollLimiter = 0;
                if (this.index > this.height - 31)
                    this.index = (this.height - 31);
                else if (this.index <= 1)
                    this.index = 1;
                this.scroll();
            }
        }.bind(this));
        canvas.addEventListener('touchend', function (event) {
            this.lastTouch = 0;
        }.bind(this));
    }

    setPosition(x, y) {
        this.x = x;
        this.y = y;
        this.init();
    }

    addElements(...elements) {
        for (let element of elements)
            this.elements.push(element);
    }

    init() {

        this.graphics.scrollBar.clear();
        this.graphics.scrollBar.beginFill(0xDDDDDD);
        this.graphics.scrollBar.drawRect(this.x + this.width - 18, this.y, 18, this.height);
        this.graphics.scrollBar.endFill();

        this.graphics.scrollButton.clear();
        this.graphics.scrollButton.beginFill(0x999999);
        this.graphics.scrollButton.drawRect(this.x + this.width - 17, this.y + 1, 16, 30);
        this.graphics.scrollButton.endFill();

        this.graphics.scrollButton.interactive = true;
        this.graphics.scrollButton.buttonMode = true;

        let y = this.y;
        for (let element of this.elements) {
            if (element instanceof Asset) {
                element.setY(y);
                element.setX((this.width-20) / 2);
                y += element.getHeight();
                element.setVisible(this.checkIsInContainer(element.getY(), element.getHeight() / 2));
                for (let pc of element.getPixiChildren())
                    this.graphics.elementContainer.addChild(pc);
            } else if (typeof element.y !== 'undefined' && typeof element.visible !== 'undefined') {
                element.y = y;
                y += (typeof element.height !== 'undefined' ? element.height : 25);
                element.visible = this.checkIsInContainer(element.y);
                this.graphics.elementContainer.addChild(element);
                element.x = this.width / 2;
            }
            y += 5;
        }
        this.maxIndex = y;

        this.graphics.scrollButton.visible = (this.height < this.maxIndex);
        this.graphics.scrollBar.visible = (this.height < this.maxIndex);

        this.scroll();
    }

    clear() {
        this.graphics.elementContainer.removeChildren();
        this.elements = [];
    }

    scroll() {
        if (this.elements.length === 0)
            return;

        this.graphics.scrollButton.position.y = this.index;

        let firstDisplayedIndex = Math.floor((this.elements.length - 1) * (this.index / this.height));

        let posY = 1 + this.y + this.elements[firstDisplayedIndex].getHeight()/2;

        for (let i = 0; i < this.elements.length; i++) {
            let element = this.elements[i];
            if (element instanceof Asset) {
                if (i < firstDisplayedIndex)
                    element.setVisible(false);
                else {
                    element.setY(posY);
                    element.setVisible(this.checkIsInContainer(element.getY()));
                    console.log(element.getY());
                    posY += element.getHeight() + 5;
                }
            } else if (typeof element.y !== 'undefined' && typeof element.visible !== 'undefined') {
                if (i < firstDisplayedIndex)
                    element.visible = false;
                else {
                    element.y = posY;
                    element.visible = this.checkIsInContainer(element.y);
                    posY += element.height + 5;
                }
            }
            if(element instanceof ToggleButton)
                element.updateView();
        }
    }


    checkIsInContainer(posY, compensation = 0) {
        return (posY < (this.y + this.height) && (posY > (this.y + compensation)));
    }

    getPixiChildren() {
        let elements = [];
        for (let e in this.graphics)
            if (this.graphics[e] instanceof Asset)
                for (let element of this.graphics[e].getPixiChildren())
                    elements.push(element);
            else
                elements.push(this.graphics[e]);
        return elements;
    }


    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {

    }

    onDragStart(event) {
        this.data = event.data;
        this.graphics.scrollButton.alpha = 0.5;
        this.dragging = true;
    }

    onDragEnd() {
        this.graphics.scrollButton.alpha = 1;
        this.dragging = false;
        this.data = null;
    }

    onDragMove() {
        if (this.dragging) {
            let newPosition = this.data.getLocalPosition(this.graphics.scrollButton.parent);
            this.index = newPosition.y - this.y;
            if (this.index > this.height - 31)
                this.index = (this.height - 31);
            else if (this.index <= 1)
                this.index = 1;
            this.scroll();
        }
    }
}
class Select extends Asset{


    constructor(height, width, x, y) {
        super();

        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;

        this.button = new Button(x,y,'SELECT',0xFFFFFF,0x000000, false,width);
        this.container = new PIXI.Container();

        this.currentIndex = 0;
        this.elements = [];

        this.button.setOnClick(function () {
            this.container.visible = !this.container.visible;
        }.bind(this));

        this.container.visible = true;
    }

    addElement(element){
        element.setSelect(this);
        this.elements.push(element);
        this.build();
    }


    onClick(data){
        console.log(data);
    }

    build(){
        this.container.removeChildren(0);
        this.container.width = this.width;
        this.container.height = this.height;
        this.container.x = this.x-this.width/2;
        this.container.y = this.y+30;


        let currentY = 0;
        for(let element of this.elements){
            element.build(this.width/2, currentY, this.width);
            currentY+=element.getHeight();
            for(let child of element.getPixiChildren()){
                this.container.addChild(child);
            }
        }


    }


    resize(height, width){
        this.height = height;
        this.width = width;
        this.build();
    }

    setPosition(x,y){
        this.x = x;
        this.y = y;
        this.build();
    }

    getPixiChildren() {
        let elements = [];
        for (let e of this.button.getPixiChildren()){
            elements.push(e);
        }
        elements.push(this.container);
        return elements;
    }



    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {
        this.button.setVisible(visible);
        this.container.visible = visible;
    }
}
class SelectItem extends Asset{

    constructor(text, data=null) {
        super();
        this.selectRef = null;
        this.button = null;
        this.text = text;
        this.data = data;
        this.build(0,0,100);
    }


    build(x,y,width){
        this.button = new Button(x,y,this.text,0xFFFFFF,0x000000, false,width);
        this.button.setOnClick(function () {
            this.selectRef.onClick(this.data);
        }.bind(this));
    }


    getPixiChildren() {
        return this.button ? this.button.getPixiChildren():[];
    }

    setText(text){
        this.text = text;
    }

    setSelect(ref){
        this.selectRef = ref;
    }

    getData(){
        return data;
    }

    setData(data){
        this.data = data;
    }

    getHeight(){
        return this.button != null ? this.button.getHeight() : 0;
    }


    getY() {
        this.button.getY();
    }

    getX() {
        this.button.getX();
    }

    setVisible(visible) {
        this.button.setVisible(visible);
    }
}
(function (pkg){

    const PIXI = pkg.PIXI

    class TextInput extends PIXI.Container{
        constructor(styles){
            super()
            this._input_style = Object.assign(
                {
                    position: 'absolute',
                    background: 'none',
                    border: 'none',
                    outline: 'none',
                    transformOrigin: '0 0',
                    lineHeight: '1'
                },
                styles.input
            )

            if(styles.box)
                this._box_generator = typeof styles.box === 'function' ? styles.box : new DefaultBoxGenerator(styles.box)
            else
                this._box_generator = null

            if(this._input_style.hasOwnProperty('multiline')){
                this._multiline = !!this._input_style.multiline
                delete this._input_style.multiline
            }else
                this._multiline = false

            this._box_cache = {}
            this._previous = {}
            this._dom_added = false
            this._dom_visible = true
            this._placeholder = ''
            this._placeholderColor = 0xa9a9a9
            this._selection = [0,0]
            this._restrict_value = ''
            this._createDOMInput()
            this.substituteText = true
            this._setState('DEFAULT')
            this._addListeners()
        }



        // GETTERS & SETTERS

        get substituteText(){
            return this._substituted
        }

        set substituteText(substitute){
            if(this._substituted==substitute)
                return

            this._substituted = substitute

            if(substitute){
                this._createSurrogate()
                this._dom_visible = false
            }else{
                this._destroySurrogate()
                this._dom_visible = true
            }
            this.placeholder = this._placeholder
            this._update()
        }

        get placeholder(){
            return this._placeholder
        }

        set placeholder(text){
            this._placeholder = text
            if(this._substituted){
                this._updateSurrogate()
                this._dom_input.placeholder = ''
            }else{
                this._dom_input.placeholder = text
            }
        }

        get disabled(){
            return this._disabled
        }

        set disabled(disabled){
            this._disabled = disabled
            this._dom_input.disabled = disabled
            this._setState(disabled ? 'DISABLED' : 'DEFAULT')
        }

        get maxLength(){
            return this._max_length
        }

        set maxLength(length){
            this._max_length = length
            this._dom_input.setAttribute('maxlength', length)
        }

        get restrict(){
            return this._restrict_regex
        }

        set restrict(regex){
            if(regex instanceof RegExp){
                regex = regex.toString().slice(1,-1)

                if(regex.charAt(0) !== '^')
                    regex = '^'+regex

                if(regex.charAt(regex.length-1) !== '$')
                    regex = regex+'$'

                regex = new RegExp(regex)
            }else{
                regex = new RegExp('^['+regex+']*$')
            }

            this._restrict_regex = regex
        }

        get text(){
            return this._dom_input.value
        }

        set text(text){
            this._dom_input.value = text
            if(this._substituted) this._updateSurrogate()
        }

        get htmlInput(){
            return this._dom_input
        }

        focus(){
            if(this._substituted && !this.dom_visible)
                this._setDOMInputVisible(true)

            this._dom_input.focus()

        }

        blur(){
            this._dom_input.blur()
        }

        select(){
            this.focus()
            this._dom_input.select()
        }

        setInputStyle(key,value){
            this._input_style[key] = value
            this._dom_input.style[key] = value

            if(this._substituted && (key==='fontFamily' || key==='fontSize'))
                this._updateFontMetrics()

            if(this._last_renderer)
                this._update()
        }

        destroy(options){
            this._destroyBoxCache()
            super.destroy(options)
        }


        // SETUP

        _createDOMInput(){
            if(this._multiline){
                this._dom_input = document.createElement('textarea')
                this._dom_input.style.resize = 'none'
            }else{
                this._dom_input = document.createElement('input')
                this._dom_input.type = 'text'
            }

            for(let key in this._input_style){
                this._dom_input.style[key] = this._input_style[key]
            }
        }

        _addListeners(){
            this.on('added',this._onAdded.bind(this))
            this.on('removed',this._onRemoved.bind(this))
            this._dom_input.addEventListener('keydown', this._onInputKeyDown.bind(this))
            this._dom_input.addEventListener('input', this._onInputInput.bind(this))
            this._dom_input.addEventListener('keyup', this._onInputKeyUp.bind(this))
            this._dom_input.addEventListener('focus', this._onFocused.bind(this))
            this._dom_input.addEventListener('blur', this._onBlurred.bind(this))
        }

        _onInputKeyDown(e){
            this._selection = [
                this._dom_input.selectionStart,
                this._dom_input.selectionEnd
            ]

            this.emit('keydown',e.keyCode)
        }

        _onInputInput(e){
            if(this._restrict_regex)
                this._applyRestriction()

            if(this._substituted)
                this._updateSubstitution()

            this.emit('input',this.text)
        }

        _onInputKeyUp(e){
            this.emit('keyup',e.keyCode)
        }

        _onFocused(){
            this._setState('FOCUSED')
            this.emit('focus')
        }

        _onBlurred(){
            this._setState('DEFAULT')
            this.emit('blur')
        }

        _onAdded(){
            document.body.appendChild(this._dom_input)
            this._dom_input.style.display = 'none'
            this._dom_added = true
        }

        _onRemoved(){
            document.body.removeChild(this._dom_input)
            this._dom_added = false
        }

        _setState(state){
            this.state = state
            this._updateBox()
            if(this._substituted)
                this._updateSubstitution()
        }



        // RENDER & UPDATE

        // for pixi v4
        renderWebGL(renderer){
            super.renderWebGL(renderer)
            this._renderInternal(renderer)
        }

        // for pixi v4
        renderCanvas(renderer){
            super.renderCanvas(renderer)
            this._renderInternal(renderer)
        }

        // for pixi v5
        render(renderer){
            super.render(renderer)
            this._renderInternal(renderer)
        }

        _renderInternal(renderer){
            this._resolution = renderer.resolution
            this._last_renderer = renderer
            this._canvas_bounds = this._getCanvasBounds()
            if(this._needsUpdate())
                this._update()
        }

        _update(){
            this._updateDOMInput()
            if(this._substituted) this._updateSurrogate()
            this._updateBox()
        }

        _updateBox(){
            if(!this._box_generator)
                return

            if(this._needsNewBoxCache())
                this._buildBoxCache()

            if(this.state==this._previous.state
                && this._box==this._box_cache[this.state])
                return

            if(this._box)
                this.removeChild(this._box)

            this._box = this._box_cache[this.state];
            this.addChildAt(this._box,0)
            this._previous.state = this.state
        }

        _updateSubstitution(){
            if(this.state==='FOCUSED'){
                this._dom_visible = true
                this._surrogate.visible = this.text.length===0
            }else{
                this._dom_visible = false
                this._surrogate.visible = true
            }
            this._updateDOMInput()
            this._updateSurrogate()
        }

        _updateDOMInput(){
            if(!this._canvas_bounds)
                return

            this._dom_input.style.top = this._canvas_bounds.top+'px'
            this._dom_input.style.left = this._canvas_bounds.left+'px'
            this._dom_input.style.transform = this._pixiMatrixToCSS(this._getDOMRelativeWorldTransform())
            this._dom_input.style.opacity = this.worldAlpha
            this._setDOMInputVisible(this.worldVisible && this._dom_visible)

            this._previous.canvas_bounds = this._canvas_bounds
            this._previous.world_transform = this.worldTransform.clone()
            this._previous.world_alpha = this.worldAlpha
            this._previous.world_visible = this.worldVisible
        }

        _applyRestriction(){
            if(this._restrict_regex.test(this.text)){
                this._restrict_value = this.text
            }else{
                this.text = this._restrict_value
                this._dom_input.setSelectionRange(
                    this._selection[0],
                    this._selection[1]
                )
            }
        }


        // STATE COMPAIRSON (FOR PERFORMANCE BENEFITS)

        _needsUpdate(){
            return (
                !this._comparePixiMatrices(this.worldTransform,this._previous.world_transform)
                || !this._compareClientRects(this._canvas_bounds,this._previous.canvas_bounds)
                || this.worldAlpha != this._previous.world_alpha
                || this.worldVisible != this._previous.world_visible
            )
        }

        _needsNewBoxCache(){
            let input_bounds = this._getDOMInputBounds()
            return (
                !this._previous.input_bounds
                || input_bounds.width != this._previous.input_bounds.width
                || input_bounds.height != this._previous.input_bounds.height
            )
        }


        // INPUT SUBSTITUTION

        _createSurrogate(){
            this._surrogate_hitbox = new PIXI.Graphics()
            this._surrogate_hitbox.alpha = 0
            this._surrogate_hitbox.interactive = true
            this._surrogate_hitbox.cursor = 'text'
            this._surrogate_hitbox.on('pointerdown',this._onSurrogateFocus.bind(this))
            this.addChild(this._surrogate_hitbox)

            this._surrogate_mask = new PIXI.Graphics()
            this.addChild(this._surrogate_mask)

            this._surrogate = new PIXI.Text('',{})
            this.addChild(this._surrogate)

            this._surrogate.mask = this._surrogate_mask

            this._updateFontMetrics()
            this._updateSurrogate()
        }

        _updateSurrogate(){
            let padding = this._deriveSurrogatePadding()
            let input_bounds = this._getDOMInputBounds()

            this._surrogate.style = this._deriveSurrogateStyle()
            this._surrogate.style.padding = Math.max.apply(Math,padding)
            this._surrogate.y = this._multiline ? padding[0] : (input_bounds.height-this._surrogate.height)/2
            this._surrogate.x = padding[3]
            this._surrogate.text = this._deriveSurrogateText()

            this._updateSurrogateHitbox(input_bounds)
            this._updateSurrogateMask(input_bounds,padding)
        }

        _updateSurrogateHitbox(bounds){
            this._surrogate_hitbox.clear()
            this._surrogate_hitbox.beginFill(0)
            this._surrogate_hitbox.drawRect(0,0,bounds.width,bounds.height)
            this._surrogate_hitbox.endFill()
            this._surrogate_hitbox.interactive = !this._disabled
        }

        _updateSurrogateMask(bounds,padding){
            this._surrogate_mask.clear()
            this._surrogate_mask.beginFill(0)
            this._surrogate_mask.drawRect(padding[3],0,bounds.width-padding[3]-padding[1],bounds.height)
            this._surrogate_mask.endFill()
        }

        _destroySurrogate(){
            if(!this._surrogate) return

            this.removeChild(this._surrogate)
            this.removeChild(this._surrogate_hitbox)

            this._surrogate.destroy()
            this._surrogate_hitbox.destroy()

            this._surrogate = null
            this._surrogate_hitbox = null
        }

        _onSurrogateFocus(){
            this._setDOMInputVisible(true)
            //sometimes the input is not being focused by the mouseclick
            setTimeout(this._ensureFocus.bind(this),10)
        }

        _ensureFocus(){
            if(!this._hasFocus())
                this.focus()
        }

        _deriveSurrogateStyle(){
            let style = new PIXI.TextStyle()

            for(var key in this._input_style){
                switch(key){
                    case 'color':
                        style.fill = this._input_style.color
                        break
                    case 'fontFamily':
                    case 'fontSize':
                    case 'fontWeight':
                    case 'fontVariant':
                    case 'fontStyle':
                        style[key] = this._input_style[key]
                        break
                    case 'letterSpacing':
                        style.letterSpacing = parseFloat(this._input_style.letterSpacing)
                        break
                }
            }

            if(this._multiline){
                style.lineHeight = parseFloat(style.fontSize)
                style.wordWrap = true
                style.wordWrapWidth = this._getDOMInputBounds().width
            }

            if(this._dom_input.value.length === 0)
                style.fill = this._placeholderColor

            return style
        }

        _deriveSurrogatePadding(){
            let indent = this._input_style.textIndent ? parseFloat(this._input_style.textIndent) : 0

            if(this._input_style.padding && this._input_style.padding.length>0){
                let components = this._input_style.padding.trim().split(' ')

                if(components.length==1){
                    let padding = parseFloat(components[0])
                    return [padding,padding,padding,padding+indent]
                }else if(components.length==2){
                    let paddingV = parseFloat(components[0])
                    let paddingH = parseFloat(components[1])
                    return [paddingV,paddingH,paddingV,paddingH+indent]
                }else if(components.length==4){
                    let padding = components.map(component => {
                        return parseFloat(component)
                    })
                    padding[3] += indent
                    return padding
                }
            }

            return [0,0,0,indent]
        }

        _deriveSurrogateText(){
            return this._dom_input.value.length === 0 ? this._placeholder : this._dom_input.value
        }

        _updateFontMetrics(){
            const style = this._deriveSurrogateStyle()
            const font = style.toFontString()

            this._font_metrics = PIXI.TextMetrics.measureFont(font)
        }


        // CACHING OF INPUT BOX GRAPHICS

        _buildBoxCache(){
            this._destroyBoxCache()

            let states = ['DEFAULT','FOCUSED','DISABLED']
            let input_bounds = this._getDOMInputBounds()

            for(let i in states){
                this._box_cache[states[i]] = this._box_generator(
                    input_bounds.width,
                    input_bounds.height,
                    states[i]
                )
            }

            this._previous.input_bounds = input_bounds
        }

        _destroyBoxCache(){
            if(this._box){
                this.removeChild(this._box)
                this._box = null
            }

            for(let i in this._box_cache){
                this._box_cache[i].destroy()
                this._box_cache[i] = null
                delete this._box_cache[i]
            }
        }


        // HELPER FUNCTIONS

        _hasFocus(){
            return document.activeElement===this._dom_input
        }

        _setDOMInputVisible(visible){
            this._dom_input.style.display = visible ? 'block' : 'none'
        }

        _getCanvasBounds(){
            let rect = this._last_renderer.view.getBoundingClientRect()
            let bounds = {top: rect.top, left: rect.left, width: rect.width, height: rect.height}
            bounds.left += window.scrollX
            bounds.top += window.scrollY
            return bounds
        }

        _getDOMInputBounds(){
            let remove_after = false

            if(!this._dom_added){
                document.body.appendChild(this._dom_input)
                remove_after = true
            }

            let org_transform = this._dom_input.style.transform
            let org_display = this._dom_input.style.display
            this._dom_input.style.transform = ''
            this._dom_input.style.display = 'block'
            let bounds = this._dom_input.getBoundingClientRect()
            this._dom_input.style.transform = org_transform
            this._dom_input.style.display = org_display

            if(remove_after)
                document.body.removeChild(this._dom_input)

            return bounds
        }

        _getDOMRelativeWorldTransform(){
            let canvas_bounds = this._last_renderer.view.getBoundingClientRect()
            let matrix = this.worldTransform.clone()

            matrix.scale(this._resolution,this._resolution)
            matrix.scale(canvas_bounds.width/this._last_renderer.width,
                canvas_bounds.height/this._last_renderer.height)
            return matrix
        }

        _pixiMatrixToCSS(m){
            return 'matrix('+[m.a,m.b,m.c,m.d,m.tx,m.ty].join(',')+')'
        }

        _comparePixiMatrices(m1,m2){
            if(!m1 || !m2) return false
            return (
                m1.a == m2.a
                && m1.b == m2.b
                && m1.c == m2.c
                && m1.d == m2.d
                && m1.tx == m2.tx
                && m1.ty == m2.ty
            )
        }

        _compareClientRects(r1,r2){
            if(!r1 || !r2) return false
            return (
                r1.left == r2.left
                && r1.top == r2.top
                && r1.width == r2.width
                && r1.height == r2.height
            )
        }
    }


    function DefaultBoxGenerator(styles){
        styles = styles || {fill: 0xcccccc}

        if(styles.default){
            styles.focused = styles.focused || styles.default
            styles.disabled = styles.disabled || styles.default
        }else{
            let temp_styles = styles
            styles = {}
            styles.default = styles.focused = styles.disabled = temp_styles
        }

        return function(w,h,state){
            let style = styles[state.toLowerCase()]
            let box = new PIXI.Graphics()

            if(style.fill)
                box.beginFill(style.fill)

            if(style.stroke)
                box.lineStyle(
                    style.stroke.width || 1,
                    style.stroke.color || 0,
                    style.stroke.alpha || 1
                )

            if(style.rounded)
                box.drawRoundedRect(0,0,w,h,style.rounded)
            else
                box.drawRect(0,0,w,h)

            box.endFill()
            box.closePath()

            return box
        }
    }

    pkg.exportTo[0][pkg.exportTo[1]] = TextInput

})(
    typeof PIXI === 'object'
        ? { PIXI: PIXI, exportTo: [PIXI,'TextInput'] }
        : (
            typeof module === 'object'
                ? { PIXI: require('pixi.js'), exportTo: [module,'exports'] }
                : console.warn('[PIXI.TextInput] could not attach to PIXI namespace. Make sure to include this plugin after pixi.js') || {}
        )
)
/**
 * @classdesc Asset bouton
 * @author Vincent Audergon
 * @version 1.0
 */
class ToggleButton extends Asset {

    /**
     * Constructeur de l'asset bouton
     * @param {double} x Coordonnée X
     * @param {double} y Coordonnée Y
     * @param {string} label Le texte du bouton
     * @param {boolean} fitToText Taille du bouton en fonction du texte
     * @param {number} width Taille forcée du bouton
     */
    constructor(x, y, label, fitToText=false, width = 150) {
        super();

        this.fitToText = fitToText;
        this.refToggleGroup = null;
        this.width = width;
        this.height = 0;
        this._data = null;
        this.currentState = 'inactive';
        /** @type {double} la coordonée x */
        this.x = x;
        /** @type {double} la coordonée y */
        this.y = y;
        /** @type {string} le texte du bouton */
        this.text = label;

        this.lastOnClick = function(){
            this.toggle();
            if(this.refToggleGroup != null){
                this.refToggleGroup.updateList(this);
            }
        };

        this.states = {
            active: {
                /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
                graphics:new PIXI.Graphics(),
                /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
                lbl: new PIXI.Text(this.text, { fontFamily: 'Arial', fontSize: 18, fill: 0xFFFFFF, align: 'center' })
            },
            inactive: {
                /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
                graphics:new PIXI.Graphics(),
                /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
                lbl: new PIXI.Text(this.text, { fontFamily: 'Arial', fontSize: 18, fill: 0x000000, align: 'center' })
            },
            disabled: {
                /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
                graphics:new PIXI.Graphics(),
                /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
                lbl: new PIXI.Text(this.text, { fontFamily: 'Arial', fontSize: 18, fill: 0x666666, align: 'center' })
            }
        };

        this.onClick = this.lastOnClick;
        this.init();
        this.updateView();

    }

    /**
     * Initialise les éléments qui composent le bouton
     */
    init() {
        this.setText(this.text);
        for (let state of Object.keys(this.states)) {
            this.states[state].graphics.interactive = true;
            this.states[state].graphics.buttonMode = true;
            this.states[state].graphics.on('pointerdown', function () {
                this.onClick()
            }.bind(this));
        }
    }

    /**
     * Défini le texte à afficher sur le bouton
     * @param {string} text Le texte à afficher
     */
    setText(text) {
        for (let state of Object.keys(this.states)){
            this.states[state].lbl.text = text;
        }
        this.update();
    }


    update(){
        for (let state of Object.keys(this.states)){
            let buttonWidth = this.fitToText ? (20 + this.states[state].lbl.width): this.width;
            let buttonHeight = this.states[state].lbl.height * 1.5;
            this.height = buttonHeight;
            this.states[state].graphics.clear();
            if(state === 'active')
                this.states[state].graphics.beginFill(0x00AA00);
            else if(state === 'inactive')
                this.states[state].graphics.beginFill(0xDDDDDD);
            else
                this.states[state].graphics.beginFill(0xCCCCCC);
            this.states[state].graphics.drawRect(this.x - buttonWidth / 2, this.y - buttonHeight / 2, buttonWidth, buttonHeight);
            this.states[state].graphics.endFill();
            this.states[state].lbl.anchor.set(0.5);
            this.states[state].lbl.x = this.x;
            this.states[state].lbl.y = this.y;
        }
    }

    toggle(){
        this.currentState = (this.currentState === 'active') ? 'inactive' : 'active';
        this.updateView();
    }

    /**
     * Défini la fonction de callback à appeler après un click sur le bouton
     * @param {function} onClick La fonction de callback
     */
    setOnClick(onClick) {
        this.onClick = function () {
            onClick(this);
            this.toggle();
            if(this.refToggleGroup != null){
                this.refToggleGroup.updateList(this);
            }
        };
        this.lastOnClick = this.onClick;
    }

    /**
     * Retourne les éléments PIXI du bouton
     * @return {Object[]} les éléments PIXI qui composent le bouton
     */
    getPixiChildren() {
        return [this.states.inactive.graphics,
            this.states.inactive.lbl,
            this.states.active.graphics,
            this.states.active.lbl,
            this.states.disabled.graphics,
            this.states.disabled.lbl];
    }

    updateView(){
        for (let state of Object.keys(this.states)){
            if(state === this.currentState){
                this.states[state].lbl.visible = true;
                this.states[state].graphics.visible = true;
            } else{
                this.states[state].lbl.visible = false;
                this.states[state].graphics.visible = false;
            }
        }
    }

    isActive(){
        return this.currentState === 'active';
    }

    isEnabled(){
        return this.currentState !== 'disabled';
    }

    set data(data){
        this._data = data;
    }

    get data(){
        return this._data;
    }


    show(){
        this.updateView();
    }

    hide(){
        for (let element of this.getPixiChildren()){
            element.visible = false;
        }
    }

    disable(){
        this.currentState = 'disabled';
        this.onClick = function () {};
        this.updateView();
    }

    enable(){
        this.currentState = 'inactive';
        this.onClick = this.lastOnClick;
        this.updateView();
    }

    setRefToggleGroup(ref){
        this.refToggleGroup = ref;
    }

    updateFont(font){
        for(let state of Object.keys(this.states)){
            this.states[state].lbl.style.fontFamily = font;
        }
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {
        visible = true;
        console.log(this.currentState);
        for (let element of this.getPixiChildren()){
            element.visible = visible;
        }
    }


    setY(y) {
        this.y = y;
        this.update();
    }

    setX(x) {
        this.x = x;
        this.update();
    }


    getHeight() {
        return this.height;
    }

    getWidth() {
        return this.width;
    }
}
class ToggleGroup extends Asset {

    /**
     *
     * @param mode
     * @param buttons
     */
    constructor(mode = 'single', buttons = []) {
        super();
        this.mode = mode;
        this.buttons = buttons;
        for (let btn of buttons) {
            btn.setRefToggleGroup(this);
        }
    }

    addButton(button) {
        if (button != null) {
            button.setRefToggleGroup(this);
            this.buttons.push(button);
        }
    }

    setButtons(buttons = []) {
        this.buttons = buttons;
        for (let btn of buttons) {
            btn.setRefToggleGroup(this);
        }
    }

    getButtons() {
        return this.buttons;
    }

    getActives() {
        let actives = [];
        for (let btn of this.buttons)
            if (btn.currentState === 'active')
                actives.push(btn);
        return actives;
    }

    getInactives() {
        let inactives = [];
        for (let btn of this.buttons)
            if (btn.currentState === 'inactive')
                inactives.push(btn);
        return inactives;
    }

    getPixiChildren() {
        let elements = [];
        for (let toggleButton of this.buttons)
            for (let element of toggleButton.getPixiChildren())
                elements.push(element);
        return elements;
    }

    updateList(button) {
        if (this.mode === 'single' && button.isActive()) {
            for (let btn of this.buttons) {
                if (btn !== button && btn.isActive()) {
                    btn.toggle();
                }
            }
        }
    }

    reset(){
        for (let btn of this.buttons){
            if(btn.isActive())
                btn.toggle();
        }
    }

    show() {
        for (let btn of this.buttons)
            btn.show();
    }

    hide() {
        for (let btn of this.buttons)
            btn.hide();
    }

    updateFont(font){
        for (let btn of this.buttons) {
            btn.updateFont(font);
        }
    }

    clearButtons(){
        this.buttons = [];
    }


    getY() {
        return 0;
    }

    getX() {
        return 0;
    }

    setVisible(visible) {
        for (let btn of this.buttons)
            btn.setVisible(visible);
    }
}
/**
 * @classdesc Gestionnaire de question
 * @author Vincent Audergon
 * @version 1.0
 */
class QuestionUtils {

    /**
     * Constructeur du gestionnaire de questions
     */
    constructor() {
        /** @type {Question[]} la liste des questions */
        this.questions = [];

        this.exerciceNames = null;
        /** @type {int} l'index de la question actuelle */
        this.cqIndex = -1;
    }

    /**
     * Vérifie la réponse donnée à la question courrante
     * @param {string[]} shapes La ou les réponses données (tableau d'id de formes)
     * @return {boolean} si la réponse est correcte
     */
    checkResponse(shapes, ignoreNumber = false) {
        let result = false;
        let question = this.questions[this.cqIndex];
        if (shapes.length === question.responses.length) {
            let correct = 0;
            for (let res of shapes) {
                for (let cor of question.responses) {
                    if (res === cor.value) {
                        correct++;
                        break;
                    }
                }
            }
            if (correct === question.responses.length) {
                result = true;
            }
            if(ignoreNumber && (correct >=1))
                result=true;
        }
        return result;
    }

    getExerciceNames() {
        return this.exerciceNames;
    }

    /**
     * Transforme un exercice en liste de questions
     * @param {JSON} exercice L'exercice obtenu de la DB
     * @return {boolean} Si l'opération s'est bien passé et qu'il y a au minimum 1 question
     */
    loadExercice(exercice) {
        let ok = false;
        this.questions = [];
        if (exercice.questions) {
            for (let question of exercice.questions) {
                let responses = [];
                let traps = [];
                for (let response of question.responses) {
                    if (response.response) {
                        responses.push(response);
                    } else {
                        traps.push(response);
                    }
                }
                this.questions.push(new Question(question.label, responses, traps, question.type === "qcm" ? 0 : 1, question.qcmtype));
            }
            if (this.questions.length > 0) ok = true;
        }
        if (exercice.name) {
            this.exerciceNames = exercice.name;
        }
        return ok;
    }

    /**
     * Retourne la prochaine question ou null si il n'y en a plus
     * @return {Question} la prochaine {@link Question}
     */
    nextQuestion() {
        this.cqIndex++;
        let question = null;
        if (this.questions.length > this.cqIndex) {
            question = this.questions[this.cqIndex];
        }
        return question;
    }

    /**
     * Indique si la liste de questions est terminée
     * @return {boolean} si la liste est terminée
     */
    isFinished() {
        return (this.cqIndex + 1) >= this.questions.length;
    }

    /**
     * Commence la liste de question
     */
    start() {
        this.cqIndex = -1;
        this.shuffle();
    }

    /**
     * Mélange aléatoirement la liste de questions
     */
    shuffle() {
        let j, x, i;
        for (i = this.questions.length - 1; i > 0; i--) {
            j = Math.floor(Math.random() * (i + 1));
            x = this.questions[i];
            this.questions[i] = this.questions[j];
            this.questions[j] = x;
        }
    }

}
/**
 * @classdesc Classe de gestion des polygones
 * @author Vincent Audergon
 * @version 1.0
 */
class ShapeUtils {

    /**
     * Constructeur du gestionnaire de polygones
     */
    constructor() {
        /** @type {Shape[]} les formes chargées dans le worker */
        this.shapes = [];
        /** @type {string[]} le nom des formes dans les différentes langues */
        this.names = [];
    }

    /**
     * Identifie une forme par rapport aux formes chargées avec la fonction loadShapes
     * @param {Shape} shape La forme a identifier
     * @return {JSON} Les noms de la forme et l'id de la forme ({id,names})
     * @example
     *          let shapeUtils = new ShapeUtils();
     *          let {id,names} = shapeUtils.identifyShape(shape);
     */
    identifyShape(shape) {
        let id = null;
        let names = null;
        let res = null;
        let vectors = shape.vectors;

        switch (shape.sides.count) {
            case 3:
                let vector_1 = vectors["a"];
                let vector_2 = vectors["b"];
                let vector_3 = vectors["c"];

                res = this.calcTriangle(vector_1, vector_2, vector_3, shape);
                id = this.getId(res);
                names = this.getNames(id);

                break;
            case 4:
                let vector_14 = vectors["a"];
                let vector_24 = vectors["b"];
                let vector_34 = vectors["c"];
                let vector_44 = vectors["d"];

                res = this.calcQuad(vector_14, vector_24, vector_34, vector_44, shape);
                id = this.getId(res);
                names = this.getNames(id);

                break;
            case 5:
                res = this.isPentagone(vectors);
                id = this.getId(res);
                names = this.getNames(id);
                break;
            case 6:
                id = this.getId("hexagone");
                names = this.getNames(id);
                break;
            default:
                id = this.getId("autre_poly");
                names = this.getNames(id);
                break;
        }
        return {id: id, names: names};
    }

    getId(id) {
        let stringIDs = new StringNames;
        let ids = stringIDs.getIDs();
        let res = "";
        for (let i = 0; i < ids.length; i++) {
            if (id === ids[i].name) {
                res = ids[i].id;
                break;
            }
        }
        return res;
    }

    isPentagone(vectors){
        //TODO:Test des angles
        return "pentagone";

    }

    calcTriangle(vector_1, vector_2, vector_3, shape) {
        let nAngleDroit = this.countRightAngles(shape);
        let rectangle = false;
        let isocele = false;
        let equilat = false;
        let res = "";

        if (nAngleDroit === 1) rectangle = true;

        if (vector_1.norme === vector_2.norme && vector_1.norme === vector_3.norme) equilat = true;

        if (vector_1.norme === vector_2.norme || vector_1.norme === vector_3.norme
            || vector_3.norme === vector_2.norme) isocele = true;

        if (!rectangle && isocele && !equilat) {
            res = "triangle_isocele";
        } else if (rectangle && !isocele && !equilat) {
            res = "triangle_rectangle";
        } else if (rectangle && isocele && !equilat) {
            res = "triangle_isocele_rectangle";
        } else if (!rectangle && isocele && equilat) {
            res = "triangle_equilateral";
        } else {
            res = "triangle";
        }

        return res;

    }

    calcQuad(vector_14, vector_24, vector_34, vector_44, shape) {
        let nAngleDroit = this.countRightAngles(shape);
        let resC = "quadrilatere_irregulier";
        if (nAngleDroit === 4) {
            if (vector_14.norme === vector_24.norme
                && vector_34.norme === vector_44.norme
                && vector_34.norme === vector_14.norme) {
                if (!shape.id.includes("_")) {
                    resC = "carre";
                } else {
                    resC = "carre1";
                }
            } else {
                if (!shape.id.includes("_")) {
                    resC = "rectangle";
                } else {
                    resC = "rectangle1";
                }
            }
        } else if (nAngleDroit === 2) {
            resC = "trapeze_rectangle"
        } else if (vector_14.direction === vector_34.direction
            && vector_24.norme === vector_44.norme
            && vector_14.norme === vector_34.norme
            && vector_24.direction === vector_44.direction) {
            if(shape.symAxis >= 1){
                resC = "losange";
            } else {
                resC = "parallelogramme";
            }
        } else if ((vector_14.direction === vector_34.direction
            && vector_24.norme === vector_44.norme)
            || (vector_24.direction === vector_44.direction
                && vector_14.norme === vector_34.norme)) {
            if (shape.symAxis >= 1) {
                resC = "trapeze_isocele";
            } else {
                resC = "trapeze";
            }
        } else if (vector_14.norme === vector_24.norme && vector_34.norme === vector_44.norme
            || vector_14.norme === vector_44.norme && vector_24.norme === vector_34.norme) {
            let a1 = VectorUtils.calcAngle(vector_14, vector_24, false);
            let a2 = VectorUtils.calcAngle(vector_24, vector_34, false);
            let a3 = VectorUtils.calcAngle(vector_34, vector_44, false);
            let a4 = VectorUtils.calcAngle(vector_44, vector_14, false);
            if (a1 <= 30 && a3 <= 30 || a2 <= 30 && a4 <= 30) {
                resC = "pointe_de_fleche";
            } else {
                resC = "cerf-volant";
            }
        }
        return resC;
    }

    getNames(id) {
        let names = null;
        for (let i = 0; i < this.shapes.length; i++) {
            if (this.shapes[i].id === id) {
                names = this.shapes[i].names;
                break;
            }
        }
        return names;
    }

    /**
     * Compte le nombre d'angles droits d'un polygone
     * @param {Shape} shape le polygone
     * @return {int} le nombre d'angles droits
     */
    countRightAngles(shape) {
        let rights = 0;
        for (let a in shape.angles) {
            let angle = shape.angles[a];
            if (angle.alpha === 90) rights++;
        }
        return rights;
    }

    /**
     * Créer et calcule les propriété des formes contenues dans un JSON.
     * Stocke ensuite les formes et les considère comme étant les formes de références.
     * @param {Shape[]} shapes La liste des formes à charger
     * @return {boolean} Si les formes on bien été chargées
     */
    loadShapes(shapes) {
        let ok = false;
        if (shapes) {
            this.shapes = [];
            for (let shape of shapes) {
                let origin = new Point(shape.points[0].x, shape.points[0].y);
                let s = new Shape(500, 500, shape.points, 50, shape.id, shape.names, origin, shape.Harmos, shape.displayAngle, shape.description);
                this.initShape(s);
                this.shapes.push(s);
            }
            ok = true;
        }
        return ok;
    }

    /**
     * Initialise un polygone et calcule ses propriétés
     * @param {Shape} shape Le polygone à initialiser
     */
    initShape(shape) {
        shape.init();
    }

    /**
     * Retourne un polygone en fonction de l'id donné
     * @param {string} id l'id
     * @return {Shape} le polygone ou undefined si aucun n'est trouvés
     */
    getShapeById(id) {
        for (let shape of this.shapes) {
            if (shape.id === id) return shape;
        }
        return undefined;
    }

    /**
     * Retourne la liste des polygones chargés
     * @return {Shape[]} la liste des polygones
     */
    getShapes() {
        return this.shapes;
    }


}
/**
 * @classdesc classe statique de calculs vectoriels
 * @author Vincent Audergon
 * @version 1.0
 */
class VectorUtils {

    /**
     * Calcule la norme d'un vecteur
     * @param {Vector} v1 le {@link Vector}
     * @return {double} la norme du vecteur
     */
    static calcNorme(v1) {
        return Math.sqrt(Math.pow(v1.getTrueX(), 2) + Math.pow(v1.getTrueY(), 2));
    }

    /**
     * Calcule le produit scalaire de deux vecteur
     * @param {Vector} v1 Le premier {@link Vector}
     * @param {Vector} v2 Le deuxième {@link Vector}
     * @return {double} Le produit scalaire
     */
    static calcScalarProduct(v1, v2) {
        return v1.getTrueX() * v2.getTrueX() + v1.getTrueY() * v2.getTrueY();
    }

    /**
     * Calcule l'angle entre deux vecteurs
     * @param {Vector} v1 Le premier {@link Vector}
     * @param {Vector} v2 Le deuxième {@link Vector}
     * @param {boolean} rad Si l'angle doit être rendu en radiants (degrés par défaut)
     * @return {double} l'angle en degrés
     */
    static calcAngle(v1, v2, rad) {
        let cos = VectorUtils.calcScalarProduct(v1, v2) / (v1.norme * v2.norme);
        cos = -cos;
        let angle = Math.acos(cos);
        let angleDeg = angle * (180 / Math.PI);

        return rad ? angle : Math.round((angleDeg) * 10) / 10;
    }

    /**
     * Indique si deux vecteur sont parallèles (colinéaire)
     * @return {boolean} si les vecteurs sont colinéaires
     */
    static areParallel(v1, v2) {
        return v1.direction === v2.direction;
    }

    /**
     * Retourne la bissectrice de l'angle entre deux vecteurs
     * @param {Vector} angle Le premier {@link Vector}
     * @param {Vector} v Le deuxième {@link Vector}
     * @return {Vector} Un vecteur qui indique la direction de la bissectrice
     */
    static calcBisection(angle, v1, v2) {
        // let a = (angle.alpha / 2 + (v1.direction === 0 ? 180 : v2.direction)); //uniquement si y < 0 ?
        let a = null;
        let res = null;

        if (v2.direction < 180){
            let a = angle.alpha/2;
        } else if (v2.direction > 180) {
            let a = angle.beta/2;
        }

        a *= (Math.PI / 180);
        let bisection = new Line(a, v2.offset);
        return bisection;
    }

    /**
     * Retourne la médiatrice d'un côté
     * @param {Vector} v1 Le {@link Vector}
     * @return {Line} Une droite qui correspond à la médiatrice
     */
    static calcSideBisection(v1) {
        let direction = v1.fullDirection;
        direction *= (Math.PI / 180); //Deg => rad
        let sbis = new Line(direction + Math.PI / 2, new Point(v1.offset.x + (v1.norme * Math.cos(direction)) / 2, v1.offset.y + (v1.norme * Math.sin(direction) / 2)));
        return sbis;
    }

    /**
     * Vérifie si un vecteur se trouve dans un quadrant (de I à IV) en considérant son origine en 0,0
     * @param {Vector} v1 Le {@link Vector}
     * @param {int} number Le numéro du quadrant (1-4)
     * @return {boolean} Si le {@link Vector} se trouve dans le quadrant
     */
    static isInQ(v1, number) {
        switch (number) {
            case 1:
                return v1.way.x > 0 && v1.way.y > 0; //x+, y+1
            case 2:
                return v1.way.x < 0 && v1.way.y > 0; //x-, y+
            case 3:
                return v1.way.x < 0 && v1.way.y < 0; //x-, y-
            case 4:
                return v1.way.x > 0 && v1.way.y < 0; //x+, y-
            default:
                return false;
        }
    }


    /**
     * Retourne la direction d'un vecteur (entre 180° et 0°)
     * @param v1 Le {@link Vector}
     * @return {Vector} La direction sous forme de {@link Vector}
     */
    static calcDirection(v1) {
        let direction = (180 / Math.PI) * Math.atan(v1.getTrueY() / v1.getTrueX());
        if (VectorUtils.isInQ(v1, 4) || VectorUtils.isInQ(v1, 2)) direction += 180;
        return Math.round((direction % 180) * 10) / 10;
    }


    /**
     * Retourne la direction d'un vecteur
     * @param {Vector} v1 Le {@link Vector}
     * @return {Vector} La direction sous forme de {@link Vector}
     */
    static calcFullDirection(v1) {
        let direction = VectorUtils.calcDirection(v1);
        if (direction === 0 && v1.way.x < 0) direction += 180;
        if (v1.way.y < 0) direction += 180
        return direction;
    }

    /**
     * Calcule le centre de gravité d'un polygone
     * @param {Point[]} points les {@link Point} qui composent le polygone
     * @return {Point} Le {@link Point} de gravité
     */
    static calcGravityPoint(points) {
        let sum = { x: 0, y: 0 };
        for (let point of points) {
            sum.x += point.x;
            sum.y += point.y;
        }
        return new Point(sum.x / points.length, sum.y / points.length);
    }

    /**
     * Vérifie si une droite est l'axe de symétrie d'un polygone.
     * @param {Line} l La droite à vérifier
     * @param {Point} g Le centre de gravité d'un polygone
     * @return {boolean} si l'axe est un axe de symétrie
     */
    static isSymAxis(l, g) {
        return l.containsPoint(g);
    }
}

/**
 * @classdesc Définition d'une interface (ihm)
 * @author Vincent Audergon
 * @version 1.0
 */
class Interface {

    /**
     * Constructeur d'une ihm
     * @param {Game} refGame la référence vers la classe de jeu
     * @param {string} scene le nom de la scène utilisée par l'ihm
     */
    constructor(refGame, scene) {
        this.refGame = refGame;
        /** @type {PIXI.Container} la scène PIXI sur laquelle dessiner l'ihm */
        this.scene = refGame.scenes[scene];
        /** @type {string[]} les textes de l'interface dans toutes les langues */
        this.texts = [];
        /** @type {Object[]} les éléments qui composent l'ihm */
        this.elements = [];
    }

    /**
     * Défini la scène de l'ihm
     * @param {PIXI.Container} scene la scène PIXI
     */
    setScene(scene) {
        this.scene = scene;
    }

    /**
     * Définis les éléments PIXI contenus dans la scène
     * @param {Objects} elements la liste des éléments PIXI
     */
    setElements(elements) {
        this.elements = elements;
    }

    /**
     * Défini la liste des textes de l'ihm dans la langue courante
     * @param {string[]} texts la liste des textes
     */
    setTexts(texts) {
        this.texts = texts;
    }

    /**
     * Retourne un texte de l'ihm dans la langue courante
     * @param {string} key la clé du texte
     * @return {string} le texte
     */
    getText(key) {
        return this.texts[key];
    }

    /**
     * Fonction de callback appelée lors d'un changement de langue
     * @param {string} lang
     */
    refreshLang(lang) { }

    /**
     * Retire tous les éléments de l'ihm de la scène PIXI
     */
    clear() {
        for (let element in this.elements) {
            let el = this.elements[element];
            if (el instanceof Asset) {
                for (let c of el.getPixiChildren()) {
                    this.scene.removeChild(c);
                }
            } else {
                this.scene.removeChild(el);
            }
        }
    }

    /**
     * Ajoute tous les éléments de l'ihm dans la scène PIXI
     */
    init() {
        for (let element in this.elements) {
            let el = this.elements[element];
            if (el instanceof Asset) {
                for (let c of el.getPixiChildren()) {
                    this.scene.addChild(c);
                }
            } else {
                this.scene.addChild(el);
            }
        }
        this.refGame.showScene(this.scene);
    }

    /**
     * Affiche l'ihm
     */
    show() { }

}
/**
 * @classdesc IHM de la création d'exercice
 * @author Théo Teixeira
 * @version 1.0
 */

/**
 * Taille de l'entête
 * @type {number} Taille
 */
const HEADER_SIZE = 100;

class Creation extends Interface {

    /**
     * Constructeur de l'ihm de la création d'exercice
     * @param {Game} refGame la référence vers la classe de jeu
     * @param {TrainMode} refTrainMode la référence au mode exercer pour tester les jeux
     */
    constructor(refGame, refTrainMode) {

        super(refGame, "creation");

        this.initialized = false;

        this.refGame = refGame;

        this.refTrainMode = refTrainMode;
        this.generateGame();
    }

    /**
     * Point d'entrée pour l'affichage
     */
    show() {
        this.clear();
        //Create generated elements
        this.generateMissingElements();
        //Setup elements --> cf. setupElements function's documentation
        this.setupElements();

        this.setElements(this.extractElements());

        //Display the current step (0 by default)
        this.updateStepView();

        this.refGame.showText("");
        this.refreshLang(this.refGame.global.resources.getLanguage());

        this.init();
    }

    /**
     * Méthode permettant de mettre à jour tous les composants graphiques lors du changement de langue.
     * @param {string} lang Langue à afficher
     */
    refreshLang(lang) {
        for (let i = 0; i < this.getShapeForCurrentDegree().length; i++) {
            this.steps.specific[0]["form".concat(i + 1)].text = this.refGame.shapeUtils.getNames(this.getShapeForCurrentDegree()[i].id)[lang];
        }

        for (let alert of Object.keys(this.alert))
            this.alert[alert] = this.getText(alert);

        //Setup for all steps's specifics elements
        for (let i = 0; i < this.steps.specific.length; i++) {
            this.steps.specific[i]["title".concat(i)].text = this.getText('step' + i + 'Title');
            this.steps.specific[i]["subtitle".concat(i)].text = this.getText('step' + i + 'Subtitle');

            let subtitle = this.getText('step' + i + 'Subtitle');
            this.lang.push(this.getText('step' + i + 'Title') +".<br>"+ ((typeof subtitle !== 'undefined') ? subtitle : ''));
            //Update size;
            this.steps.specific[i]["subtitle".concat(i)].y = 85 + this.steps.specific[i]["subtitle".concat(i)].height / 2;
        }

        this.steps.specific[0].btnNext0.setText(this.getText('btnNext'));
        this.steps.specific[1].btnBack1.setText(this.getText('btnBack'));
        this.steps.specific[1].btnSaveAndNext.setText(this.getText('btnSaveAndNext'));
        this.steps.specific[2].btnNext2.setText(this.getText('btnNext'));
        this.steps.specific[3].btnNext3.setText(this.getText('btnNext'));
        this.steps.specific[3].btnBack3.setText(this.getText('btnBack'));
        this.steps.specific[4].btnNext4.setText(this.getText('btnNext'));
        this.steps.specific[4].btnBack4.setText(this.getText('btnBack'));
        this.steps.specific[5].btnNext5.setText(this.getText('btnNext'));
        this.steps.specific[5].btnBack5.setText(this.getText('btnBack'));
        this.steps.specific[6].btnNext6.setText(this.getText('btnNextQuestion'));
        this.steps.specific[6].btnBack6.setText(this.getText('btnBack'));
        this.steps.specific[6].btnQuestionEnd.setText(this.getText('btnQuestionEnd'));

        this.steps.specific[0].btnShowShapes.setText(this.getText('btnDisplayShapes'));
        this.steps.specific[0].btnHideShapes.setText(this.getText('btnHideShapes'));

        this.steps.general.info.text = this.refGame.global.resources.getOtherText('info', {
            mode: this.getText('mode'),
            harmos: this.refGame.global.resources.getDegre(),
            lang: lang
        });

        this.steps.specific[1].french.placeholder = this.getText('french');
        this.steps.specific[1].german.placeholder = this.getText('german');
        this.steps.specific[1].english.placeholder = this.getText('english');
        this.steps.specific[1].italian.placeholder = this.getText('italian');

        this.steps.specific[1].englishInputLabel.text = this.getText("english");
        this.steps.specific[1].frenchInputLabel.text = this.getText("french");
        this.steps.specific[1].germanInputLabel.text = this.getText("german");
        this.steps.specific[1].italianInputLabel.text = this.getText("italian");

        this.steps.specific[3].frenchQuestionName.placeholder = this.getText('french');
        this.steps.specific[3].germanQuestionName.placeholder = this.getText('german');
        this.steps.specific[3].englishQuestionName.placeholder = this.getText('english');
        this.steps.specific[3].italianQuestionName.placeholder = this.getText('italian');

        this.steps.specific[3].englishQuestionNameInputLabel.text = this.getText("english");
        this.steps.specific[3].frenchQuestionNameInputLabel.text = this.getText("french");
        this.steps.specific[3].germanQuestionNameInputLabel.text = this.getText("german");
        this.steps.specific[3].italianQuestionNameInputLabel.text = this.getText("italian");

        this.steps.specific[2].questionType.getButtons()[0].setText(this.getText('questionTypeRadio'));
        this.steps.specific[2].questionType.getButtons()[1].setText(this.getText('questionTypeCheckbox'));
        this.steps.specific[2].questionType.getButtons()[2].setText(this.getText('questionTypeDraw'));


        for (let btn of this.steps.specific[4].multipleChoice.getButtons()) {
            btn.setText(this.refGame.shapeUtils.getNames(btn.data)[lang]);
        }
        for (let btn of this.steps.specific[5].singleChoice.getButtons()) {
            btn.setText(this.refGame.shapeUtils.getNames(btn.data)[lang]);

        }

        this.steps.specific[6].allShapeLabel.text = this.getText('resumeAllShapesLabel');
        this.steps.specific[6].trueShapeLabel.text = this.getText('resumeTrueShapesLabel');

        if (this.currentStep === 6)
            this.prepareResume();

        this.steps.specific[7].isNew.setText(this.getText("isExerciceNew"));
        this.steps.specific[7].understandable.setText(this.getText("isExerciceUnderstandable"));
        this.steps.specific[7].btnNext7.setText(this.getText('btnNext'));
        this.steps.specific[8].btnTest.setText(this.getText('step8Test'));
        this.steps.specific[8].btnSubmit.setText(this.getText('step8Submit'));
        this.steps.specific[8].btnDelete.setText(this.getText('step8Delete'));

        this.steps.specific[9].yes9.setText(this.getText("yes"));
        this.steps.specific[9].no9.setText(this.getText("no"));
        this.steps.specific[10].yes10.setText(this.getText("yes"));
        this.steps.specific[10].no10.setText(this.getText("no"));

   }

    generateGame(){
        this.exercice = new ExerciceForSave();

        this.currentStep = 0;
        this.shapesNameDisplayed = false;

        this.labelStyle = new PIXI.TextStyle();
        this.titleStyle = new PIXI.TextStyle();
        this.subtitleStyle = new PIXI.TextStyle();

        this.titleStyle.align = 'center';
        this.titleStyle.fill = 0x000000;
        this.titleStyle.fontFamily = 'Arial';
        this.titleStyle.fontSize = 32;
        this.subtitleStyle.align = 'center';
        this.subtitleStyle.fill = 0x000000;
        this.subtitleStyle.fontFamily = 'Arial';
        this.subtitleStyle.fontSize = 18;
        this.subtitleStyle.wordWrap = true;
        this.subtitleStyle.wordWrapWidth = 550;
        this.labelStyle.fontFamily = 'Arial';
        this.labelStyle.fontSize = 22;
        this.labelStyle.align = 'left';
        this.labelStyle.fill = 0x000000;


        /**
         * Variable contenant les formes pour le degrès actuel
         * @type {{shapes: Array, lastDegree: number}}
         */
        this.shapes = {
            shapes: [],
            lastDegree: -1
        };

        /**
         * Variable contenant tous les messages d'alerte.
         * @type {{noType: string, confirmDeletionText: string, noNameText: string, checkConditionText: string, checkCondition: string, noName: string, confirmDeletion: string, noTypeText: string}}
         */
        this.alert = {
            noName: '',
            noNameText: '',
            noType: '',
            noTypeText: '',
            checkCondition: '',
            checkConditionText: '',
            confirmDeletion: '',
            confirmDeletionText: ''
        };

        /**
         *
         * @type {{general: {}, specific: *[]}}} Variable contenant tous les éléments graphiques pour les différentes interfaces.
         */
        this.steps = {
            specific: [
                // 0 = Choose Harmos and display shapes for degree
                {
                    btnNext0: new Button(450, 570, '', 0x00AA00, 0xFFFFFF),
                    btnShowShapes: new Button(100, 570, '', 0x00AA00, 0xFFFFFF),
                    btnHideShapes: new Button(100, 570, '', 0x00AA00, 0xFFFFFF),
                    title0: new PIXI.Text('', this.titleStyle),
                    subtitle0: new PIXI.Text('', this.subtitleStyle),
                },
                // 1 = Choose title for exercices
                {

                    btnBack1: new Button(100, 570, '', 0x00AA00, 0xFFFFFF),
                    btnSaveAndNext: new Button(400, 570, '', 0x00AA00, 0xFFFFFF, true),
                    title1: new PIXI.Text('', this.titleStyle),
                    subtitle1: new PIXI.Text('', this.subtitleStyle),
                    french: new PIXI.TextInput({
                        input: {
                            fontSize: '16px',
                            padding: '8px',
                            width: '250px',
                            color: '#000000'
                        },
                        box: {
                            default: {fill: 0xFFFFFF, rounded: 12, stroke: {color: 0xCBCEE0, width: 3}},
                            focused: {fill: 0xFFFFFF, rounded: 12, stroke: {color: 0xABAFC6, width: 3}},
                            disabled: {fill: 0xDBDBDB, rounded: 12}
                        }
                    }),
                    german: new PIXI.TextInput({
                        input: {
                            fontSize: '16px',
                            padding: '8px',
                            width: '250px',
                            color: '#000000'
                        },
                        box: {
                            default: {fill: 0xFFFFFF, rounded: 12, stroke: {color: 0xCBCEE0, width: 3}},
                            focused: {fill: 0xFFFFFF, rounded: 12, stroke: {color: 0xABAFC6, width: 3}},
                            disabled: {fill: 0xDBDBDB, rounded: 12}
                        }
                    }),
                    english: new PIXI.TextInput({
                        input: {
                            fontSize: '16px',
                            padding: '8px',
                            width: '250px',
                            color: '#000000'
                        },
                        box: {
                            default: {fill: 0xFFFFFF, rounded: 12, stroke: {color: 0xCBCEE0, width: 3}},
                            focused: {fill: 0xFFFFFF, rounded: 12, stroke: {color: 0xABAFC6, width: 3}},
                            disabled: {fill: 0xDBDBDB, rounded: 12}
                        }
                    }),
                    italian: new PIXI.TextInput({
                        input: {
                            fontSize: '16px',
                            padding: '8px',
                            width: '250px',
                            color: '#000000'
                        },
                        box: {
                            default: {fill: 0xFFFFFF, rounded: 12, stroke: {color: 0xCBCEE0, width: 3}},
                            focused: {fill: 0xFFFFFF, rounded: 12, stroke: {color: 0xABAFC6, width: 3}},
                            disabled: {fill: 0xDBDBDB, rounded: 12}
                        }
                    }),
                    frenchInputLabel: new PIXI.Text('', this.labelStyle),
                    englishInputLabel: new PIXI.Text('', this.labelStyle),
                    germanInputLabel: new PIXI.Text('', this.labelStyle),
                    italianInputLabel: new PIXI.Text('', this.labelStyle),

                },
                // 2 = Choose type of question
                {
                    btnNext2: new Button(450, 570, '', 0x00AA00, 0xFFFFFF),
                    title2: new PIXI.Text('', this.titleStyle),
                    subtitle2: new PIXI.Text('', this.subtitleStyle),
                    questionType: new ToggleGroup('single', [
                        new ToggleButton(300, 200, '', true),
                        new ToggleButton(300, 250, '', true),
                        new ToggleButton(300, 300, '', true)
                    ])

                },
                // 3 = Choose name for question
                {
                    btnNext3: new Button(450, 570, '', 0x00AA00, 0xFFFFFF),
                    btnBack3: new Button(100, 570, '', 0x00AA00, 0xFFFFFF),
                    title3: new PIXI.Text('', this.titleStyle),
                    subtitle3: new PIXI.Text('', this.subtitleStyle),
                    frenchQuestionName: new PIXI.TextInput({
                        input: {
                            multiline: true,
                            height: '60px',
                            fontSize: '16px',
                            padding: '8px',
                            width: '250px',
                            color: '#000000'
                        },
                        box: {
                            default: {fill: 0xFFFFFF, rounded: 12, stroke: {color: 0xCBCEE0, width: 3}},
                            focused: {fill: 0xFFFFFF, rounded: 12, stroke: {color: 0xABAFC6, width: 3}},
                            disabled: {fill: 0xDBDBDB, rounded: 12}
                        }
                    }),
                    germanQuestionName: new PIXI.TextInput({
                        input: {
                            multiline: true,
                            height: '60px',
                            fontSize: '16px',
                            padding: '8px',
                            width: '250px',
                            color: '#000000'
                        },
                        box: {
                            default: {fill: 0xFFFFFF, rounded: 12, stroke: {color: 0xCBCEE0, width: 3}},
                            focused: {fill: 0xFFFFFF, rounded: 12, stroke: {color: 0xABAFC6, width: 3}},
                            disabled: {fill: 0xDBDBDB, rounded: 12}
                        }
                    }),
                    englishQuestionName: new PIXI.TextInput({
                        input: {
                            multiline: true,
                            height: '60px',
                            fontSize: '16px',
                            padding: '8px',
                            width: '250px',
                            color: '#000000'
                        },
                        box: {
                            default: {fill: 0xFFFFFF, rounded: 12, stroke: {color: 0xCBCEE0, width: 3}},
                            focused: {fill: 0xFFFFFF, rounded: 12, stroke: {color: 0xABAFC6, width: 3}},
                            disabled: {fill: 0xDBDBDB, rounded: 12}
                        }
                    }),
                    italianQuestionName: new PIXI.TextInput({
                        input: {
                            multiline: true,
                            height: '60px',
                            fontSize: '16px',
                            padding: '8px',
                            width: '250px',
                            color: '#000000'
                        },
                        box: {
                            default: {fill: 0xFFFFFF, rounded: 12, stroke: {color: 0xCBCEE0, width: 3}},
                            focused: {fill: 0xFFFFFF, rounded: 12, stroke: {color: 0xABAFC6, width: 3}},
                            disabled: {fill: 0xDBDBDB, rounded: 12}
                        }
                    }),
                    frenchQuestionNameInputLabel: new PIXI.Text('', this.labelStyle),
                    englishQuestionNameInputLabel: new PIXI.Text('', this.labelStyle),
                    germanQuestionNameInputLabel: new PIXI.Text('', this.labelStyle),
                    italianQuestionNameInputLabel: new PIXI.Text('', this.labelStyle)
                },
                // 4 = Choose shapes - Multiple choice
                {
                    btnNext4: new Button(450, 570, '', 0x00AA00, 0xFFFFFF),
                    btnBack4: new Button(100, 570, '', 0x00AA00, 0xFFFFFF),
                    title4: new PIXI.Text('', this.titleStyle),
                    subtitle4: new PIXI.Text('', this.subtitleStyle),
                    multipleChoice: new ToggleGroup('multiple')
                },
                // 5 = Choose shape - Unique choice
                {
                    btnNext5: new Button(450, 570, '', 0x00AA00, 0xFFFFFF),
                    btnBack5: new Button(100, 570, '', 0x00AA00, 0xFFFFFF),
                    title5: new PIXI.Text('', this.titleStyle),
                    subtitle5: new PIXI.Text('', this.subtitleStyle),
                    singleChoice: new ToggleGroup()
                },

                // 6 = Resume
                {
                    btnQuestionEnd: new Button(275, 570, '', 0x00AA00, 0xFFFFFF, true),
                    btnNext6: new Button(450, 570, '', 0x00AA00, 0xFFFFFF, true),
                    btnBack6: new Button(100, 570, '', 0x00AA00, 0xFFFFFF),
                    title6: new PIXI.Text('', this.titleStyle),
                    subtitle6: new PIXI.Text('', this.subtitleStyle),
                    allShape: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 16, fill: 0x000000, align: 'center'}),
                    trueShape: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 16, fill: 0x000000, align: 'center'}),
                    questionLabel: new PIXI.Text('', {
                        fontFamily: 'Arial',
                        fontSize: 19,
                        fill: 0x000000,
                        align: 'center',
                        fontWeight: 'bold',
                        breakWords: true,
                        wordWrap: true,
                        wordWrapWidth: 550
                    }),
                    allShapeLabel: new PIXI.Text('', {
                        fontFamily: 'Arial',
                        fontSize: 18,
                        fill: 0x000000,
                        align: 'center',
                        fontWeight: 'bold',
                        breakWords: true,
                        wordWrap: true,
                        wordWrapWidth: 280
                    }),
                    trueShapeLabel: new PIXI.Text('', {
                        fontFamily: 'Arial',
                        fontSize: 18,
                        fill: 0x000000,
                        align: 'center',
                        fontWeight: 'bold',
                        breakWords: true,
                        wordWrap: true,
                        wordWrapWidth: 280
                    }),

                },
                // 7 = Choose answers for checkbox
                {
                    title7: new PIXI.Text('', this.titleStyle),
                    subtitle7: new PIXI.Text('', this.subtitleStyle),
                    isNew: new CheckBox(25, HEADER_SIZE + 100, '', 0.9),
                    understandable: new CheckBox(25, HEADER_SIZE + 200, '', 0.9),
                    btnNext7: new Button(450, 570, '', 0x00AA00, 0xFFFFFF)

                },
                // 8 = Confirmation soumission
                {
                    title8: new PIXI.Text('', this.titleStyle),
                    subtitle8: new PIXI.Text('', this.subtitleStyle),
                    btnSubmit: new Button(300, 200, '', 0x00AA00, 0xFFFFFF, false, 250),
                    btnDelete: new Button(300, 300, '', 0x00AA00, 0xFFFFFF, false, 250),
                    btnTest: new Button(300, 400, '', 0x00AA00, 0xFFFFFF, false, 250)
                },
                // 9 = Confirmation suppression
                {
                    title9: new PIXI.Text('', this.titleStyle),
                    subtitle9: new PIXI.Text('', this.subtitleStyle),
                    yes9: new Button(200, 300, '', 0x00AA00, 0xFFFFFF),
                    no9: new Button(400, 300, '', 0x00AA00, 0xFFFFFF)
                },
                // 10 = alert pas de question
                {
                    title10: new PIXI.Text('', this.titleStyle),
                    subtitle10: new PIXI.Text('', this.subtitleStyle),
                    yes10: new Button(200, 300, '', 0x00AA00, 0xFFFFFF),
                    no10: new Button(400, 300, '', 0x00AA00, 0xFFFFFF)
                }
            ],
            general: {
                info: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 16, fill: 0x00, align: 'left'})
            }
        };

        this.lang = [];
    }


    /**
     * Méthode retournant tous les éléments graphiques
     * @param {boolean} onlySteps Choix de prendre uniquement les éléments correspondant à une étape spécifique
     * @return {{}} JSON des éléments
     */
    extractElements(onlySteps = false) {
        let elements = {};
        if (!onlySteps)
            for (let element of Object.keys(this.steps.general))
                elements[element] = this.steps.general[element];
        for (let step of this.steps.specific)
            for (let element of Object.keys(step))
                elements[element] = step[element];
        return elements;
    }


    /**
     * Méthode de génération de tous les éléments provenant de la base de données
     */
    generateMissingElements() {
        let x = 120;
        let y = HEADER_SIZE + 50;
        let spaceBetweenLine = 40;
        for (let element of Object.keys(this.steps.specific[0])) {
            if (element.startsWith("form")) {
                delete this.steps.specific[0][element];
            }
        }
        for (let i = 1; i <= this.getShapeForCurrentDegree().length; i++) {
            this.steps.specific[0]["form".concat(i)] = new PIXI.Text('', {
                fontFamily: 'Arial',
                fontSize: 16,
                fill: 0x000000,
                align: 'left'
            });
            this.steps.specific[0]["form".concat(i)].x = x;
            this.steps.specific[0]["form".concat(i)].y = y;
            this.steps.specific[0]["form".concat(i)].anchor.set(0.5);
            y += spaceBetweenLine;
            if (this.getShapeForCurrentDegree().length / 2 - 1 < i && x === 120) {
                x = 420;
                y = HEADER_SIZE + 50;
            }
        }

        x = 150;
        y = HEADER_SIZE + 50;
        spaceBetweenLine = 40;
        this.steps.specific[4].multipleChoice.clearButtons();
        this.steps.specific[5].singleChoice.clearButtons();
        for (let i = 0; i < this.getShapeForCurrentDegree().length; i++) {
            let multiple = new ToggleButton(x, y, '', false, 250);
            multiple.data = this.getShapeForCurrentDegree()[i].id;
            let single = new ToggleButton(x, y, '', false, 250);
            single.data = this.getShapeForCurrentDegree()[i].id;
            this.steps.specific[4].multipleChoice.addButton(multiple);
            this.steps.specific[5].singleChoice.addButton(single);
            y += spaceBetweenLine;
            if (this.getShapeForCurrentDegree().length / 2 - 1 <= i && x === 150) {
                x = 450;
                y = HEADER_SIZE + 50;
            }

        }
    }


    /**
     * Paramétrage de la mise en page et des actions sur les différents boutons
     */
    setupElements() {

        this.exercice.degree = this.refGame.global.resources.getDegre();

        /////////////////////////////////////
        //              General            //
        /////////////////////////////////////

        this.steps.general.info.x = 10;
        this.steps.general.info.y = 10;
        this.steps.general.info.anchor.set(0.0);
        this.steps.general.info.visible = false;

        this.steps.specific[0].btnNext0.setOnClick(function () {
            this.chooseNextStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[1].btnBack1.setOnClick(function () {
            this.chooseBackStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[1].btnSaveAndNext.setOnClick(function () {
            this.chooseNextStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[2].btnNext2.setOnClick(function () {
            this.chooseNextStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[3].btnNext3.setOnClick(function () {
            this.chooseNextStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[3].btnBack3.setOnClick(function () {
            this.chooseBackStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[4].btnNext4.setOnClick(function () {
            this.chooseNextStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[4].btnBack4.setOnClick(function () {
            this.chooseBackStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[5].btnNext5.setOnClick(function () {
            this.chooseNextStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[5].btnBack5.setOnClick(function () {
            this.chooseBackStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[6].btnNext6.setOnClick(function () {
            this.chooseNextStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[6].btnBack6.setOnClick(function () {
            this.chooseBackStep();
            this.updateStepView();
        }.bind(this));
        this.steps.specific[7].btnNext7.setOnClick(function () {
            this.chooseNextStep();
            this.updateStepView();
        }.bind(this));


        for (let i = 0; i < this.steps.specific.length; i++) {
            this.steps.specific[i]["title".concat(i)].anchor.set(0.5);
            this.steps.specific[i]["subtitle".concat(i)].anchor.set(0.5);
            this.steps.specific[i]["title".concat(i)].x = 300;
            this.steps.specific[i]["title".concat(i)].y = 50;
            this.steps.specific[i]["subtitle".concat(i)].x = 300;
            this.steps.specific[i]["subtitle".concat(i)].y = 85;
        }

        /////////////////////////////////////
        //              Step 0             //
        /////////////////////////////////////

        for (let potentialForm of Object.keys(this.steps.specific[0])) {
            if (potentialForm.startsWith("form"))
                this.steps.specific[0][potentialForm].visible = false;
        }

        this.steps.specific[0].btnShowShapes.setOnClick(function () {
            for (let potentialForm of Object.keys(this.steps.specific[0])) {
                if (potentialForm.startsWith("form"))
                    this.steps.specific[0][potentialForm].visible = true;
                this.shapesNameDisplayed = true;
                this.setButtonVisible(this.steps.specific[0].btnHideShapes, true);
                this.setButtonVisible(this.steps.specific[0].btnShowShapes, false);
            }
        }.bind(this));

        this.steps.specific[0].btnHideShapes.setOnClick(function () {
            for (let potentialForm of Object.keys(this.steps.specific[0])) {
                if (potentialForm.startsWith("form"))
                    this.steps.specific[0][potentialForm].visible = false;
                this.shapesNameDisplayed = false;
                this.setButtonVisible(this.steps.specific[0].btnHideShapes, false);
                this.setButtonVisible(this.steps.specific[0].btnShowShapes, true);
            }
        }.bind(this));

        this.setButtonVisible(this.steps.specific[0].btnHideShapes, this.shapesNameDisplayed);


        /////////////////////////////////////
        //              Step 1             //
        /////////////////////////////////////


        this.steps.specific[1].french.x = 150;
        this.steps.specific[1].german.x = 150;
        this.steps.specific[1].english.x = 150;
        this.steps.specific[1].italian.x = 150;

        this.steps.specific[1].french.y = 120;
        this.steps.specific[1].german.y = 200;
        this.steps.specific[1].english.y = 280;
        this.steps.specific[1].italian.y = 360;

        this.steps.specific[1].french.width = 300;
        this.steps.specific[1].german.width = 300;
        this.steps.specific[1].english.width = 300;
        this.steps.specific[1].italian.width = 300;

        this.steps.specific[1].englishInputLabel.x = 50;
        this.steps.specific[1].frenchInputLabel.x = 50;
        this.steps.specific[1].germanInputLabel.x = 50;
        this.steps.specific[1].italianInputLabel.x = 50;

        this.steps.specific[1].frenchInputLabel.y = 125;
        this.steps.specific[1].germanInputLabel.y = 205;
        this.steps.specific[1].englishInputLabel.y = 285;
        this.steps.specific[1].italianInputLabel.y = 365;

        this.steps.specific[1].englishInputLabel.anchor.set(0);
        this.steps.specific[1].frenchInputLabel.anchor.set(0);
        this.steps.specific[1].germanInputLabel.anchor.set(0);
        this.steps.specific[1].italianInputLabel.anchor.set(0);

        /////////////////////////////////////
        //              Step 2             //
        /////////////////////////////////////

        this.steps.specific[2].questionType.getButtons()[0].data = {
            nextStep: 3,
            type: 'qcm',
            qcmtype: 'radio'
        };
        this.steps.specific[2].questionType.getButtons()[1].data = {
            nextStep: 3,
            type: 'qcm',
            qcmtype: 'checkbox'
        };
        this.steps.specific[2].questionType.getButtons()[2].data = {
            nextStep: 3,
            type: 'draw',
            qcmtype: null
        };

        /////////////////////////////////////
        //              Step 3             //
        /////////////////////////////////////

        this.steps.specific[3].frenchQuestionName.x = 150;
        this.steps.specific[3].germanQuestionName.x = 150;
        this.steps.specific[3].englishQuestionName.x = 150;
        this.steps.specific[3].italianQuestionName.x = 150;

        this.steps.specific[3].frenchQuestionName.y = 120;
        this.steps.specific[3].germanQuestionName.y = 200;
        this.steps.specific[3].englishQuestionName.y = 280;
        this.steps.specific[3].italianQuestionName.y = 360;

        this.steps.specific[3].frenchQuestionName.width = 300;
        this.steps.specific[3].germanQuestionName.width = 300;
        this.steps.specific[3].englishQuestionName.width = 300;
        this.steps.specific[3].italianQuestionName.width = 300;

        this.steps.specific[3].englishQuestionNameInputLabel.x = 50;
        this.steps.specific[3].frenchQuestionNameInputLabel.x = 50;
        this.steps.specific[3].germanQuestionNameInputLabel.x = 50;
        this.steps.specific[3].italianQuestionNameInputLabel.x = 50;

        this.steps.specific[3].frenchQuestionNameInputLabel.y = 125;
        this.steps.specific[3].germanQuestionNameInputLabel.y = 205;
        this.steps.specific[3].englishQuestionNameInputLabel.y = 285;
        this.steps.specific[3].italianQuestionNameInputLabel.y = 365;

        this.steps.specific[3].englishQuestionNameInputLabel.anchor.set(0);
        this.steps.specific[3].frenchQuestionNameInputLabel.anchor.set(0);
        this.steps.specific[3].germanQuestionNameInputLabel.anchor.set(0);
        this.steps.specific[3].italianQuestionNameInputLabel.anchor.set(0);

        /////////////////////////////////////
        //              Step 6             //
        /////////////////////////////////////

        this.steps.specific[6].questionLabel.anchor.set(0.5);
        this.steps.specific[6].allShape.anchor.set(0.5);
        this.steps.specific[6].trueShape.anchor.set(0.5);
        this.steps.specific[6].trueShapeLabel.anchor.set(0.5);
        this.steps.specific[6].allShapeLabel.anchor.set(0.5);

        this.steps.specific[6].btnQuestionEnd.setOnClick(function () {
            this.exercice.endQuestion();
            this.currentStep = 7;
            this.updateStepView();
        }.bind(this));

        /////////////////////////////////////
        //              Step 8             //
        /////////////////////////////////////

        this.steps.specific[8].btnDelete.setOnClick(function () {
            this.currentStep = 10;
            this.updateStepView();
        }.bind(this));
        this.steps.specific[8].btnSubmit.setOnClick(function () {
            this.currentStep = 9;
            this.updateStepView();
        }.bind(this));
        this.steps.specific[8].btnTest.setOnClick(function () {
            this.testExercice();
        }.bind(this));

        /////////////////////////////////////
        //              Step 9             //
        /////////////////////////////////////

        this.steps.specific[9].no9.setOnClick(function () {
            this.currentStep = 8;
            this.updateStepView();
        }.bind(this));
        this.steps.specific[9].yes9.setOnClick(function () {
            this.submitExercice();
        }.bind(this));

        /////////////////////////////////////
        //              Step 10            //
        /////////////////////////////////////

        this.steps.specific[10].no10.setOnClick(function () {
            this.currentStep = 8;
            this.updateStepView();
        }.bind(this));
        this.steps.specific[10].yes10.setOnClick(function () {
            this.deleteExercice();
        }.bind(this));
    }

    /**
     * Mise à jour de l'interface en fonction de l'étape.
     */
    updateStepView() {
        this.hideAll();
        for (let elementToDisplay of Object.keys(this.steps.specific[this.currentStep])) {
            if (this.currentStep === 0 &&
                ((elementToDisplay.startsWith("form") && !this.shapesNameDisplayed)
                    || elementToDisplay.startsWith("btnHideShapes") && !this.shapesNameDisplayed))
                continue;
            if (this.steps.specific[this.currentStep][elementToDisplay] instanceof Button)
                this.setButtonVisible(this.steps.specific[this.currentStep][elementToDisplay], true);
            else if (this.steps.specific[this.currentStep][elementToDisplay] instanceof ToggleButton
                || this.steps.specific[this.currentStep][elementToDisplay] instanceof ToggleGroup)
                this.steps.specific[this.currentStep][elementToDisplay].show();
            else if (this.steps.specific[this.currentStep][elementToDisplay] instanceof CheckBox)
                this.steps.specific[this.currentStep][elementToDisplay].setVisible(true);
            else
                this.steps.specific[this.currentStep][elementToDisplay].visible = true;
        }
        this.refGame.showText(this.lang[this.currentStep]);
    }

    /**
     * Cache tous les éléments présents sur l'interface
     */
    hideAll() {
        let elements = this.extractElements(true);
        for (let elementName of Object.keys(elements))
            if (elements[elementName] instanceof Button)
                this.setButtonVisible(elements[elementName], false);
            else if (elements[elementName] instanceof ToggleButton || elements[elementName] instanceof ToggleGroup)
                elements[elementName].hide();
            else if (elements[elementName] instanceof CheckBox)
                elements[elementName].setVisible(false);
            else
                elements[elementName].visible = false;
    }


    /**
     * Affichage / Désaffichage d'un bouton.
     * @param {Button} button Bouton à modifier
     * @param {boolean} visible Le bouton doit-il être visible
     */
    setButtonVisible(button, visible) {
        for (let pixiElement of button.getPixiChildren()) {
            pixiElement.visible = visible;
        }
    }

    /**
     * Choix de l'étape précédante en fonction de l'étape actuelle;
     */
    chooseBackStep() {
        switch (this.currentStep) {
            case 6:
                if (this.exercice.currentQuestion.type === 'draw') {
                    this.currentStep = 4;
                    this.updateShapeSelection(false);
                } else if (this.exercice.currentQuestion.qcmtype === 'radio') {
                    this.currentStep = 5;
                    this.updateShapeSelection(true);
                } else if (this.exercice.currentQuestion.qcmtype === 'checkbox') {
                    this.currentStep = 4;
                    this.updateShapeSelection(true);
                }
                break;
            case 4:
                if (this.exercice.currentQuestion.type === 'draw')
                    this.currentStep = 3;
                else if (this.exercice.currentQuestion.qcmtype === 'radio')
                    this.currentStep = 3;
                else if (this.exercice.currentQuestion.qcmtype === 'checkbox') {
                    if (this.exercice.currentQuestion.defaultShapeChoosed) {
                        this.currentStep = 4;
                        this.steps.specific[4].subtitle4.text = this.getText('step4Subtitle');
                    } else
                        this.currentStep = 3;
                }
                this.exercice.currentQuestion.defaultShapeChoosed = false;
                this.updateShapeSelection(false);
                break;
            case 5:
                this.updateShapeSelection(false);
                this.exercice.currentQuestion.defaultShapeChoosed = false;
                this.currentStep--;
                break;
            default :
                this.currentStep = this.currentStep > 0 ? this.currentStep - 1 : 0;
                break;
        }
        this.loadForm();
    }

    /**
     * Choix de l'étape suivante en fonction de l'étape actuelle;
     */
    chooseNextStep() {
        switch (this.currentStep) {
            case 1:
                if (this.steps.specific[1].french.text != null &&
                    this.steps.specific[1].french.text !== '' &&
                    this.steps.specific[1].german.text != null &&
                    this.steps.specific[1].german.text !== '') {
                    this.currentStep++;
                    this.exercice.name.fr = this.steps.specific[1].french.text;
                    this.exercice.name.de = this.steps.specific[1].german.text;
                    this.exercice.name.en = this.steps.specific[1].english.text;
                    this.exercice.name.it = this.steps.specific[1].italian.text;
                    this.exercice.createQuestion();
                } else
                    Util.getInstance().showAlert('warning', this.alert.noNameText, this.alert.noName);
                break;
            case 2:
                if (this.steps.specific[2].questionType.getActives().length > 0) {
                    this.currentStep = this.steps.specific[2].questionType.getActives()[0].data.nextStep;
                    if (this.exercice.currentQuestion.type !== this.steps.specific[2].questionType.getActives()[0].data.type
                        || this.exercice.currentQuestion.qcmtype !== this.steps.specific[2].questionType.getActives()[0].data.qcmtype)
                        this.exercice.currentQuestion.trueResponses = [];
                    this.exercice.currentQuestion.type = this.steps.specific[2].questionType.getActives()[0].data.type;
                    this.exercice.currentQuestion.qcmtype = this.steps.specific[2].questionType.getActives()[0].data.qcmtype;
                } else
                    Util.getInstance().showAlert('warning', this.alert.noTypeText, this.alert.noType);
                break;
            case 3:
                if (this.steps.specific[3].frenchQuestionName.text != null &&
                    this.steps.specific[3].frenchQuestionName.text !== '' &&
                    this.steps.specific[3].germanQuestionName.text != null &&
                    this.steps.specific[3].germanQuestionName.text !== '') {
                    this.currentStep++;
                } else
                    Util.getInstance().showAlert('warning', this.alert.noNameText, this.alert.noName);

                this.exercice.currentQuestion.label.fr = this.steps.specific[3].frenchQuestionName.text;
                this.exercice.currentQuestion.label.de = this.steps.specific[3].germanQuestionName.text;
                this.exercice.currentQuestion.label.en = this.steps.specific[3].englishQuestionName.text;
                this.exercice.currentQuestion.label.it = this.steps.specific[3].italianQuestionName.text;
                if (this.exercice.currentQuestion.type === 'draw')
                    this.steps.specific[4].subtitle4.text = this.getText('step4Subtitle1');
                this.steps.specific[4].multipleChoice.reset();
                break;
            case 4:
                if (this.exercice.currentQuestion.type === 'draw') {
                    this.exercice.currentQuestion.defaultShapeChoosed = true;
                    this.currentStep = 6;
                } else if (this.exercice.currentQuestion.qcmtype === 'radio')
                    this.currentStep = 5;
                else if (this.exercice.currentQuestion.qcmtype === 'checkbox') {
                    if (this.exercice.currentQuestion.defaultShapeChoosed) {
                        this.currentStep = 6;
                    } else {
                        this.currentStep = 4;
                        this.steps.specific[4].subtitle4.text = this.getText('step4Subtitle2');
                    }
                }
                this.saveShapes(this.steps.specific[4].multipleChoice, this.exercice.currentQuestion.defaultShapeChoosed);
                this.exercice.currentQuestion.defaultShapeChoosed = true;
                this.steps.specific[4].multipleChoice.reset();
                this.updateShapeSelection(true);
                break;
            case 5:
                this.currentStep++;
                this.saveShapes(this.steps.specific[5].singleChoice, this.exercice.currentQuestion.defaultShapeChoosed);
                break;
            case 6:
                this.currentStep = 2;
                this.exercice.endQuestion();
                this.updateShapeSelection(false);
                this.exercice.createQuestion();
                break;
            case 7:
                if (this.steps.specific[7].isNew.isChecked() && this.steps.specific[7].understandable.isChecked())
                    this.currentStep++;
                else
                    Util.getInstance().showAlert('warning', this.alert.checkConditionText, this.alert.checkCondition);
                break;
            default :
                this.currentStep = this.currentStep < 10 ? this.currentStep + 1 : 10;
                break;
        }
        this.loadForm();
        this.prepareResume();
    }

    /**
     * Sauvegarde les formes choisies dans le ToggleGroup dans l'exercice en cours
     * @param {ToggleGroup} toggleGroup Le sélecteur de formes.
     * @param {boolean} isTrueShapes Est-ce que les formes sont les formes correctes.
     */
    saveShapes(toggleGroup, isTrueShapes) {
        let activesShapes = toggleGroup.getActives();
        if (isTrueShapes)
            this.exercice.currentQuestion.trueResponses = [];
        else
            this.exercice.currentQuestion.allResponses = [];
        for (let shape of activesShapes) {
            this.exercice.addResponses(shape.data, isTrueShapes);
        }
    }

    /**
     * Chargement des données de l'exercice en cours dans l'IHM.
     */
    loadForm() {
        if (this.exercice.currentQuestion != null) {
            this.steps.specific[2].questionType.reset();
            if (this.exercice.currentQuestion.type === 'draw')
                this.steps.specific[2].questionType.buttons[2].toggle();
            else if (this.exercice.currentQuestion.type === 'qcm' && this.exercice.currentQuestion.qcmtype === 'radio')
                this.steps.specific[2].questionType.buttons[0].toggle();
            else if (this.exercice.currentQuestion.type === 'qcm' && this.exercice.currentQuestion.qcmtype === 'checkbox')
                this.steps.specific[2].questionType.buttons[1].toggle();


            this.steps.specific[3].frenchQuestionName.text = this.exercice.currentQuestion.label.fr;
            this.steps.specific[3].englishQuestionName.text = this.exercice.currentQuestion.label.en;
            this.steps.specific[3].germanQuestionName.text = this.exercice.currentQuestion.label.de;
            this.steps.specific[3].italianQuestionName.text = this.exercice.currentQuestion.label.it;

            if (this.exercice.currentQuestion.defaultShapeChoosed)
                for (let e of this.exercice.currentQuestion.trueResponses) {
                    for (let e2 of this.steps.specific[4].multipleChoice.buttons)
                        if (e2.data === e.value && e2.isEnabled() && !e2.isActive())
                            e2.toggle();
                    for (let e2 of this.steps.specific[5].singleChoice.buttons)
                        if (e2.data === e.value && e2.isEnabled() && !e2.isActive())
                            e2.toggle();
                }
            else
                for (let e of this.exercice.currentQuestion.allResponses) {
                    for (let e2 of this.steps.specific[4].multipleChoice.buttons)
                        if (e2.data === e.value && e2.isEnabled() && !e2.isActive())
                            e2.toggle();
                    for (let e2 of this.steps.specific[5].singleChoice.buttons)
                        if (e2.data === e.value && e2.isEnabled() && !e2.isActive())
                            e2.toggle();
                }
        }
    }

    /**
     * Mise à jour de la sélection des formes et des formes actives.
     * @param {boolean} disableButton Faut-il désactiver les formes en cours
     */
    updateShapeSelection(disableButton) {
        for (let btn of this.steps.specific[4].multipleChoice.buttons)
            btn.enable();
        for (let btn of this.steps.specific[5].singleChoice.buttons)
            btn.enable();
        if (disableButton) {
            let active = [];
            for (let shape of this.exercice.currentQuestion.allResponses)
                active.push(shape.value);
            for (let btn of this.steps.specific[4].multipleChoice.buttons)
                if (active.indexOf(btn.data) < 0)
                    btn.disable();
            for (let btn of this.steps.specific[5].singleChoice.buttons)
                if (active.indexOf(btn.data) < 0)
                    btn.disable();
        }

    }

    /**
     * Prépare l'interface du résumé de la question dynamiquement.
     */
    prepareResume() {
        if (this.exercice.currentQuestion == null)
            return;
        let currentLanguage = this.refGame.global.resources.getLanguage();

        this.steps.specific[6].questionLabel.text = this.exercice.currentQuestion.label[currentLanguage];
        this.steps.specific[6].questionLabel.x = 300;
        this.steps.specific[6].questionLabel.y = 120 + Math.floor(this.steps.specific[6].questionLabel.height / 2);

        let headerSize = HEADER_SIZE + 20 + this.steps.specific[6].questionLabel.height + 20;

        this.steps.specific[6].allShapeLabel.x = 155;
        this.steps.specific[6].allShapeLabel.y = headerSize;

        this.steps.specific[6].trueShapeLabel.x = 445;
        this.steps.specific[6].trueShapeLabel.y = headerSize;

        headerSize += this.steps.specific[6].trueShapeLabel.height + 15;

        let shapes = '';
        for (let resp of this.exercice.currentQuestion.allResponses) {
            shapes += this.refGame.shapeUtils.getNames(resp.value)[currentLanguage] + '\n';
        }

        this.steps.specific[6].allShape.text = shapes;


        shapes = '';
        for (let resp of this.exercice.currentQuestion.trueResponses) {
            shapes += this.refGame.shapeUtils.getNames(resp.value)[currentLanguage] + '\n';
        }
        this.steps.specific[6].trueShape.text = shapes;


        this.steps.specific[6].trueShape.x = 445;
        this.steps.specific[6].trueShape.y = headerSize + Math.floor(this.steps.specific[6].trueShape.height / 2);

        this.steps.specific[6].allShape.x = 155;
        this.steps.specific[6].allShape.y = headerSize + Math.floor(this.steps.specific[6].allShape.height / 2);

    }

    /**
     * Supprime l'exercice en cours
     */
    deleteExercice() {
        this.currentStep = 0;
        this.generateGame();
        this.show();
        Util.getInstance().showAlert('success', this.alert.confirmDeletionText, this.alert.confirmDeletion);
    }

    /**
     * Enregistrement de l'exercice dans la base de données (appel aux services REST)
     */
    submitExercice() {
        this.refGame.global.resources.saveExercice(this.exercice.toString(), function (result) {
            this.currentStep = 0;
            this.generateGame();
            this.show();
            Util.getInstance().showAlert(result.type, null, result.message);
        }.bind(this));
    }

    /**
     * Début du mode de test de l'exercice en cours
     */
    testExercice() {
        this.hideAll();
        this.refTrainMode.init(false, true, this.exercice.toString(), this);
        this.refTrainMode.show();
    }

    /**
     * Méthode de mise à jour de la police d'écriture
     * @param isOpendyslexic Police spécial pour les dyslexiques
     */
    updateFont(isOpendyslexic) {
        let font = isOpendyslexic ? 'Opendyslexic' : 'Arial';
        let elements = this.extractElements();
        for (let element of Object.keys(elements)) {
            if (elements[element] instanceof Button
                || elements[element] instanceof ToggleButton
                || elements[element] instanceof ToggleGroup
                || elements[element] instanceof CheckBox)
                elements[element].updateFont(font);
            else if (elements[element] instanceof PIXI.Text)
                elements[element].style.fontFamily = font;

        }
    }

     /**
     * Retourne la liste des formes pour le dégrès courrant
     * @return {Array} Les formes disponibles pour le degrès.
     */
    getShapeForCurrentDegree() {
        let degree = this.refGame.global.resources.getDegre();
        if (degree == this.shapes.lastDegree)
            return this.shapes.shapes;
        let shapes = [];
        for (let shape of this.refGame.shapeUtils.getShapes()) {
            if (shape.harmos <= degree)
                shapes.push(shape);
        }
        this.shapes.shapes = shapes;
        this.shapes.lastDegree = degree;
        return shapes;
    }

}

/**
 * @classdesc IHM de dessin
 * @author Vincent Audergon
 * @version 1.0
 */
class Draw extends Interface {

    /**
     * Constructeur de l'ihm de dessin
     * @param {Game} refGame la référence vers la classe de jeu
     * @param {string} [scene="draw"] le nom de la scène utilisée par l'ihm, "draw" par défaut (facultatif)
     */
    constructor(refGame, scene = "draw") {
        super(refGame, scene);
        super.setElements({
            grid: new DrawingGrid(9, 9, 50, this.scene, function (shape) { this.onShapeCreated(shape) }.bind(this)),
            finger: PIXI.Sprite.from(refGame.global.resources.getImage('finger').getImage().src)
        });
        this.elements.grid.setOnLineDrawn(function () { this.elements.finger.visible = false; }.bind(this));
        this.refGame.global.pixiApp.ticker.add(function () {
            this.elements.finger.x += 2;
            this.elements.finger.alpha -= 0.01;
            if (this.elements.finger.x > 460) {
                this.elements.finger.x = 160;
                this.elements.finger.alpha = 1;
            }
        }.bind(this));
        /** @type {function} fonction de callback appelée lorsqu'une forme est dessinée */
        this.onShapeCreated = undefined;
    }

    /**
     * Affiche l'ihm de dessin. Réinitialise la grille de dessin.
     */
    show() {
        this.clear();
        this.elements.grid.reset();
        this.elements.finger.visible = true;
        this.elements.finger.alpha = 1;
        this.elements.finger.x = 160;
        this.elements.finger.y = 190;
        this.init();
    }

    /**
     * Défini la fonction de callback appelée lorsqu'une forme est créée avec la grille de dessin
     * @param {function} onShapeCreated La fonction de callback
     */
    setOnShapeCreated(onShapeCreated) {
        this.onShapeCreated = onShapeCreated;
    }

}
/**
 * @classdesc IHM Page de garde d'un exercice
 * @author Vincent Audergon
 * @version 1.0
 */
class Exercice extends Interface {

    /**
     * Constructeur de l'ihm page de garde
     * @param {Game} refGame la référence vers la classe de jeu
     */
    constructor(refGame) {
        super(refGame, "exercice");
        super.setElements({
            title: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 32, fill: 0x000000, align: 'center'}),
            subTitle: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 27, fill: 0x000000, align: 'center'}),
            begin: new Button(300, 300, '', 0x00AA00, 0xFFFFFF),
            info: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 16, fill: 0x00, align: 'left'})
        });
        /** @type {function} fonction de callback appelée au moment du click sur le bouton */
        this.onBegin = function () {
            console.log('Replace this action with exercice.setOnBegin')
        };
        /** @type {boolean} indique si c'est une évaluation ou un exercice */
        this.evaluation = false;
        this.names = null;
    }

    /**
     * Affiche la page de garde
     * @param {string} harmos le niveau harmos
     * @param {boolean} evaluation si il s'agit d'un exercice ou d'une evaluation
     */
    show(harmos, evaluation, names = null) {
        this.clear();

        this.elements.info.x = 0;
        this.elements.info.y = 15;
        this.elements.info.visible = false;
        this.names = names;
        this.evaluation = evaluation;
        this.elements.begin.setOnClick(function () {
            this.onBegin()
        }.bind(this));
        this.elements.title.text = harmos;
        this.elements.title.anchor.set(0.5);
        this.elements.title.x = 300;
        this.elements.title.y = 100;
        this.elements.subTitle.anchor.set(0.5);
        this.elements.subTitle.x = 300;
        this.elements.subTitle.y = 150;
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
    }

    /**
     * Fonction de callback appelée lorsque la langue a été changée.
     * Mets à jour les textes des composants de l'ihm
     * @param {string} lang la nouvelle langue
     */
    refreshLang(lang) {
        this.elements.begin.setText(this.getText('begin'));
        this.elements.subTitle.text = (this.names != null && this.names[lang] != null && this.names[lang] !== '') ? this.names[lang] : (this.evaluation ? this.getText('evaluation') : this.getText('exercice'));

        this.elements.info.text = this.refGame.global.resources.getOtherText('info', {
            mode: (this.evaluation ? this.getText('ModeEvaluer') : this.getText('ModeEntrainer')),
            harmos: this.refGame.global.resources.getDegre(),
            lang: this.refGame.global.resources.getLanguage()
        });
    }

    /**
     * Défini la fonction de callback appelée lorsque le joueur clique sur "commencer".
     * @param {function} onBegin la fonction de callback
     */
    setOnBegin(onBegin) {
        this.onBegin = onBegin;
    }

}
/**
 * @classdesc IHM Exploration
 * @author Vincent Audergon
 * @version 1.0
 */
class Explore extends Interface {

    /**
     * Constructeur de l'IHM
     * @param {Game} refGame la référence vers la classe de jeu
     */
    constructor(refGame) {
        super(refGame, "explore");
        this.refGame = refGame;
        this.setElements({
            info: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 16, fill: 0x00, align: 'left'})
        });
        /** @type {function} fonction de callback appelée lorsqu'un click est effectué sur une forme */
        this.onClick = function () { console.log('Replace this action with explore.setOnClick') }
        this.poly = 0;
    }

    /**
     * Affiche l'ihm et les différentes formes qui doivent y figurer
     * @param {Shape[]} shapes la liste des formes à afficher dans l'ihm
     */
    show(shapes) {
        this.clear();
        this.elements.info.x = 0;
        this.elements.info.y = 15;
        this.elements.info.visible = false;
        let tailleGrille = shapes.length;
        let trueShapes = JSON.parse(this.refGame.global.resources.getScenario());
        let dbShapes = trueShapes.shapes;
        let width = Math.sqrt(tailleGrille);
        if (!Number.isInteger(width)) width = Math.floor(width) + 1;

        let harmos = this.refGame.global.resources.getDegre();

        for (let s in shapes) {
            let shape = shapes[s];
            if (!shape.id.includes("_") && dbShapes[s].Harmos <= harmos) {
                let graphics = shape.initGraphics(500 / width, 500 / width);
                graphics.x = (s * 500 / width) % 500 + 500 / width / 2;
                graphics.y = (Math.floor(s / width)) * (500 / width) + 500 / width / 2 + 40;
                this.elements[`shape${s}`] = graphics;
                graphics.on('pointertap', function () {
                    this.onClick(shape);
                }.bind(this));
                this.poly++;
            } else {
                let graphics = shape.initGraphics(1 / width, 1 / width);
                graphics.x = (s * 1 / width) % 1 + 1 / width / 2;
                graphics.y = (Math.floor(s / width)) * (1 / width) + 1 / width / 2;
                this.elements[`shape${s}`] = graphics;
                graphics.on('pointertap', function () {
                    this.onClick(shape);
                }.bind(this));
            }
        }
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
    }

    refreshLang(lang) {
        this.elements.info.text = this.refGame.global.resources.getOtherText('info', {
            mode: this.getText('ModeExplorer'),
            harmos: this.refGame.global.resources.getDegre(),
            lang: lang
        });
    }

    /**
     * Défini la fonction de callback appelée lorsqu'un clic est effectué sur un des polygones
     * @param {function} onClick la fonction de callback
     */
    setOnClick(onClick) {
        this.onClick = onClick;
    }

}
/**
 * @classdesc IHM de la création d'exercice
 * @author Théo Teixeira
 * @version 1.0
 */
class Play extends Interface {

    /**
     * Constructeur de l'ihm de la création d'exercice
     * @param {Game} refGame la référence vers la classe de jeu
     * @param {TrainMode} refTrainMode la référence au mode exercer pour tester les jeux
     */
    constructor(refGame, refTrainMode) {

        super(refGame, "play");

        this.exercices = {};

        this.selectedGameId = -1;

        this.refTrainMode = refTrainMode;

        this.elements = {
            title: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 16, fill: 0x000000, align: 'left'}),
            scrollPane: new ScrollPane(0, 60, 600, 480),
            btn: new Button(300,570,'',0x0088FF,0x000000, true)
        };
        this.elements.title.anchor.set(0.5);
        this.elements.title.x = 300;
        this.elements.title.y = 30;

        this.elements.btn.setOnClick(function(){
            if(this.selectedGameId < 0)
                return;
            this.playExerice(this.selectedGameId);
        }.bind(this));
    }

    init() {
        super.init();
    }

    /**
     * Point d'entrée pour l'affichage
     */
    show() {
        this.clear();

        this.exercices = this.refGame.global.resources.getExercicesEleves();
        this.exercices = JSON.parse(this.exercices);


        this.refreshLang(this.refGame.global.resources.getLanguage());

        this.setElements(this.elements);

        this.init();
    }

    /**
     * Méthode permettant de mettre à jour tous les composants graphiques lors du changement de langue.
     * @param {string} lang Langue à afficher
     */
    refreshLang(lang) {
        this.generateScrollPane(lang);
        this.refGame.showText(this.getText("playTitle"));
        this.elements.btn.setText(this.getText('playButton'));
    }


    playExerice(exId) {
        this.refTrainMode.init(false, true, JSON.stringify(this.exercices[exId].exercice), this);
        this.refTrainMode.show();
    }

    /**
     * Méthode de mise à jour de la police d'écriture
     * @param isOpendyslexic Police spécial pour les dyslexiques
     */
    updateFont(isOpendyslexic) {
        let font = isOpendyslexic ? 'Opendyslexic' : 'Arial';

    }

    generateScrollPane(lang) {
        this.elements.scrollPane.clear();
        let tg = new ToggleGroup('single');
        for (let exID of Object.keys(this.exercices)) {
            let ex = this.exercices[exID];
            let label = (ex.exercice.name[lang] !== '' || ex.exercice.name[lang] !== null ? ex.exercice.name[lang] : ex.exercice.name['fr']) + ex.name;
            let btn = new ToggleButton(0,0,label,false,580);
            btn.enable();
//            btn = new Button(0, 0, label, 0x00AA00, 0xFFFFFF, false, 580);

            tg.addButton(btn);

            btn.setOnClick(function () {
                this.selectedGameId = exID;
            }.bind(this));
            this.elements.scrollPane.addElements(btn);
        }
        this.elements.scrollPane.init();
    }
}

/** Constante définissant le mode radio du QCM */
const QCM_TYPE_RADIO = "radio";
/** Constante définissant le mode choix multiples du QCM */
const QCM_TYPE_CHECK = "checkbox";

/**
 * @classdesc IHM Questions choix multiples
 * @author Vincent Audergon
 * @version 1.0
 */
class QCM extends Interface {

    /**
     * Constructeur de l'ihm QCM
     * @param {Game} refGame la référence vers la classe de jeu
     */
    constructor(refGame) {
        super(refGame, "qcm");
        super.setElements({
            validateButton: new Button(300, 550, '', 0x00AA00, 0xFFFFFF,true)
        });
        /** @type {function} fonction de callback appelée lorsque la réponse est validée */
        this.onResponse = undefined;
        /** @type {string[]} la liste des réponses */
        this.responses = [];
        /** @type {string} le type de QCM */
        this.qcmtype;
    }

    /**
     * Affiche l'ihm ainsi que les réponses possibles de la question
     * @param {Question} question la question à afficher
     */
    show(question) {
        for (let el in this.elements){
            if(el.startsWith('shape'))
                this.elements[el].visible = false;
        }
        this.clear();
        this.responses = [];
        this.qcmtype = question.qcmtype;
        let shapes = [];
        for (let id of question.responses) {
            shapes.push(this.refGame.shapeUtils.getShapeById(id.value));
        }
        for (let id of question.traps) {
            shapes.push(this.refGame.shapeUtils.getShapeById(id.value));
        }
        shapes = this.shuffle(shapes);
        let gridHeight = 500 / shapes.length;
        for (let s in shapes) {
            let shape = shapes[s];
            let element = this.elements[`shape${s}`] = shape.initGraphics(600, gridHeight);
            element.x = 600 / 2;
            element.y = gridHeight * s + gridHeight / 2;
            element.value = shape.id;
            element.interactive = true;
            element.alpha = 0.5;
            element.on('pointerdown', function () {
                this.onClick(element);
            }.bind(this));
            this.responses.push(element);
        }
        this.elements.validateButton.setOnClick(function(){
            this.onResponse(this.getSelectedResponses());
        }.bind(this));
        this.refreshLang();
        this.init();
    }

    /**
     * Mélange un tableau aléatoirement
     * @param {Array} a le tableau à mélanger
     */
    shuffle(a) {
        let j, x, i;
        for (i = a.length - 1; i > 0; i--) {
            j = Math.floor(Math.random() * (i + 1));
            x = a[i];
            a[i] = a[j];
            a[j] = x;
        }
        return a;
    }

    /**
     * Retournes les résponses que l'utilisateur a séléctionné
     * @return {string[]} les réponses
     */
    getSelectedResponses(){
        let values = [];
        for (let response of this.responses){
            if (response.selected) values.push(response.value);
        }
        return values;
    }

    /**
     * Fonction de callback appelée lorsqu'un clic est effectué sur une réponse
     * @param {PIXI.Sprite} element l'élément sur lequel on a cliqué
     */
    onClick(element) {
        if (this.qcmtype === QCM_TYPE_CHECK){
            element.alpha = element.alpha === 1 ? 0.5 : 1;
            element.selected = element.alpha === 1;
        }else if (this.qcmtype === QCM_TYPE_RADIO) {
            for (let el of this.responses){
                el.alpha = 0.5,
                    el.selected = false;
            }
            element.alpha = 1;
            element.selected = true;
        }
    }

    /**
     * Fonction de callback appelée lorsque la langue a été changée.
     * Mets à jour le texte du bouton "valider"
     * @param {string} lang la nouvelle langue
     */
    refreshLang(lang){
        this.elements.validateButton.setText(this.getText('validate'));
    }

    /**
     * Défini la fonction de callback appele lorsque l'utilisateur valide sa réponse
     * @param {function} onResponse la fonction de callback
     */
    setOnResponse(onResponse) {
        this.onResponse = onResponse;
    }

}
/**
 * @classdesc IHM d'informations sur un polygone
 * @author Vincent Audergon
 * @version 1.0
 */
class ShapeInfo extends Interface {

    /**
     * Constructeur de l'ihm d'informations
     * @param {Game} refGame la référence vers la classe de jeu
     */
    constructor(refGame) {
        super(refGame, "shapeInfo");
        super.setElements({
            polygon: new PIXI.Graphics(),
            background: new PIXI.Graphics(),
            lblAngles: new PIXI.Text('Angles', {fontFamily: 'Arial', fontSize: 20, fill: 0x00, align: 'left'}),
            lblParallels: new PIXI.Text('Paralleles', {fontFamily: 'Arial', fontSize: 20, fill: 0x00, align: 'left'}),
            lblSym: new PIXI.Text('Axes de symmétrie', {fontFamily: 'Arial', fontSize: 20, fill: 0x00, align: 'left'}),
            lblCom: new PIXI.Text('||Insert comment here||', {
                fontFamily: 'Arial',
                fontSize: 14,
                fill: 0x00,
                align: 'center'
            }),

            lblBiss: new PIXI.Text('Test', {fontFamily: 'Arial', fontSize: 20, fill: 0x00, align: 'left'}),
            lblSideBiss: new PIXI.Text('Test', {fontFamily: 'Arial', fontSize: 20, fill: 0x00, align: 'left'}),
            lblMed: new PIXI.Text('Test', {fontFamily: 'Arial', fontSize: 20, fill: 0x00, align: 'left'}),

            btnLignes: new Button(100, 240, '', 0xd62f2f, 0xFFFFFF),
            btnBiss: new Button(100, 280, '', 0x33cc33, 0xFFFFFF),
            btnMediane: new Button(100, 320, '', 0x0099ff, 0xFFFFFF),
            btnMediatrice: new Button(100, 360, '', 0xcc00ff, 0xFFFFFF),
            btnAxeSym: new Button(100, 420, '', 0xbf4080, 0xFFFFFF),
            btnGravity: new Button(100, 470, 'Gravi', 0xfaae0a, 0xFFFFFF),

            lblCheck: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 16, fill: 0x000000, align: 'left'}),
            lblRadio: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 16, fill: 0x000000, align: 'left'}),
            lblBtnDraw: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 16, fill: 0x000000, align: 'left'}),

            btnClose: new Button(300, 550, '', 0xd62f2f, 0xFFFFFF),
            title: new PIXI.Text('Test', {fontFamily: 'Arial', fontSize: 30, fill: 0x101010, align: 'center'}),
        });
        this.refGame.global.pixiApp.ticker.add(function () {
            // this.elements.polygon.rotation += 1;
        }.bind(this));
        /** @type {function} fonction de callback appelée lorsqu'un click est effectué sur le bonton "fermer" */
        this.close = undefined;

        /** @type {function} fonction de callback appelée lorsqu'un click est effectué sur le bonton "btnLignes" */
        this.btnLignes = function () {
            console.log('Replace this action with ShapeInfo.setBtnlignes')
        };
        /** @type {function} fonction de callback appelée lorsqu'un click est effectué sur le bonton "btnBiss" */
        this.btnBiss = function () {
            console.log('Replace this action with ShapeInfo.setBtnBissectrice')
        };
        /** @type {function} fonction de callback appelée lorsqu'un click est effectué sur le bonton "btnMediane" */
        this.btnMediane = function () {
            console.log('Replace this action with ShapeInfo.setBtnMediane')
        };
        /** @type {function} fonction de callback appelée lorsqu'un click est effectué sur le bonton "btnMediatrice" */
        this.btnMediatrice = function () {
            console.log('Replace this action with ShapeInfo.setBtnMediatrice')
        };
        /** @type {function} fonction de callback appelée lorsqu'un click est effectué sur le bonton "btnAxeSym" */
        this.btnAxeSym = function () {
            console.log('Replace this action with ShapeInfo.setBtnAxeSym')
        };

        /** @type {Shape} la forme affichée */
        this.shape = undefined;
        this.shapeSecondaire = undefined;

        this.isAll = false;
        this.maxAxeSym = 0;
        this.maxSideBiss = 0;
        this.maxBiss = 0;
        this.maxMed = 0;
        this.gravity = false;

        this.isDraw = false;
        this.secShape = undefined;

        this.nAxeSym = 0;
        this.nSideBiss = 0;
        this.nBiss = 0;
        this.nMediane = 0;
        this.nDiagonal = 0;
        this.gravity = false;

        this.allText = "axesTous";
        this.axeSymText = "axeSym";
        this.medianeText = "medianes";
        this.bissText = "bissectrices";
        this.commentText = "commentDefault";
        this.mediatriceText = "mediatrices";
        this.centreGraviteText = "centreGravite";
    }

    /**
     * Affiche l'ihm d'informations sur le polygone donné.
     * @param {Shape} shape Le polygone dont on affiche les informations
     * @param {PIXI.Sprite} graphics Le sprite du polygone
     * @param {PIXI.Sprite} graphicsSec forme secondaire affichée à l'utilisateur pour exemple.
     */
    show(shape, shapeSec, graphicsSec, graphics, draw) {

        this.maxAxeSym = shape.axeSym.length;
        this.maxSideBiss = shape.sideBisections.length;
        this.maxBiss = shape.bisections.length;
        this.maxMed = shape.mediane.length;
        this.maxDiagonal = shape.diagonal.length;

        if (this.maxAxeSym !== 0) {
            this.axeSymText = "axeSym";
        } else {
            this.axeSymText = "noAxe";
        }

        if (this.maxBiss === 0) this.bissText = "noAxe";

        if (this.maxMed === 0 && this.maxDiagonal === 0)
            this.medianeText = "noAxe";
        else if (this.maxMed !== 0 && this.maxDiagonal === 0)
            this.medianeText = "medianes";
        else if (this.maxMed === 0 && this.maxDiagonal !== 0)
            this.medianeText = "diagonal";


        if (this.maxSideBiss === 0) this.mediatriceText = "noAxe";

        if (this.maxAxeSym === 0 && this.maxBiss === 0 &&
            this.maxMed === 0 && this.maxSideBiss === 0) this.allText = "axesRien";

        if (typeof shape.gravity[0] === 'undefined') this.centreGraviteText = "noAxe";

        this.isDraw = draw;
        this.secShape = shapeSec;
        this.clear();
        this.shape = shape;
        this.shapeSecondaire = graphicsSec;
        this.shapeSecondaire.visible = draw;
        this.elements.background.beginFill(0xe6eeff);
        this.elements.background.lineStyle(4, 0x4d88ff, 1);
        this.elements.background.drawRect(0, 0, 600, 200);
        this.elements.background.endFill();
        this.elements.polygon = graphics;
        this.elements.polygon.x = 375;
        this.elements.polygon.y = 375;
        this.elements.polygon.interactive = false;
        this.elements.polygon2 = graphicsSec;
        this.elements.polygon2.x = 550;
        this.elements.polygon2.y = 400;
        this.elements.polygon2.interactive = false;
        this.elements.title.anchor.set(0.5);
        this.elements.title.x = 300;
        this.elements.title.y = 20;
        this.elements.btnLignes.setOnClick(function () {
            if (this.isAll) {
                this.gravity = false;
                this.nSideBiss = 0;
                this.nMediane = 0;
                this.nAxeSym = 0;
                this.nBiss = 0;
                this.nDiagonal = 0;
                this.allText = "axesTous";
                this.commentText = "commentDefault";
                this.isAll = false;
                this.refreshView();
            } else {
                this.gravity = true;
                this.nSideBiss = -1;
                this.nAxeSym = -1;
                this.nBiss = -1;
                this.nMediane = -1;
                this.nDiagonal = -1
                this.allText = "axesRien";
                this.commentText = "commentDefault";
                this.isAll = true;
                this.refreshView();
            }
        }.bind(this));
        this.elements.btnClose.setOnClick(function () {
            this.isAll = false;
            this.gravity = false;
            this.maxMed = 0;
            this.maxBiss = 0;
            this.maxAxeSym = 0;
            this.maxSideBiss = 0;
            this.nSideBiss = 0;
            this.nMediane = 0;
            this.nAxeSym = 0;
            this.nBiss = 0;
            this.secShape = undefined;

            this.allText = "axesRien";
            this.axeSymText = "axeSym";
            this.medianeText = "medianes";
            this.bissText = "bissectrices";
            this.commentText = "commentDefault";
            this.mediatriceText = "mediatrices";
            this.centreGraviteText = "centreGravite";

            this.elements.btnBiss.setText("");
            this.elements.btnLignes.setText("");
            this.elements.btnAxeSym.setText("");
            this.elements.btnMediane.setText("");
            this.elements.btnMediatrice.setText("");
            this.close();
        }.bind(this));
        this.elements.btnBiss.setOnClick(function () {
            if (this.maxBiss !== 0) {
                if (this.nBiss === this.maxBiss) {
                    this.nBiss = -1;
                    this.commentText = "commentBissectrice";
                    this.refreshView();
                } else {
                    this.commentText = "commentBissectrice";
                    this.nBiss++;
                    this.refreshView();
                }
            } else {
                this.bissText = "noAxe";
            }

        }.bind(this));
        this.elements.btnMediane.setOnClick(function () {
            if (this.maxMed !== 0) {
                if (this.nMediane === this.maxMed) {
                    this.nMediane = -1;
                    if (shape.sides.count === 3) {
                        this.commentText = "commentMedianeTriangle";
                    } else {
                        this.commentText = "commentMedianeQuad";
                    }
                    this.refreshView();
                } else {
                    this.nMediane++;
                    if (shape.sides.count === 3) {
                        this.commentText = "commentMedianeTriangle";
                    } else {
                        this.commentText = "commentMedianeQuad";
                    }
                    this.refreshView();
                }
            } else if (this.maxDiagonal !== 0) {
                if (this.nDiagonal === this.maxDiagonal) {
                    this.nDiagonal = -1;
                    this.commentText = "commentDiagonalQuad";
                    this.refreshView();
                } else {
                    this.nDiagonal++;
                    this.commentText = "commentDiagonalQuad";
                    this.refreshView();
                }
            } else {
                this.medianeText = "noAxe";
            }
        }.bind(this));
        this.elements.btnMediatrice.setOnClick(function () {
            if (this.maxSideBiss !== 0) {
                if (this.nSideBiss === this.maxSideBiss) {
                    this.nSideBiss = -1;
                    this.commentText = "commentMediatrice";
                    this.refreshView();
                } else {
                    this.nSideBiss++;
                    this.commentText = "commentMediatrice";
                    this.refreshView();
                }
            } else {
                this.mediatriceText = "noAxe";
            }

        }.bind(this));
        this.elements.btnAxeSym.setOnClick(function () {
            if (this.maxAxeSym !== 0) {
                if (this.nAxeSym === this.maxAxeSym) {
                    this.nAxeSym = -1;
                    this.commentText = "commentAxeDeSymetrie";
                    this.refreshView();
                } else {
                    this.nAxeSym++;
                    this.commentText = "commentAxeDeSymetrie";
                    this.refreshView();
                }
            } else {
                this.axeSymText = "noAxe";
            }

        }.bind(this));
        this.elements.btnGravity.setOnClick(function () {
            if (typeof shape.gravity[0] !== 'undefined') {
                if (this.gravity) {
                    this.gravity = false;
                    this.commentText = "gravi";
                    this.refreshView();
                } else {
                    this.gravity = true;
                    this.commentText = "gravi";
                    this.refreshView();
                }
            } else {
                this.centreGraviteText = "noAxe";
                this.refreshView();
            }
        }.bind(this));
        this.elements.lblAngles.x = 10;
        this.elements.lblAngles.y = 36;
        this.elements.lblParallels.x = 10;
        this.elements.lblParallels.y = 65;
        this.elements.lblSym.x = 10;
        this.elements.lblSym.y = 90;
        this.elements.lblBiss.x = 10;
        this.elements.lblBiss.y = 115;
        this.elements.lblSideBiss.x = 10;
        this.elements.lblSideBiss.y = 140;
        this.elements.lblMed.x = 10;
        this.elements.lblMed.y = 165;
        this.elements.lblCom.anchor.set(0.5);
        this.elements.lblCom.x = 300;
        this.elements.lblCom.y = 510;
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
    }

    /**
     * Fonction de callback appelée lorsque la langue a été changée.
     * Mets à jour les textes des composants de l'ihm
     * @param {string} lang la nouvelle langue
     */
    refreshLang(lang) {
        if (this.shape) {
            if (this.shape.description) {
                //Override description
                this.elements.lblParallels.text = this.shape.description;
                //Setup mulitline mode
                this.elements.lblParallels.style.wordWrap = true;
                this.elements.lblParallels.style.wordWrapWidth = 550;

                this.elements.lblSym.text = "";
                this.elements.lblBiss.text = "";
                this.elements.lblSideBiss.text = "";
                this.elements.lblMed.text = "";
            } else {
                //clear mulitline mode
                this.elements.lblParallels.style.wordWrap = false;

                this.elements.lblSym.text = this.shape.axeSym.length === 0 ? this.getText('nosymAxis') : `${this.shape.axeSym.length} ${this.getText('symAxis')}`;
                this.elements.lblParallels.text = this.shape.nPaireCotePara === 0 ? this.getText('noparallel') : `${this.shape.nPaireCotePara} ${this.getText('parallels')}`;
                this.elements.lblBiss.text = this.shape.bisections.length === 0 ? this.getText('nobissectrice') : `${this.shape.bisections.length} ${this.getText('bissectrice')}`;
                this.elements.lblSideBiss.text = this.shape.sideBisections.length === 0 ? this.getText('nomediatrice') : `${this.shape.sideBisections.length} ${this.getText('mediatrice')}`;
                this.elements.lblMed.text = this.shape.mediane.length === 0 ? this.getText('nomediane') : `${this.shape.mediane.length} ${this.getText('mediane')}`;
            }
            if (this.shape) this.elements.title.text = this.shape.getName(lang);
            let droit = 0;
            let text = `${this.getText('angles')} :`;
            text += this.angleText();
            this.elements.lblAngles.text = this.shape.displayAngle ? text : '';

            this.elements.lblCom.text = this.getText(this.commentText);
            this.elements.btnLignes.setText(`${this.getText(this.allText)}`);

            if (this.bissText !== "noAxe") {
                if (this.nBiss !== -1) {
                    this.elements.btnBiss.setText(`${this.getText(this.bissText) + " " + this.nBiss + "/" + this.maxBiss}`);
                } else {
                    this.elements.btnBiss.setText(`${this.getText(this.bissText) + " " + this.getText("tout")}`);
                }
            } else {
                this.elements.btnBiss.setText(`${this.getText(this.bissText)}`);
            }

            if (this.axeSymText !== "noAxe") {
                if (this.nAxeSym !== -1) {
                    this.elements.btnAxeSym.setText(`${this.getText(this.axeSymText) + " " + this.nAxeSym + "/" + this.maxAxeSym}`);
                } else {
                    this.elements.btnAxeSym.setText(`${this.getText(this.axeSymText) + " " + this.getText("tout")}`);
                }
            } else {
                this.elements.btnAxeSym.setText(`${this.getText(this.axeSymText)}`);
            }

            if (this.medianeText !== "noAxe") {
                if (this.maxMed !== 0) {
                    if (this.nMediane !== -1) {
                        this.elements.btnMediane.setText(`${this.getText(this.medianeText) + " " + this.nMediane + "/" + this.maxMed}`);
                    } else {
                        this.elements.btnMediane.setText(`${this.getText(this.medianeText) + " " + this.getText("tout")}`);
                    }
                } else {
                    if (this.nDiagonal !== -1) {
                        this.elements.btnMediane.setText(`${this.getText(this.medianeText) + " " + this.nDiagonal + "/" + this.maxDiagonal}`);
                    } else {
                        this.elements.btnMediane.setText(`${this.getText(this.medianeText) + " " + this.getText("tout")}`);
                    }
                }
            } else {
                this.elements.btnMediane.setText(`${this.getText(this.medianeText)}`);
            }

            if (this.mediatriceText !== "noAxe") {
                if (this.nSideBiss !== -1) {
                    this.elements.btnMediatrice.setText(`${this.getText(this.mediatriceText) + " " + this.nSideBiss + "/" + this.maxSideBiss}`);
                } else {
                    this.elements.btnMediatrice.setText(`${this.getText(this.mediatriceText) + " " + this.getText("tout")}`);
                }
            } else {
                this.elements.btnMediatrice.setText(`${this.getText(this.mediatriceText)}`);
            }

            if (this.centreGraviteText !== "noAxe") {
                if (this.gravity) {
                    this.elements.btnGravity.setText(`${this.getText(this.centreGraviteText) + " ON"}`);
                } else {
                    this.elements.btnGravity.setText(`${this.getText(this.centreGraviteText) + " OFF"}`);
                }
            } else {
                this.elements.btnGravity.setText(`${this.getText(this.centreGraviteText)}`);
            }
        }
        this.elements.btnClose.setText(this.getText('close'));
    }

    refreshView() {
        if (this.isDraw) {
            this.refGame.explorationMode.displayShapeInfo(this.shape, this.isDraw, true, this.nBiss, this.nMediane, this.nSideBiss, this.nAxeSym, true, this.secShape, this.gravity);
        } else {
            this.refGame.explorationMode.displayShapeInfo(this.shape, this.isDraw, true, this.nBiss, this.nMediane, this.nSideBiss, this.nAxeSym, true, undefined, this.gravity, this.nDiagonal);
        }
    }

    /**
     * Défini la fonction de callback appelée lorsque l'utilisateur clique sur "fermer"
     * @param {function} onClose la fonction de callback
     */
    setOnClose(onClose) {
        this.close = onClose;
    }

    setBtnlignes(ligne) {
        this.btnLignes = ligne;
    }

    setBtnBissectrice(biss) {
        this.btnBiss = biss;
    }


    setBtnMediane(med) {
        this.btnMediane = med;
    }

    setBtnMediatrice(med) {
        this.btnMediatrice = med;
    }

    setBtnAxeSym(sym) {
        this.btnAxeSym = sym;
    }

    angleText() {
        let degreeSymbol = String.fromCharCode(176);
        let resultText = "";
        let angleMap = new Map();
        let rightAngle = 0;
        for (let a in this.shape.angles) {
            let angle = this.shape.angles[a];
            if (angleMap.get(angle.alpha))
                angleMap.get(angle.alpha).push(a);
            else
                angleMap.set(angle.alpha, [a]);
            if (angle.alpha === 90)
                rightAngle++;
        }
        for (let value of angleMap.keys()) {
            resultText += " ";
            for (let name of angleMap.get(value)) {
                resultText += name + " = ";
            }
            resultText += value + degreeSymbol;
        }
        //  resultText += rightAngle > 0 ? ` (${rightAngle} ${this.getText('rightAngles')})` : '';
        return resultText;
    }
}
/**
 * @classdesc IHM de la création des titres d'exercice
 * @author Théo Teixeira
 * @version 0.1
 */
class Titre extends Interface{
    constructor(refGame){
        super(refGame, "titre");
        this.refGame = refGame;
        super.setElements({
            title: new PIXI.Text('Hello',{ fontFamily: 'Arial', fontSize: 32, fill: 0x000000, align: 'center' }),

            lblFR: new PIXI.Text('',{ fontFamily: 'Arial', fontSize: 14, fill: 0x000000, align: 'center' }),
            lblDE: new PIXI.Text('',{ fontFamily: 'Arial', fontSize: 14, fill: 0x000000, align: 'center' }),
            lblEN: new PIXI.Text('',{ fontFamily: 'Arial', fontSize: 14, fill: 0x000000, align: 'center' }),
            lblIT: new PIXI.Text('',{ fontFamily: 'Arial', fontSize: 14, fill: 0x000000, align: 'center' }),

            btnSauver: new Button(300,500,'',0x00AA00, 0xFFFFFF),
            btnRetour: new Button(350,500,'',0x00AA00, 0xFFFFFF),
            btnQuit: new Button(325,550,'',0x00AA00, 0xFFFFFF),
        });

    }

    show() {
        console.log("clear");
        // this.clear();
        this.elements.title.x = 300;
        this.elements.title.y = 20;


        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
    }

    refreshLang(lang){

    }

}
/**
 * @classdesc Représente un mode de jeu
 * @author Vincent Audergon
 * @version 1.0
 */
class Mode {

    /**
     * Constructeur d'un mode de jeu
     * @param {string} name le nom du mode de jeu (référencé dans le JSON de langues)
     */
    constructor(name) {
        this.name = name;
        this.interfaces = [];
        this.texts = [];
    }

    /**
     * Défini la liste des interfaces utilisées par le mode de jeu
     * @param {Interface[]} interfaces la liste des interfaces
     */
    setInterfaces(interfaces) {
        this.interfaces = interfaces;
    }

    /**
     * Ajoute une interface au mode de jeu
     * @param {string} name le nom de l'interface
     * @param {Interface} inter l'interface
     */
    addInterface(name, inter) {
        this.interfaces[name] = inter;
    }

    /**
     * Retourne un texte dans la langue courrante
     * @param {string} key la clé du texte
     * @return {string} le texte
     */
    getText(key){
        return this.texts[key];
    }

    /**
     * Méthode de callback appelée lors d'un changement de langue.
     * Indique à toutes les interfaces du mode de jeu de changer les textes en fonction de la nouvelle langue
     * @param {string} lang la langue
     */
    onLanguageChanged(lang) {
        this.texts = this.refGame.global.resources.getOtherText(this.name)
        for (let i in this.interfaces) {
            let inter = this.interfaces[i];
            if (inter instanceof Interface) {
                inter.setTexts(this.texts);
                inter.refreshLang(lang);
            }
        }
    }

}
/**
 * @classdesc Mode création
 * @author Théo Teixeira
 * @version 0.1
 */
class CreationMode extends Mode{
    /**
     * Constructeur du mode création
     * @param {Game} refGame La référence vers la classe de jeu
     */
    constructor(refGame, refTrainMode){
        super('creation');
        this.refGame = refGame;
        this.shapeUtils = new ShapeUtils;
        // Si la création d'une question est en cours
        this.isCreationEnCours = false;
        // Nombre de questions
        this.nQuestion = 0;
        // Exercice créer
        this.exercice = null;
        // Question créer
        this.question = null;
        this.setInterfaces({titlePage: new Creation(refGame,refTrainMode)});
    }

    /**
     * Initialise le mode création. Demande le niveau Harmos de l'exercice.
     */
    init(){
        this.refGame.showText("");
    }

    /**
     * Affiche la page de garde de la création
     */
    show(){
        this.isCreationEnCours = true;
        this.interfaces.titlePage.show();
    }

    /**
     * Fonctione de callback appelée lors d'une changement de langue.
     * Affiche la consigne de la question dans la nouvelle langue.
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        super.onLanguageChanged(lang);
        this.interfaces.titlePage.refreshLang(lang);
    }


    onFontChange(isOpendyslexic){
        this.interfaces.titlePage.updateFont(isOpendyslexic);
    }
}
/**
 * @classdesc Mode création des titres
 * @author Théo Teixeira
 * @version 0.1
 */
class CreationTitreMode extends Mode {
    /**
     * Constructeur du mode création
     * @param {Game} refGame La référence vers la classe de jeu
     */
    constructor(refGame){
        super('creationTitre');
        this.refGame = refGame;
        this.shapeUtils = new ShapeUtils;
        /** Si la création d'une question est en cours */
        this.isCreationEnCours = false;
        /** Nombre de questions */
        this.nQuestion = 0;
        /** Exercice créer */
        this.exercice = null;
        /** Question créer */
        this.question = null;
        this.setInterfaces({titlePage: new Titre(refGame)});
    }

    /**
     * Initialise le mode création. Demande le niveau Harmos de l'exercice.
     */
    init(){
        console.log("Initialisation lancée.");
        this.refGame.showText("");
    }

    /**
     * Affiche la page de garde de la création
     */
    show(){
        this.isCreationEnCours = true;
        this.interfaces.titlePage.show();

    }

    /**
     * Fonctione de callback appelée lors d'une changement de langue.
     * Affiche la consigne de la question dans la nouvelle langue.
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        super.onLanguageChanged(lang);
        this.interfaces.titlePage.refreshLang(lang);
    }
}
/**
 * @classdesc Mode de jeu exploration
 * @author Vincent Audergon
 * @version 1.0
 */
class ExplorationMode extends Mode {

    /**
     * Constructeur du mode exploration
     * @param {Game} refGame La référence vers la classe de jeu
     */
    constructor(refGame) {
        super('explore');
        this.refGame = refGame;
        this.drawmode = false;
        this.setInterfaces({ draw: new Draw(refGame), explore: new Explore(refGame), shapeInfo: new ShapeInfo(refGame) })
    }

    /**
     * Initialise le mode de jeu et charge les polygones
     */
    init() {
        this.refGame.inputs.drawMode.onclick = function () {
            this.drawMode = !this.drawMode;
            this.show(this.drawMode);
        }.bind(this);
        try{
            let shapes = JSON.parse(this.refGame.global.resources.getScenario());
            shapes = shapes.shapes;
            this.refGame.shapeUtils.loadShapes(shapes);
            this.interfaces.explore.show(this.refGame.shapeUtils.getShapes());
            this.interfaces.draw.setOnShapeCreated(function (shape) {
                this.displayShapeInfo(shape, true, true,0,0,0,0, true, shape, false);
            }.bind(this));
            this.interfaces.explore.setOnClick(function (shape) {
                this.displayShapeInfo(shape, false, true,0,0,0,0, true, undefined, false);
            }.bind(this));
        } catch(error){
            Swal.fire({ title: this.refGame.global.resources.getOtherText('sorry'), type: 'error', text: this.refGame.global.resources.getOtherText('noshapes') });
        }
    }

    /**
     * Affiche le mode explorer dans le mode "dessin" ou "découverte"
     * @param {boolean} draw si c'est le mode dessin qui doit être affiché
     */
    show(draw) {
        if (draw) {
            this.refGame.showText(this.refGame.global.resources.getTutorialText('exploredraw'))
            this.interfaces.draw.show();
        } else {
            this.refGame.showText(this.refGame.global.resources.getTutorialText('explore'))
            this.interfaces.explore.show(this.refGame.shapeUtils.getShapes());
        }
        this.refGame.inputs.drawMode.style.display = 'block';
    }

    /**
     * Affiche les informations liée à une forme sur la page
     * @param {Shape} shape La forme dont on doit afficher les informations
     */
    displayShapeInfo(shape, draw, lines, biss, med, sideBis, axe, point, secShape = undefined, gravity, nDiagonal) {
        let JSONshapes = JSON.parse(this.refGame.global.resources.getScenario())
        let shapes = JSONshapes.shapes;
        if (!shape.initialized) this.refGame.shapeUtils.initShape(shape);
        let identify = this.refGame.shapeUtils.identifyShape(shape);

        let nAxeSym, nPaireCotePara = 0;
        let axeSym, bissec, mediatrice, mediane, gravityPoint, diagonal = [];

        shape.id = identify.id;
        shape.names = [];
        shape.names = identify.names;

        if (draw){
            let sh = this.refGame.shapeUtils.getShapes();
            let ind = undefined;
            for (let i = 0; i < sh.length; i++) {
                if (shape.id === sh[i].id){
                    ind = sh[i];
                    break;
                }
            }
            let s = undefined;
            for (let i = 0; i < shapes.length; i++) {
                if (secShape.id === shapes[i].id){
                    s = shapes[i];
                    nAxeSym = shapes[i]["nbrAxeSym"];
                    nPaireCotePara =  shapes[i]["nbrPaireCotePara"];
                    axeSym = shapes[i]["AxeSym"];
                    bissec = shapes[i]["bissectrice"];
                    mediane = shapes[i]["mediane"];
                    mediatrice = shapes[i]["mediatrice"];
                    gravityPoint = shapes[i]["gravity"];
                    break;
                }
            }
            this.interfaces.shapeInfo.show(ind, secShape, secShape.initGraphics(100, 100, false),
                ind.initGraphics(200, 200, lines, nAxeSym, nPaireCotePara, axeSym, bissec, mediatrice, mediane, gravityPoint, axe, biss, med, sideBis, point, gravity,diagonal,nDiagonal), draw);
        } else {
            let shapeSec = shape;
            let identifySec = identify;

            for (let i = 0; i < shapes.length; i++) {
                if (shape.id === shapes[i].id){
                    nAxeSym = shapes[i]["nbrAxeSym"];
                    nPaireCotePara =  shapes[i]["nbrPaireCotePara"];
                    axeSym = shapes[i]["AxeSym"];
                    bissec = shapes[i]["bissectrice"];
                    mediane = shapes[i]["mediane"];
                    mediatrice = shapes[i]["mediatrice"];
                    gravityPoint = shapes[i]["gravity"];
                    diagonal = shapes[i]['diagonal'];
                    break;
                }
            }
            this.interfaces.shapeInfo.show(shape, shapeSec, shapeSec.initGraphics(100, 100, false),
                shape.initGraphics(200, 200, lines, nAxeSym, nPaireCotePara, axeSym, bissec, mediatrice, mediane, gravityPoint, axe, biss, med, sideBis, point, gravity,diagonal,nDiagonal), draw);
        }

        this.interfaces.shapeInfo.setOnClose(function () {
            this.show(this.drawMode);
        }.bind(this))
    }


}
class PlayMode extends Mode{
    /**
     * Constructeur du mode play
     * @param {Game} refGame La référence vers la classe de jeu
     */
    constructor(refGame, refTrainMode){
        super('play');
        this.refGame = refGame;
        this.trainMode = refTrainMode;
        this.setInterfaces({main: new Play(refGame, refTrainMode)});
    }

    /**
     * Initialise le mode création. Demande le niveau Harmos de l'exercice.
     */
    init(){
        this.refGame.showText("");
    }

    /**
     * Affiche la page de garde de la création
     */
    show(){
        this.interfaces.main.show();
    }

    /**
     * Fonctione de callback appelée lors d'une changement de langue.
     * Affiche la consigne de la question dans la nouvelle langue.
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        super.onLanguageChanged(lang);
        this.interfaces.main.refreshLang(lang);
    }


    onFontChange(isOpendyslexic){
        this.interfaces.main.updateFont(isOpendyslexic);
    }
}
/**
 * @classdesc Mode entraînement
 * @author Vincent Audergon
 * @version 1.0
 */
class TrainMode extends Mode {

    /**
     * Constructeur du mode entrainement / evaluation
     * @param {Game} refGame La référence vers la classe de jeu
     */
    constructor(refGame) {
        super('train');

        this.isTryMode = false;
        this.refCreationMode = null;

        this.refGame = refGame;
        this.questionUtils = new QuestionUtils();
        /** @type {boolean} Si un quizz est en cours */
        this.running = false;
        /** @type {boolean} Mode évaluation ou mode exercice */
        this.evaluation = false;
        /** @type {boolean[]} Le résultat des questions */
        this.results = [];
        this.setInterfaces({
            QCM: new QCM(refGame),
            draw: new Draw(refGame, "drawQuestion"),
            titlePage: new Exercice(refGame)
        });

        this.currentExerciceId = 0;
    }

    /**
     * Initialise le mode entrainement / évaluation. Charge un exercice dans la DB en fonction du niveau harmos
     * @param {boolean} evaluation si il s'agit d'une évaluation
     */
    init(evaluation, tryMode = false, exercice = null, refCreationMode = null) {
        if (!tryMode) {
            this.refGame.inputs.signal.onclick = function () {
                Swal.fire({
                    title: this.getText('signalementTitle'),
                        cancelButtonText: this.getText('signalementCancel'),
                        showCancelButton: true,
                    html:
                        '<label>'+this.getText('signalementReason')+'</label><textarea id="swal-reason" class="swal2-textarea"></textarea>' +
                        '<label>'+this.getText('signalementPassword')+'</label><input type="password" id="swal-password" class="swal2-input">',
                    preConfirm: function () {
                        return new Promise(function (resolve) {
                            resolve([
                                $('#swal-reason').val(),
                                $('#swal-password').val()
                            ])
                        })
                    },
                    onOpen: function () {
                        $('#swal-reason').focus()
                    }
                }).then(function (result) {
                    this.refGame.global.resources.signaler(result.value[0],result.value[1],this.currentExerciceId, function (res) {
                        Swal.fire(res.message,'',res.type);
                        if(res.type === 'success'){
                            this.init(this.evaluation);
                            this.show();
                        }
                    }.bind(this));
                }.bind(this));
            }.bind(this);

        }

        if (tryMode && exercice != null && refCreationMode != null) {
            this.isTryMode = tryMode;
            this.refCreationMode = refCreationMode;
        } else
            this.isTryMode = false;

        this.evaluation = evaluation;
        this.data = this.refGame.global.resources.getExercice();
        try {
            exercice = JSON.parse(this.isTryMode ? exercice : this.data.exercice);
            if (!this.isTryMode) {
                this.currentExerciceId = this.data.id;
                let scenario = JSON.parse(this.refGame.global.resources.getScenario());
                if (scenario.shapes) {
                    this.refGame.shapeUtils.loadShapes(scenario.shapes);
                }
            }
            if (this.questionUtils.loadExercice(exercice)) {
                this.interfaces.titlePage.setOnBegin(function () {
                    this.running = true;
                    this.nextQuestion()
                }.bind(this));
            } else {
                this.interfaces.titlePage.setOnBegin(function () {
                    Swal.fire({
                        title: this.getText('sorry'),
                        type: 'error',
                        text: this.getText('noexercice')
                    })
                }.bind(this));
            }
        } catch (error) {
            Swal.fire({
                title: this.getText('sorry'),
                type: 'error',
                text: this.getText('noexercice')
            });
        }
    }

    /**
     * Affiche la page de garde du quizz
     */
    show() {
        this.running = false;
        this.questionUtils.start();
        this.results = [];
        this.interfaces.titlePage.show(`Harmos ${this.refGame.global.resources.getDegre()}`, this.evaluation, this.questionUtils.getExerciceNames());
        this.refGame.showText(this.refGame.global.resources.getTutorialText(this.evaluation ? 'evaluate' : 'train'));
    }

    /**
     * Passe à la prochaine question et l'affiche sur la page
     */
    nextQuestion() {
        if (!this.questionUtils.isFinished()) {
            this.displayQuestion(this.questionUtils.nextQuestion());
            if(!this.isTryMode)
                this.refGame.inputs.signal.style.display = "block";
        } else {
            //FIN DE L'EXERCICE
            let correct = 0;
            let list = "";
            for (let r in this.results) {
                let result = this.results[r];
                if (result) correct++;
                list += `<li>${this.getText('question')} ${Number(r) + 1}: <span style="color:${result ? 'lime' : 'red'}">${result ? this.getText('correct') : this.getText('wrong')}</span></li>`
            }
            let html = `<ul class="sans-puce">${list}</ul><p>${correct}/${this.results.length}</p>`;
            this.refGame.global.util.showAlertPersonnalized({title: this.getText('results'), html: html});
            this.running = false;
            if (this.isTryMode)
                this.refCreationMode.show();
            else {
                this.refGame.global.statistics.addStats(this.evaluation ? 2 : 1);
                this.refGame.global.statistics.updateStats(this.getGrade(correct, this.results.length));
                this.init(this.evaluation);
                this.show();
            }

        }
    }

    /**
     * Calcule la note en fonction du résultat obtenu
     * @param {int} correct le nombre de bonnes réponses
     * @param {int} total le nombre de questions
     * @return {int} la note de 0 à 2
     */
    getGrade(correct, total) {
        let barem = JSON.parse(this.refGame.global.resources.getEvaluation()).notes;
        let pourcentageCorrect = (correct/total)*100;
        for (let b of barem) {
            if (b.min <= pourcentageCorrect && b.max >= pourcentageCorrect) {
                console.log(b);
                return b.note;
            }
        }
        return 0;
    }

    /**
     * Affiche une question sur la page
     * @param {Question} question La question à afficher
     */
    displayQuestion(question) {
        this.question = question;
        if (question.type === 0) { //QCM
            this.interfaces.QCM.show(question);
            this.interfaces.QCM.setOnResponse(function (responses) {
                let result = this.questionUtils.checkResponse(responses);
                this.results.push(result);
                this.refGame.global.util.showAlertPersonnalized({
                    title: result ? this.getText('correct') : this.getText('wrong'),
                    type: result ? 'success' : 'error'
                });
                this.nextQuestion();
            }.bind(this))
        } else if (question.type === 1) { //Draw
            this.interfaces.draw.show();
            this.interfaces.draw.setOnShapeCreated(function (shape) {
                this.refGame.shapeUtils.initShape(shape);
                let identify = this.refGame.shapeUtils.identifyShape(shape);
                shape.id = identify.id;
                shape.names = identify.names;
                let result = this.questionUtils.checkResponse([shape.id], true);
                this.results.push(result);
                this.refGame.global.util.showAlertPersonnalized({
                    title: result ? this.getText('correct') : this.getText('wrong'),
                    type: result ? 'success' : 'error'
                })
                this.nextQuestion();
            }.bind(this));
        }
        this.onLanguageChanged(this.refGame.global.resources.getLanguage());
    }

    /**
     * Fonctione de callback appelée lors d'une changement de langue.
     * Affiche la consigne de la question dans la nouvelle langue.
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        super.onLanguageChanged(lang);
        if (this.question && this.running) this.refGame.showText(this.question.label[lang]);
    }

}
/**
 * @classdesc Classe principale du jeu
 * @author Vincent Audergon
 * @version 1.0
 */

const VERSION = 1.0;

class Game {

    /**
     * Constructeur du jeu
     * @param {Object} global Un objet contenant les différents objets du Framework
     */
    constructor(global) {
        this.global = global;
        this.global.util.callOnGamemodeChange(this.onGamemodeChanged.bind(this));
        this.global.resources.callOnLanguageChange(this.onLanguageChanged.bind(this));
        this.global.resources.callOnFontChange(this.onFontChange.bind(this));
        this.shapeUtils = new ShapeUtils();
        this.scenes = { draw: new PIXI.Container(), drawQuestion: new PIXI.Container(), explore: new PIXI.Container(), qcm: new PIXI.Container(), shapeInfo: new PIXI.Container(), exercice: new PIXI.Container(), creation: new PIXI.Container, play:new PIXI.Container()}
        this.explorationMode = new ExplorationMode(this);
        this.trainMode = new TrainMode(this);
        this.createMode = new CreationMode(this,this.trainMode);
        this.creationTitre = new CreationTitreMode(this);
        this.playMode = new PlayMode(this, this.trainMode);
        this.oldGamemode = undefined;
        this.inputs = {
            drawMode: document.getElementById('drawMode'),
            signal: document.getElementById('signal')
        };
        new this.global.Log('success', `Chargement de "Formes géométriques" version ${VERSION}`).show();
    }

    /**
     * Affiche une scène sur le canvas
     * @param {PIXI.Container} scene La scène à afficher
     */
    showScene(scene) {
        new this.global.Log('success', `Chargement de la scène`).show();
        for (let input in this.inputs) {
            this.inputs[input].style.display = 'none';
        }
        this.reset();
        this.global.pixiApp.stage.addChild(scene);
    }

    /**
     * Retire toutes les scènes du stage PIXI
     */
    reset() {
        for (let scene in this.scenes) {
            this.global.pixiApp.stage.removeChild(this.scenes[scene])
        }
    }

    /**
     * Affiche du texte sur la page (à gauche du canvas)
     * @param {string} text Le texte à afficher
     */
    showText(text) {
        this.global.util.setTutorialText(text);
    }

    showCreationTitre(){
        this.creationTitre.init();
        this.creationTitre.show();
    }

    /**
     * Fonction de callback appelée lors d'un chagement de langue.
     * Indiques à tous les modes de jeu le changement de langue
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        this.explorationMode.onLanguageChanged(lang);
        this.trainMode.onLanguageChanged(lang);
        this.createMode.onLanguageChanged(lang);
        this.playMode.onLanguageChanged(lang);
    }


    onFontChange(isOpendyslexic){
        this.createMode.onFontChange(isOpendyslexic);
        this.playMode.onFontChange(isOpendyslexic);
    }

    /**
     * Listener appelé par le Framework lorsque le mode de jeu change.
     * Démarre le bon mode de jeu en fonction de celui qui est choisi
     * @async
     */
    async onGamemodeChanged() {
        let gamemode = this.global.util.getGamemode();
        let ok = true;
        if (this.trainMode.running) {
            await Swal.fire({
                title: this.global.resources.getOtherText('quit'),
                text: this.global.resources.getOtherText('progresslost'),
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: this.global.resources.getOtherText('yes'),
                cancelButtonText: this.global.resources.getOtherText('no'),
                reverseButtons: true
            }).then(function (result) {
                ok = (result.value);
                if (ok) {
                    this.trainMode.running = false;
                } else {
                    this.global.util.setGamemode(this.oldGamemode);
                }
            }.bind(this));
        }
        if (ok) {

            switch (gamemode) {
                case this.global.Gamemode.Evaluate:
                    this.trainMode.init(true);
                    this.trainMode.show();
                    break;
                case this.global.Gamemode.Train:
                    this.trainMode.init(false);
                    this.trainMode.show();
                    break;
                case this.global.Gamemode.Explore:
                    this.explorationMode.init();
                    this.explorationMode.show(false);
                    break;
                case this.global.Gamemode.Create:
                    this.createMode.init();
                    this.createMode.show();
                    break;
                case this.global.Gamemode.Play:
                    this.playMode.init();
                    this.playMode.show();
                    break;
            }
            this.oldGamemode = gamemode;
            new this.global.Log('success', 'Mode de jeu changé').show();
        }
    }
}
const global = {};
global.canvas = Canvas.getInstance();
global.Log = Log;
global.util = Util.getInstance();
global.pixiApp = new PixiFramework().getApp();
global.resources = Resources;
global.statistics = Statistics;
global.Gamemode = Gamemode;
global.listenerManager = ListenerManager.getInstance();
const game = new Game(global);
